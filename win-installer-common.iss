#define MyAppName "Parallel Launcher"
#define MyAppVersion "2.1.3"
#define MyAppPublisher "Matt Pharoah"
#define MyAppURL "https://blueprint64.ca/parallel-launcher"
#define MyAppExeName "parallel-launcher.exe"

#define RegistryId "parallel_launcher"

[InstallDelete]
Type: files; Name: "{app}\libbrotlidec.dll"
Type: files; Name: "{app}\libbrotlicommon.dll"
Type: files; Name: "{app}\libbrotilicommon.dll"
Type: files; Name: "{app}\libbz2-1.dll"
Type: files; Name: "{app}\libdouble-conversion.dll"
Type: files; Name: "{app}\libfreetype-6.dll"
Type: files; Name: "{app}\libgcc_s_seh-1.dll"
Type: files; Name: "{app}\libglib-2.0-0.dll"
Type: files; Name: "{app}\libgcc_s_dw2-1.dll"
Type: files; Name: "{app}\libgraphite2.dll"
Type: files; Name: "{app}\libharfbuzz-0.dll"
Type: files; Name: "{app}\libiconv-2.dll"
Type: files; Name: "{app}\libicudt67.dll"
Type: files; Name: "{app}\libicuin67.dll"
Type: files; Name: "{app}\libicuuc67.dll"
Type: files; Name: "{app}\libintl-8.dll"
Type: files; Name: "{app}\libpcre-1.dll"
Type: files; Name: "{app}\libpcre2-16-0.dll"
Type: files; Name: "{app}\libpng16-16.dll"
Type: files; Name: "{app}\libstdc++-6.dll"
Type: files; Name: "{app}\libwinpthread-1.dll"
Type: files; Name: "{app}\libzstd.dll"
Type: files; Name: "{app}\zlib1.dll"
Type: files; Name: "{app}\qrc_resources.cpp"

[Setup]
AppId=2C94867F-0911-4237-B447-CE6E30AD7F55
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\parallel-launcher
DisableProgramGroupPage=yes
LicenseFile={#SourcePath}\LICENSE
PrivilegesRequired=admin
OutputDir={#SourcePath}\{#TargetArch}
OutputBaseFilename=parallel-launcher_setup_{#TargetArch}
Compression=lzma
SolidCompression=yes
WizardStyle=modern
ChangesAssociations=yes

[Registry]
Root: HKCR; Subkey: ".z64"; ValueName: ""; ValueType: string; ValueData: "{#RegistryId}"; Flags: uninsdeletevalue
Root: HKCR; Subkey: ".n64"; ValueName: ""; ValueType: string; ValueData: "{#RegistryId}"; Flags: uninsdeletevalue
Root: HKCR; Subkey: ".v64"; ValueName: ""; ValueType: string; ValueData: "{#RegistryId}"; Flags: uninsdeletevalue
Root: HKCR; Subkey: ".bps"; ValueName: ""; ValueType: string; ValueData: "{#RegistryId}"; Flags: uninsdeletevalue

Root: HKA; Subkey: "Software\Classes\Applications\{#MyAppExeName}\SupportedTypes"; ValueType: string; ValueName: ".z64"; ValueData: ""
Root: HKA; Subkey: "Software\Classes\Applications\{#MyAppExeName}\SupportedTypes"; ValueType: string; ValueName: ".n64"; ValueData: ""
Root: HKA; Subkey: "Software\Classes\Applications\{#MyAppExeName}\SupportedTypes"; ValueType: string; ValueName: ".v64"; ValueData: ""
Root: HKA; Subkey: "Software\Classes\Applications\{#MyAppExeName}\SupportedTypes"; ValueType: string; ValueName: ".bps"; ValueData: ""

Root: HKCR; Subkey: "{#RegistryId}"; ValueName: ""; ValueType: string; ValueData: "N64 ROM"; Flags: uninsdeletekey
Root: HKCR; Subkey: "{#RegistryId}\shell\open\command"; ValueName: ""; ValueType: string; ValueData: """{app}\{#MyAppExeName}"" ""%1"""

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkedonce

[Files]
Source: "{#SourcePath}\release\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
