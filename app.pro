QT += core gui widgets
TEMPLATE = app

TARGET = "parallel-launcher"

CONFIG += qt
CONFIG(debug, debug|release){
	OBJECTS_DIR = "build/debug/obj"
	MOC_DIR = "build/debug/moc"
	UI_DIR = "build/debug/ui"
	DEFINES += DEBUG
	!win32 {
		QMAKE_CXXFLAGS += -rdynamic
	}
}
CONFIG(release, debug|release){
	OBJECTS_DIR = "build/release/obj"
	MOC_DIR = "build/release/moc"
	UI_DIR = "build/release/ui"
}

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += src/main.cpp src/core/*.cpp src/ui/*.cpp src/polyfill/*.cpp src/input/*.cpp src/thirdparty/*.cpp
HEADERS += src/types.hpp src/core/*.hpp src/ui/*.hpp src/polyfill/*.hpp src/input/*.hpp src/thirdparty/*.h
FORMS += src/ui/designer/*.ui
RESOURCES = data/resources.qrc

win32 {
	win32:contains(QMAKE_HOST.arch, x86_64) {
		WIN_DIR = win64
	} else {
		WIN_DIR = win32
	}
	
	CONFIG(debug, debug|release){
		WIN_BUILD_DIR = debug
	}
	CONFIG(release, debug|release){
		WIN_BUILD_DIR = release
	}

	CONFIG += embed_manifest_exe
	QMAKE_CXXFLAGS += /std:c++17 /WX /utf-8 /Zc:preprocessor /Wv:18
	LIBS += -L$${WIN_DIR}/lib -lonecore -lSDL2
	INCLUDEPATH += $${WIN_DIR}/include
	
	VERSION = 2.1.3
	RC_ICONS = "data\\appicon.ico"
	RC_LANG = "EN-CA"
	QMAKE_TARGET_COMPANY = "Matt Pharoah"
	QMAKE_TARGET_COPYRIGHT = "GNU GENERAL PUBLIC LICENSE Version 2"
	QMAKE_TARGET_PRODUCT = "Parallel Launcher"
	
	lsjsTarget.target = parallel-launcher-lsjs.exe
	lsjsTarget.depends = FORCE
	lsjsTarget.commands = cl /WX /Wv:18 /O2 /I $${WIN_DIR}\\include lsjs.c /link $${WIN_DIR}\\lib\\SDL2.lib /out:$${WIN_BUILD_DIR}\\parallel-launcher-lsjs.exe
	
	lsjsStub.target = data/parallel-launcher-lsjs
	lsjsStub.depends = FORCE
	lsjsStub.commands = type nul > data\\parallel-launcher-lsjs
	
	PRE_TARGETDEPS += parallel-launcher-lsjs.exe data/parallel-launcher-lsjs
	QMAKE_EXTRA_TARGETS += lsjsTarget lsjsStub
}

!win32 {
	CONFIG += object_parallel_to_source
	QMAKE_CXXFLAGS += -std=c++17 -Werror
	QMAKE_LFLAGS += -no-pie -fno-pie
	LIBS += -lstdc++fs -lSDL2 -lpthread
	
	lsjsTarget.target = data/parallel-launcher-lsjs
	lsjsTarget.depends = FORCE
	lsjsTarget.commands = gcc -std=c99 -O3 lsjs.c -o data/parallel-launcher-lsjs -lSDL2
	PRE_TARGETDEPS += data/parallel-launcher-lsjs
	QMAKE_EXTRA_TARGETS += lsjsTarget
}
