#ifndef SRC_TYPES_HPP_
#define SRC_TYPES_HPP_

#include <string>

using std::string;
using std::size_t;
using namespace std::string_literals;

#define HashMap std::unordered_map
#define HashSet std::unordered_set

typedef signed char sbyte;
typedef unsigned char ubyte;

#ifdef _WIN32
typedef unsigned short ushort;
typedef unsigned uint;
typedef unsigned long ulong;
#endif

typedef long long int64;
typedef unsigned long long uint64;

#endif /* SRC_TYPES_HPP_ */
