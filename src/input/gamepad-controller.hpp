#ifndef SRC_INPUT_JOYPAD_CONTROLLER_HPP_
#define SRC_INPUT_JOYPAD_CONTROLLER_HPP_

#include <QTimer>
#include <vector>
#include "src/core/controller.hpp"

typedef int GamepadId;

struct GamepadConnectedEvent {
	GamepadId id;
	ControllerInfo info;
};

struct GamepadDisconnectedEvent {
	GamepadId id;
};

struct GamepadButtonEvent {
	GamepadId id;
	ubyte button;
	bool isPressed;
};

struct GamepadAxisEvent {
	GamepadId id;
	ubyte axis;
	double position;
};

struct GamepadState {
	std::vector<bool> buttons;
	std::vector<double> axes;
};

struct ConnectedGamepad {
	GamepadId id;
	ControllerInfo info;
};

class GamepadController : public QObject {
	Q_OBJECT

	private:
	QTimer m_timer;

	GamepadController();

	public:
	virtual ~GamepadController();

	void start();
	void stop();

	std::vector<ConnectedGamepad> getConnected() const;
	GamepadState getState( GamepadId id ) const;

	static GamepadController &instance();

	private slots:
	void processEvents();

	signals:
	void gamepadConnected( GamepadConnectedEvent );
	void gamepadDisconnected( GamepadDisconnectedEvent );
	void gamepadButtonPressed( GamepadButtonEvent );
	void gamepadAxisMoved( GamepadAxisEvent );

};

#endif /* SRC_INPUT_JOYPAD_CONTROLLER_HPP_ */
