#include "gamepad-controller.hpp"

#define SDL_MAIN_HANDLED 1
#include <SDL2/SDL.h>
#include <unordered_map>
#include <cstring>
#include "src/core/preset-controllers.hpp"

const HashMap<ControllerType, const char*> s_knownNames = {
	{ ControllerType::Gamecube, "Gamecube Controller" },
	{ ControllerType::XBox360, "XBox 360 Controller" }
};

static inline double normalize( short axisPosn ) {
	return (double)axisPosn / (axisPosn >= 0 ? (double)0x7FFF : (double)0x8000 );
}

static GamepadId openGamepad( int index, ControllerInfo &info ) {
	info.controllerId.vendorId = SDL_JoystickGetDeviceVendor( index );
	info.controllerId.productId = SDL_JoystickGetDeviceProduct( index );
	info.uuid = Uuid( SDL_JoystickGetDeviceGUID( index ).data );

	SDL_Joystick *joypad = nullptr;
	const char *name;
	if( SDL_IsGameController( index ) ) {
		SDL_GameController *controller = SDL_GameControllerOpen( index );
		if( controller == nullptr ) {
			return -1;
		}

		name = SDL_GameControllerName( controller );
		joypad = SDL_GameControllerGetJoystick( controller );
		info.numButtons = 15;
		info.numAxes = 6;
	} else {
		joypad = SDL_JoystickOpen( index );
		if( joypad == nullptr ) {
			return -1;
		}

		name = SDL_JoystickNameForIndex( index );
		info.numButtons = (ushort)SDL_JoystickNumButtons( joypad );
		info.numAxes = (ushort)SDL_JoystickNumAxes( joypad );
	}

	if( name == nullptr ) {
		auto i = s_knownNames.find( getControllerType( info.controllerId ) );
		name = (i != s_knownNames.end()) ? i->second : "";
	}

	if(
		info.controllerId.vendorId == 0 &&
		info.controllerId.productId == 0 &&
		std::strncmp( name, "Wii U GameCube Adapter", 22 ) == 0
	) {
		info.controllerId.vendorId = 0x057E;
		info.controllerId.productId = 0x0337;
	}

	info.name = name;
	return SDL_JoystickInstanceID( joypad );
}

void GamepadController::processEvents() {
    SDL_Event event;
    if( !m_timer.isActive() ) return;
	while( SDL_PollEvent( &event ) ) {
		switch( event.type ) {
			case SDL_JOYDEVICEADDED: {
				ControllerInfo info;
				SDL_LockJoysticks();
				const GamepadId joypadId = openGamepad( event.jdevice.which, info );
				SDL_UnlockJoysticks();
				if( joypadId >= 0 ) {
					emit gamepadConnected({
						joypadId,
						info
					});
				}
				break;
			}
			case SDL_JOYDEVICEREMOVED:
				emit gamepadDisconnected({
					event.jdevice.which
				});
				break;
			case SDL_CONTROLLERBUTTONDOWN:
			case SDL_CONTROLLERBUTTONUP:
				emit gamepadButtonPressed({
					event.cbutton.which,
					event.cbutton.button,
					event.cbutton.state == SDL_PRESSED
				});
				break;
			case SDL_JOYBUTTONDOWN:
			case SDL_JOYBUTTONUP:
				if( SDL_GameControllerFromInstanceID( event.jbutton.which ) != nullptr ) break;
				emit gamepadButtonPressed({
					event.jbutton.which,
					event.jbutton.button,
					event.jbutton.state == SDL_PRESSED
				});
				break;
			case SDL_CONTROLLERAXISMOTION:
				emit gamepadAxisMoved({
					event.caxis.which,
					event.caxis.axis,
					normalize( event.caxis.value )
				});
				break;
			case SDL_JOYAXISMOTION:
				if( SDL_GameControllerFromInstanceID( event.jbutton.which ) != nullptr ) break;
				emit gamepadAxisMoved({
					event.jaxis.which,
					event.jaxis.axis,
					normalize( event.jaxis.value )
				});
				break;
			default: break;
		}
	}
}

GamepadController::GamepadController() : QObject() {
	SDL_Init( SDL_INIT_GAMECONTROLLER | SDL_INIT_JOYSTICK | SDL_INIT_EVENTS );
	connect( &m_timer, SIGNAL(timeout()), this, SLOT(processEvents()) );
}

GamepadController::~GamepadController() {
	m_timer.stop();
	SDL_Quit();
}

void GamepadController::start() {
	SDL_JoystickEventState( SDL_ENABLE );
	SDL_GameControllerEventState( SDL_ENABLE );

	SDL_LockJoysticks();
	const int numConnected = SDL_NumJoysticks();
	for( int i = 0; i < numConnected; i++ ) {
		ControllerInfo info;
		const GamepadId id = openGamepad( i, info );
		if( id < 0 ) continue;
		emit gamepadConnected({ id, info });
	}
	SDL_UnlockJoysticks();

	processEvents();
	m_timer.setInterval( 10 );
	m_timer.start();
}

void GamepadController::stop() {
	SDL_JoystickEventState( SDL_DISABLE );
	SDL_GameControllerEventState( SDL_DISABLE );
	processEvents();
	m_timer.stop();
}

GamepadController &GamepadController::instance() {
	static GamepadController s_instance;
	return s_instance;
}

static inline GamepadState getState_SDLController( SDL_GameController *controller ) {
	GamepadState state;

	state.buttons.reserve( 15 );
	state.axes.reserve( 6 );

	for( int i = 0; i < 15; i++ ) {
		state.buttons.push_back( SDL_GameControllerGetButton( controller, (SDL_GameControllerButton)i ) > 0 );
	}

	for( int i = 0; i < 6; i++ ) {
		state.axes.push_back( normalize( SDL_GameControllerGetAxis( controller, (SDL_GameControllerAxis)i ) ) );
	}

	return state;
}

static inline GamepadState getState_SDLJoystick( SDL_Joystick *joystick ) {
	GamepadState state;

	int numButtons = SDL_JoystickNumButtons( joystick );
	int numAxes = SDL_JoystickNumAxes( joystick );

	state.buttons.reserve( numButtons > 0 ? numButtons : 0 );
	state.axes.reserve( numAxes > 0 ? numAxes : 0 );

	for( int i = 0; i < numButtons; i++ ) {
		state.buttons.push_back( SDL_JoystickGetButton( joystick, i ) > 0 );
	}

	for( int i = 0; i < numAxes; i++ ) {
		state.axes.push_back( normalize( SDL_JoystickGetAxis( joystick, i ) ) );
	}

	return state;
}

GamepadState GamepadController::getState( GamepadId id ) const {
	SDL_GameController *controller = SDL_GameControllerFromInstanceID( id );
	if( controller != nullptr ) {
		return getState_SDLController( controller );
	}

	SDL_Joystick *joystick = SDL_JoystickFromInstanceID( id );
	if( joystick != nullptr ) {
		return getState_SDLJoystick( joystick );
	}

	std::cerr << "[Warn] Could not read state for joypad." << std::endl << std::flush;
	return GamepadState();
}

std::vector<ConnectedGamepad> GamepadController::getConnected() const {
	std::vector<ConnectedGamepad> gamepads;

	SDL_JoystickUpdate();
	const int numConnected = SDL_NumJoysticks();
	for( int i = 0; i < numConnected; i++ ) {
		ConnectedGamepad gamepad;
		gamepad.id = openGamepad( i, gamepad.info );
		if( gamepad.id >= 0 ) {
			gamepads.push_back( std::move( gamepad ) );
		}
	}

	return gamepads;
}
