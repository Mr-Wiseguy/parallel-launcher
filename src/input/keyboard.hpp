#ifndef SRC_INPUT_KEYBOARD_HPP_
#define SRC_INPUT_KEYBOARD_HPP_

struct KeyName {
	const char *retroConfigName;
	const char *displayName;
};

namespace Keycode {
	extern const KeyName &getNames( int key );
}


#endif /* SRC_INPUT_KEYBOARD_HPP_ */
