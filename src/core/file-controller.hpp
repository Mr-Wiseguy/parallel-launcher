#ifndef SRC_CORE_FILE_CONTROLLER_HPP_
#define SRC_CORE_FILE_CONTROLLER_HPP_

#include <unordered_map>
#include <vector>
#include <set>
#include <map>
#include "src/core/controller.hpp"
#include "src/core/settings.hpp"
#include "src/core/rom.hpp"
#include "src/ui/ui-state.hpp"

namespace FileController {

	const AppSettings &loadAppSettings();
	void saveAppSettings( const AppSettings &settings );

	const std::vector<ROM> &loadRomList();
	void saveRomList( const std::vector<ROM> &roms );

	const std::vector<RomSource> &loadRomSources();
	void saveRomSources( const std::vector<RomSource> &sources );

	const std::vector<fs::path> &loadSavedRoms();
	void saveSavedRoms( const std::vector<fs::path> &savedRoms );

	const std::set<string> &loadTags();
	void saveTags( const std::set<string> &tags );

	const std::map<string, ControllerProfile> &loadControllerProfiles();
	void saveControllerProfiles( const std::map<string, ControllerProfile> &profiles );

	const HashMap<Uuid,string> &loadControllerMappings();
	void saveControllerMappings( const HashMap<Uuid,string> &mappings );

	const UiState &loadUiState();
	void saveUiState( const UiState &state );

	const std::vector<int> &loadHotkeys();
	void saveHotkeys( const std::vector<int> &hotkeys );

}



#endif /* SRC_CORE_FILE_CONTROLLER_HPP_ */
