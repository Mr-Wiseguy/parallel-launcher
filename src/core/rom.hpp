#ifndef SRC_CORE_ROM_HPP_
#define SRC_CORE_ROM_HPP_

#include <chrono>
#include <vector>
#include <set>
#include <unordered_map>
#include "src/core/filesystem.hpp"
#include "src/core/retroarch.hpp"
#include "src/core/json.hpp"
#include "src/core/async.hpp"

struct ROM {
	string name;
	string internalName;
	fs::path path;
	EmulatorCore emulator;
	GfxPlugin parallelPlugin;
	GfxPlugin mupenPlugin;
	int64 lastPlayed;
	int64 playTime;
	std::set<string> tags;
	string notes;
	bool overclockCPU;
	bool overclockVI;
	int64 fileVersion;
	uint crc32;
};

struct RomSource {
	fs::path folder;
	bool recursive;
	bool ignoreHidden;
	bool followSymlinks;
	ubyte maxDepth;
	std::set<string> autoTags;
};

namespace JsonSerializer {
	template<> void serialize<ROM>( JsonWriter &jw, const ROM &obj );
	template<> ROM parse<ROM>( const Json &json );

	template<> void serialize<RomSource>( JsonWriter &jw, const RomSource &obj );
	template<> RomSource parse<RomSource>( const Json &json );

	template<> void serialize<fs::path>( JsonWriter &jw, const fs::path &obj );
	template<> fs::path parse<fs::path>( const Json &json );
}

namespace RomFinder {
	extern std::vector<ROM> scan(
		const std::vector<ROM> &currentRomList,
		const std::vector<RomSource> &sources,
		const std::vector<fs::path> &additionalRomPaths,
		CancellationToken &cancellationToken
	);
}

namespace RomUtil {
	extern string getInternalName( const fs::path romPath );
	extern int64 getLastModified( const fs::path romPath );
	extern uint getCrc32( const fs::path &romPath );
}

#endif /* SRC_CORE_ROM_HPP_ */
