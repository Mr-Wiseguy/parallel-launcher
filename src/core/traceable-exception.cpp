#include "src/core/traceable-exception.hpp"

#if defined(DEBUG) && !defined(_WIN32)
#include <execinfo.h>

TraceableException::TraceableException() {
	m_size = backtrace( m_trace, 32 );
}

void TraceableException::printBacktrace( int fd ) const {
	backtrace_symbols_fd( m_trace, m_size, fd );
}
#endif
