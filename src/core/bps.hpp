#ifndef SRC_CORE_BPS_HPP_
#define SRC_CORE_BPS_HPP_

#include "src/core/filesystem.hpp"
#include "src/core/rom.hpp"

namespace Bps {
    enum class BpsApplyError : ubyte {
        None,
        InvalidBps,
        PatchFailed,
        NoBaserom,
    };

    extern bool isBpsFile( const fs::path &bpsPath );
    extern BpsApplyError tryApplyBps( fs::path bpsPath, const std::vector<ROM> &romList );
}

#endif /* SRC_CORE_ASYNC_HPP_ */
