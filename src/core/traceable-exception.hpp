#ifndef SRC_CORE_TRACEABLE_EXCEPTION_HPP_
#define SRC_CORE_TRACEABLE_EXCEPTION_HPP_

#if defined(DEBUG) && !defined(_WIN32)
class TraceableException {

	private:
	void *m_trace[32];
	int m_size;

	protected:
	TraceableException();

	public:
	virtual ~TraceableException() {};

	void printBacktrace( int fd = 2 ) const;

};
#else
class TraceableException {
	inline void printBacktrace( [[maybe_unused]] int fd = 2 ) const {}
};
#endif



#endif /* SRC_CORE_TRACEABLE_EXCEPTION_HPP_ */
