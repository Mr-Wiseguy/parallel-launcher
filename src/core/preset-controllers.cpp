#include "preset-controllers.hpp"

#include <unordered_set>

static const HashSet<ControllerId> s_gamecube = {
	{ 0x057E, 0x0337 }, // Official
	{ 0x0079, 0x1844 }  // Mayflash
};

static const HashSet<ControllerId> s_xbox360 = {
	{ 0x045E, 0x028E }, // Official, wired
	{ 0x045E, 0x028F }, // Official, wireless
	{ 0x0E6F, 0x0213 }, // Afterglow
	{ 0x0E6F, 0x0413 }, // Afterglow AX.1
	{ 0x12AB, 0x0301 }, // Afterglow (Hong Kong)
	{ 0x046D, 0x0242 }, // Logitech
	{ 0x0738, 0x4716 }, // Mad Catz, wired
	{ 0x0738, 0x4726 }, // Mad Cats, wireless
	{ 0x0E6F, 0x011F }, // Rock Candy, wired
	{ 0x0E6F, 0x021F }, // Rock Candy, wireless
	{ 0x0F0D, 0x000C }, // Hori
	{ 0x146B, 0x0601 }, // BigBen
	{ 0x15E4, 0x3F0A }, // Numark Airflo
	{ 0x24C6, 0x5300 }, // ThrustMaster (Mini ProEX)
	{ 0x24C6, 0x5303 }, // ThrustMaster (Airflo)
	{ 0x24C6, 0x530A }, // ThrustMaster (ProEX)
	{ 0x24C6, 0xFAFD }, // ThrustMaster (Afterglow)
	{ 0x24C6, 0xFAFE }  // ThrustMaster (Rock Candy)
};

ControllerType getControllerType( const ControllerId &controllerId ) {
	if( s_gamecube.count( controllerId ) > 0 ) {
		return ControllerType::Gamecube;
	}

	if( s_xbox360.count( controllerId ) > 0 ) {
		return ControllerType::XBox360;
	}

	return ControllerType::Other;
}

const ControllerProfile DefaultProfile::Gamecube = {
	/* Name */ "Default Gamecube Profile",
	/* Bindings */ {
		/* AnalogUp */ 		{ BindingType::AxisNegative, 1 },
		/* AnalogDown */ 	{ BindingType::AxisPositive, 1 },
		/* AnalogLeft */ 	{ BindingType::AxisNegative, 0 },
		/* AnalogRight */ 	{ BindingType::AxisPositive, 0 },
		/* CUp */ 			{ BindingType::AxisNegative, 4 },
		/* CDown */ 		{ BindingType::AxisPositive, 4 },
		/* CLeft */ 		{ BindingType::AxisNegative, 3 },
		/* CRight */ 		{ BindingType::AxisPositive, 3 },
		/* DPadUp */ 		{ BindingType::Button, 8 },
		/* DPadDown */		{ BindingType::Button, 9 },
		/* DPadLeft */		{ BindingType::Button, 10 },
		/* DPadRight */		{ BindingType::Button, 11 },
		/* ButtonA */		{ BindingType::Button, 0 },
		/* ButtonB */		{ BindingType::Button, 3 },
		/* TriggerL */		{ BindingType::Button, 6 },
		/* TriggerZ */		{ BindingType::AxisPositive, 2 },
		/* TriggerR */		{ BindingType::AxisPositive, 5 },
		/* Start */			{ BindingType::Button, 7 },
		/* SaveState */		{ BindingType::None, 0 },
		/* LoadState */		{ BindingType::None, 0 }
	},
	/* Sensitivity */ 1.15,
	/* Deadzone */ 0.15,
	/* Rumble */ false
};

const ControllerProfile DefaultProfile::XBox360 = {
	/* Name */ "Default XBox360 Profile",
	/* Bindings */ {
		/* AnalogUp */ 		{ BindingType::AxisNegative, 1 },
		/* AnalogDown */ 	{ BindingType::AxisPositive, 1 },
		/* AnalogLeft */ 	{ BindingType::AxisNegative, 0 },
		/* AnalogRight */ 	{ BindingType::AxisPositive, 0 },
		/* CUp */ 			{ BindingType::AxisNegative, 3 },
		/* CDown */ 		{ BindingType::AxisPositive, 3 },
		/* CLeft */ 		{ BindingType::AxisNegative, 2 },
		/* CRight */ 		{ BindingType::AxisPositive, 2 },
		/* DPadUp */ 		{ BindingType::Button, 11 },
		/* DPadDown */		{ BindingType::Button, 12 },
		/* DPadLeft */		{ BindingType::Button, 13 },
		/* DPadRight */		{ BindingType::Button, 14 },
		/* ButtonA */		{ BindingType::Button, 0 },
		/* ButtonB */		{ BindingType::Button, 2 },
		/* TriggerL */		{ BindingType::Button, 10 },
		/* TriggerZ */		{ BindingType::AxisPositive, 4 },
		/* TriggerR */		{ BindingType::AxisPositive, 5 },
		/* Start */			{ BindingType::Button, 6 },
		/* SaveState */		{ BindingType::None, 0 },
		/* LoadState */		{ BindingType::None, 0 }
	},
	/* Sensitivity */ 1.0,
	/* Deadzone */ 0.15,
	/* Rumble */ false
};

bool DefaultProfile::exists( const string &name ) {
	return name == DefaultProfile::Gamecube.name || name == DefaultProfile::XBox360.name;
}

