#include "src/core/bps.hpp"
#include "src/thirdparty/libbps.h"

#include <fstream>
#include <ios>
#include <cstring>

class ParallelBpsFile : public file {

    private:
    size_t m_len;
    std::ifstream m_file;

    public:
    ParallelBpsFile( const fs::path &bpsPath ) {
        m_len = fs::file_size( bpsPath );
        m_file.open( bpsPath.string(), std::ios_base::in | std::ios_base::binary );
    }

    ~ParallelBpsFile() { m_file.close(); }

    size_t len() override {
        return m_len;
    }

    bool read(uint8_t* target, size_t start, size_t len) override {
        m_file.seekg( std::streampos( start ) );
        m_file.read( (char*) target, len );
        return true;
    }
};

bool Bps::isBpsFile( const fs::path &bpsPath ) {
	char fileHeader[4];
    std::ifstream bpsFile ( bpsPath.string(), std::ios_base::in | std::ios_base::binary );

	bpsFile.read( fileHeader, 4 );
	if( !bpsFile.good() ) return false;

	bpsFile.close();

    return strncmp(fileHeader, "BPS1", 4) == 0;
}

Bps::BpsApplyError Bps::tryApplyBps( fs::path bpsPath, const std::vector<ROM> &romList ) {
    ParallelBpsFile bpsFile ( bpsPath );
    bpsinfo info = bps_get_info( &bpsFile, false );
    fs::path baseromPath;
    fs::path outputPath;
    std::ifstream inputStream;
    std::ofstream outputStream;
    bool foundRom = false;
    mem patchMem, outMem, inMem;
    bpserror patchErr;

    if ( info.error != bpserror::bps_ok ) {
        return Bps::BpsApplyError::InvalidBps;
    }

    for ( const ROM &rom : romList ) {
        if ( rom.crc32 == info.crc_in ) {
            baseromPath = rom.path;
            foundRom = true;
            break;
        }
    }

    if ( !foundRom ) {
        return Bps::BpsApplyError::NoBaserom;
    }

    outputPath = bpsPath;
    outputPath.replace_extension(".z64");

    patchMem.len = bpsFile.len();
    patchMem.ptr = new uint8_t[ patchMem.len ];
    bpsFile.read( patchMem.ptr, 0, patchMem.len );

    inMem.len = info.size_in;
    inMem.ptr = new uint8_t[ inMem.len ];
    
    inputStream.open( baseromPath, std::ios_base::in | std::ios_base::binary );
    inputStream.read( (char*)inMem.ptr, inMem.len );
    inputStream.close();

    outMem.len = info.size_out;
    outMem.ptr = new uint8_t[ outMem.len ];

    patchErr = bps_apply( patchMem, inMem, &outMem, nullptr, false );
    if ( patchErr != bpserror::bps_ok ) {
        return Bps::BpsApplyError::PatchFailed;
    }

    outputStream.open( outputPath, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc );
    outputStream.write( (const char*)outMem.ptr, outMem.len );
    outputStream.close();

    delete[] patchMem.ptr;
    delete[] inMem.ptr;
    delete[] outMem.ptr;

    return Bps::BpsApplyError::None;
}
