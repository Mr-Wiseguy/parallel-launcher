#include "src/core/retroarch.hpp"

#include "src/polyfill/base-directory.hpp"
#include "src/core/file-controller.hpp"
#include "src/core/numeric-string.hpp"
#include "src/core/hotkeys.hpp"
#include "src/input/gamepad-controller.hpp"
#include "src/input/keyboard.hpp"
#include <map>
#include <queue>
#include <unordered_map>
#include <cstdio>
#include <fstream>
#include <regex>
#include <cstring>
#include <cassert>

#ifndef _WIN32

#include <QFile>
#include <cstdlib>

bool RetroArch::hasFlatpakInstall() {
	return(
		std::system( "which flatpak > /dev/null" ) == 0 &&
		std::system( "flatpak info org.libretro.RetroArch > /dev/null" ) == 0
	);
}

fs::path RetroArch::configPath( bool useFlatpakInstall ) {
	if( useFlatpakInstall ) {
		return BaseDir::home() / ".var/app/org.libretro.RetroArch/config/retroarch/retroarch.cfg";
	} else {
		return BaseDir::config().parent_path() / "retroarch/retroarch.cfg";
	}
}

static inline string escapePath( const fs::path &path ) {
	string escapedPath = "";
	for( const char c : path.string() ) {
#ifdef _WIN32
		if( c == '\"' || c == '\\' ) {
#else
		if( c == '\"' || c == '\\' || c == '`' || c == '$' ) {
#endif
			escapedPath += '\\';
		}
		escapedPath += c;
	}
	return escapedPath;
}

#endif

static fs::path expand_path_helper( const fs::path &path ) {
	const string pathStr = path.string();
	if( pathStr == "~" ) {
		return BaseDir::home();
	} else if( pathStr.length() >= 2 && pathStr[0] == '~' && (pathStr[1] == '/' || pathStr[1] == '\\' ) ) {
		return BaseDir::home() / fs::path( &path.c_str()[2] );
	} else return path;
}

static inline fs::path getBaseConfigDirectory() {
	const AppSettings &settings = FileController::loadAppSettings();
#ifdef _WIN32
	return expand_path_helper( settings.retroInstallDir );
#else
	return RetroArch::configPath( settings.usingFlatpak ).parent_path();
#endif
}

static fs::path expand_path( const fs::path &path ) {
	const string pathStr = path.string();
	if( pathStr == ":" ) {
		return getBaseConfigDirectory();
	} else if( pathStr.length() >= 2 && pathStr[0] == ':' && (pathStr[1] == '/' || pathStr[1] == '\\' ) ) {
		return getBaseConfigDirectory() / fs::path( &path.c_str()[2] );
	} else return expand_path_helper( path );
}

static inline bool stringEndsWith( const string &str, const string &suffix ) {
	if( str.length() < suffix.length() ) return false;
	return std::strcmp( &str.c_str()[str.length() - suffix.length()], suffix.c_str() ) == 0;
}

static const char* s_plugins[] = {
	"auto",
	"parallel",
	"glide64",
	"angrylion",
	"gln64",
	"rice"
};

ubyte RetroArch::resolveUpscaling(
	ParallelUpscaling requestedUpscaling,
	ubyte windowScale
) noexcept {
	if( requestedUpscaling != ParallelUpscaling::Auto ) return (ubyte)requestedUpscaling;
	if( windowScale <= 1 ) return 1;
	if( windowScale == 2 ) return 2;
	if( windowScale < 8 ) return 4;
	return 8;
}

static const std::regex s_cfgRegex(
	"^([^=]+)\\s*=\\s*\"([^\"]*)\"",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static void loadConfigHelper( const fs::path &configPath, std::map<string,string> &configs ) {
	if( !fs::exists( configPath ) ) return;
	std::ifstream configFile( configPath.string() );
	while( configFile.good() && !configFile.eof() ) {
		string line;
		std::getline( configFile, line );

		std::smatch matches;
		if( std::regex_search( line, matches, s_cfgRegex ) ) {
			string key = matches[1];
			while( !key.empty() && key.back() == ' ') key.pop_back();
			if( !key.empty() ) {
				configs[key] = matches[2];
			}
		}
	}
}

static void loadBaseConfig(
	std::map<string,string> &configs,
	bool includeCoreOptions
) {
	const fs::path baseConfigPath = getBaseConfigDirectory() / "retroarch.cfg";
	loadConfigHelper( baseConfigPath, configs );
	if( includeCoreOptions ) {
		loadConfigHelper( baseConfigPath.parent_path() / "retroarch-core-options.cfg", configs );
	}
}

static void loadConfig(
	const AppSettings &settings,
	std::map<string, string> &configs
) {
	if( settings.configBehaviour == ConfigBehaviour::Inherited ) {
		loadBaseConfig( configs, true );
	} else if( settings.configBehaviour == ConfigBehaviour::Persistent ) {
		loadConfigHelper( BaseDir::data() / "retroarch.cfg", configs );
		loadConfigHelper( BaseDir::data() / "retroarch-core-options.cfg", configs );
	}

	if( settings.configBehaviour != ConfigBehaviour::Inherited ) {
		std::map<string,string> baseConfig;
		loadBaseConfig( baseConfig, false );

		for( const auto &i : baseConfig ) {
			if( configs.count( i.first ) == 0 && (
				stringEndsWith( i.first, "_dir" ) ||
				stringEndsWith( i.first, "_path" ) ||
				stringEndsWith( i.first, "_directory" )
			)) {
				configs[i.first] = i.second;
			}
		}
	}
}

static void saveConfig( const std::map<string,string> &configs ) {
	std::ofstream mainConfigFile( (BaseDir::data() / "retroarch.cfg").string(), std::ios_base::out | std::ios_base::trunc );
	std::ofstream coreConfigFile( (BaseDir::data() / "retroarch-core-options.cfg").string(), std::ios_base::out | std::ios_base::trunc );

	for( const auto &i : configs ) {
		const string &key = i.first;
		const string &value = i.second;

		if(
			std::strncmp( key.c_str(), "parallel-n64-", sizeof( "parallel-n64-" ) - 1 ) == 0 ||
			std::strncmp( key.c_str(), "mupen64plus-", sizeof( "mupen64plus-" ) - 1 ) == 0
		) {
			coreConfigFile << key << " = \"" << value << '"' << std::endl;
		} else {
			mainConfigFile << key << " = \"" << value << '"' << std::endl;
		}
	}

	mainConfigFile << std::endl << std::flush;
	coreConfigFile << std::endl << std::flush;
}

static const char *s_inputNames[] = {
	"l_y_minus", // AnalogUp
	"l_y_plus", // AnalogDown
	"l_x_minus", // AnalogLeft
	"l_x_plus", // AnalogRight
	"r_y_minus", // CUp
	"r_y_plus", // CDown
	"r_x_minus", // CLeft
	"r_x_plus", // CRight
	"up", // DPadUp
	"down", // DPadDown
	"left", // DPadLeft
	"right", // DPadRight
	"b", // ButtonA
	"y", // ButtonB
	"l", // TriggerL
	"l2", // TriggerZ
	"r", // TriggerR
	"start" // Start
};

static void bindInput( std::map<string,string> &configs, const string &key, const Binding &binding ) {
	const string buttonKey = key + "_btn";
	const string axisKey = key + "_axis";

	// 99 and +99 are used instead of nul to prevent default bindings from messing things up
	switch( binding.type ) {
		case BindingType::None:
			configs[buttonKey] = "99";
			configs[axisKey] = "+99";
			break;
		case BindingType::Button:
			configs[buttonKey] = Number::toString( binding.buttonOrAxis );
			configs[axisKey] = "+99";
			break;
		case BindingType::AxisNegative:
			configs[buttonKey] = "99";
			configs[axisKey] = "-"s + Number::toString( binding.buttonOrAxis );
			break;
		case BindingType::AxisPositive:
			configs[buttonKey] = "99";
			configs[axisKey] = "+"s + Number::toString( binding.buttonOrAxis );
			break;
	}
}

static inline HashMap<Uuid,std::queue<int>> getDevicePorts( [[maybe_unused]] const AppSettings &settings, int &numMapped ) {
	HashMap<Uuid,std::queue<int>> devicePorts;
	numMapped = 0;

	/* On Windows, lsjs needs to be in the same directory as the libraries it depends on.
	 * On Linux, it can be anywhere; however, if RetroArch is running in a Flatpak sandbox,
	 * then lsjs needs to be somewhere that sandbox can access.
	 *
	 * On Windows builds, lsjs is installed to the same directory as the main exe by the
	 * installer and just runs there. On Linux, I instead embed this helper program, then
	 * write it to a temporary folder accessible by Flatpak sandboxes.
	 */

	bool lsjsFailed = false;
	try {
#ifdef _WIN32
		const string lsjsCmd = (BaseDir::program() / "parallel-launcher-lsjs.exe").string();
#else
		const fs::path lsjsPath = BaseDir::homeTemp() / "lsjs";
		if( !fs::exists( lsjsPath ) ) {
			QFile lsjsBin( ":/parallel-launcher-lsjs" );
			lsjsBin.open( QFile::ReadOnly );
			QByteArray lsjsData = lsjsBin.readAll();

			QFile lsjsTarget( lsjsPath.c_str() );
			lsjsTarget.open( QFile::WriteOnly | QFile::Truncate );
			lsjsTarget.setPermissions( QFileDevice::ReadOwner | QFileDevice::ExeOwner | QFileDevice::ReadGroup | QFileDevice::ExeGroup | QFileDevice::ReadOther | QFileDevice::ExeOther );
			lsjsTarget.write( lsjsData );
		}

		const string lsjsCmd = settings.usingFlatpak ?
			("flatpak run --filesystem=home --command=\""s + escapePath( lsjsPath ) + "\" org.libretro.RetroArch") :
			escapePath( lsjsPath );
#endif

		string uuidList;
		lsjsFailed = !Process::tryGetOutput( lsjsCmd, uuidList );
		if( !lsjsFailed ) {

			size_t lineStart = 0;
			for( int i = 0; lineStart < uuidList.length(); i++ ) {
				size_t lineEnd = uuidList.find( '\n', lineStart );
				if( lineEnd == string::npos ) {
					lineEnd = uuidList.length();
				}

				Uuid uuid;
				if( Uuid::tryParse( uuidList.substr( lineStart, lineEnd - lineStart ), uuid ) ) {
					devicePorts[uuid].push( i );
					numMapped++;
				}

				lineStart = lineEnd + 1;
			}
		}
	} catch( ... ) {
		lsjsFailed = true;
	}

	if( lsjsFailed ) {
		std::cerr << "Failed to run parallel-launcher-lsjs. Falling back to default port ordering. Controllers may not be bound correctly." << std::endl << std::flush;

		const std::vector<ConnectedGamepad> fallbackPortOrder = GamepadController::instance().getConnected();
		devicePorts.clear();
		devicePorts.reserve( fallbackPortOrder.size() );

		numMapped = (int)fallbackPortOrder.size();
		for( size_t i = 0; i < fallbackPortOrder.size(); i++ ) {
			devicePorts[fallbackPortOrder[i].info.uuid].push( (int)i );
		}
	}

	return devicePorts;
}

AsyncProcess RetroArch::launchRom(
	fs::path romPath,
	const AppSettings &settings,
	const std::vector<PlayerController> &players,
	EmulatorCore emulator,
	GfxPlugin plugin,
	bool overclockCPU,
	bool overclockVI,
	bool bindSavestate
) {
	std::map<string,string> configs;
	loadConfig( settings, configs );

	const string width = Number::toString( 320 * (int)settings.windowScale );
	const string height = Number::toString( 240 * (int)settings.windowScale );
	assert( emulator != EmulatorCore::UseDefault );
	assert( plugin != GfxPlugin::UseDefault );

	if( overclockCPU || overclockVI ) {
		configs["video_frame_delay"] = "0";
	}

	configs["video_fullscreen"] = "false";
	configs["suspend_screensaver_enable"] = "true";
	configs["video_force_aspect"] = "true";
	configs["video_window_save_positions"] = "true";
	configs["parallel-n64-cpucore"] = "dynamic_recompiler";
	configs["mupen64plus-cpucore"] = "dynamic_recompiler";
	configs["parallel-n64-gfxplugin-accuracy"] = "veryhigh";
	configs["parallel-n64-parallel-rdp-native-texture-lod"] = "true";
	configs["mupen64plus-parallel-rdp-native-texture-lod"] = "True";
	configs["global_core_options"] = "true";
	configs["config_save_on_exit"] = "true";
	configs["save_file_compression"] = "false";
	configs["video_shader_enable"] = "false";
	configs["mupen64plus-ForceDisableExtraMem"] = "False";

	configs["video_scale_integer"] = (plugin == GfxPlugin::ParaLLEl) ? "true" : "false";
	configs["video_vsync"] = settings.vsync ? "true" : "false";
	configs["video_windowed_position_width"] = configs["custom_viewport_width"] = width;
	configs["video_windowed_position_height"] = configs["custom_viewport_height"] = height;
	configs["mupen64plus-aspect"] = "4:3";
	configs["parallel-n64-screensize"] = width + "x" + height;
	configs["mupen64plus-43screensize"] = width + "x" + height;
	configs["parallel-n64-framerate"] = overclockCPU ? "fullspeed" : "original";
	configs["mupen64plus-Framerate"] = overclockCPU ? "Fullspeed" : "Original";
	configs["mupen64plus-CountPerOp"] = overclockCPU ? "1" : "2";
	configs["parallel-n64-virefresh"] = overclockVI ? "2200" : "auto";
	configs["mupen64plus-virefresh"] = overclockVI ? "2200" : "Auto";
	configs["parallel-n64-parallel-rdp-upscaling"] = Number::toString( (int)resolveUpscaling( settings.upscaling, settings.windowScale ) ) + "x";
	configs["mupen64plus-parallel-rdp-upscaling"] = Number::toString( (int)resolveUpscaling( settings.upscaling, settings.windowScale ) ) + "x";
	configs["parallel-n64-gfxplugin"] = s_plugins[(int)plugin];
	configs["mupen64plus-rdp-plugin"] = s_plugins[(int)plugin];
	configs["mupen64plus-rsp-plugin"] = (plugin == GfxPlugin::GlideN64) ? "hle" : "parallel";
	configs["parallel-n64-filtering"] = "automatic";
	configs["mupen64plus-EnableLODEmulation"] = "True";
	configs["mupen64plus-EnableCopyAuxToRDRAM"] = "True";

	configs["parallel-n64-astick-deadzone"] = "0";
	configs["parallel-n64-astick-sensitivity"] = "100";
	configs["mupen64plus-astick-deadzone"] = "0";
	configs["mupen64plus-astick-sensitivity"] = "100";

	char floatStr[10];
	std::snprintf( floatStr, sizeof( floatStr ), "%0.6f", players.at( 0 ).profile.deadzone );
	configs["input_analog_deadzone"] = string( floatStr );
	std::snprintf( floatStr, sizeof( floatStr ), "%0.6f", players.at( 0 ).profile.sensitivity );
	configs["input_analog_sensitivity"] = string( floatStr );

	configs["input_remap_binds_enable"] = "false";
	configs["input_joypad_driver"] = "sdl2";
	configs["input_max_users"] = Number::toString( (int)players.size() );

	int fallbackPort;
	HashMap<Uuid,std::queue<int>> devicePorts = getDevicePorts( settings, fallbackPort );
	bool enableRumble = false;
	for( int i = 0; i < (int)players.size(); i++ ) {
		const char pc = '1' + (char)i;

		const string indexKey = "input_player"s + pc + "_joypad_index";
		if( !devicePorts[players[i].deviceUuid].empty() ) {
			configs[indexKey] = Number::toString( devicePorts.at( players[i].deviceUuid ).front() );
			devicePorts.at( players[i].deviceUuid ).pop();
		} else {
			configs[indexKey] = Number::toString( fallbackPort++ );
		}

		const string rumbleKey1 = "parallel-n64-pak"s + pc;
		const string rumbleKey2 = "mupen64plus-pak"s + pc;
		configs[rumbleKey1] = players[i].profile.rumble ? "rumble" : "none";
		configs[rumbleKey2] = players[i].profile.rumble ? "rumble" : "none";
		enableRumble |= players[i].profile.rumble;

		for( int j = 0; j <= (int)ControllerAction::Start; j++ ) {
			const string key = "input_player"s + pc + '_' + s_inputNames[j];
			bindInput( configs, key, players[i].profile.bindings[j] );
		}

		bindInput( configs, "input_player"s + pc + "_r2"s, { BindingType::None, 0 } );
		bindInput( configs, "input_player"s + pc + "_turbo"s, { BindingType::None, 0 } );
	}

	if( bindSavestate ) {
		bindInput( configs, "input_load_state", players.at( 0 ).profile.bindings[(int)ControllerAction::LoadState] );
		bindInput( configs, "input_save_state", players.at( 0 ).profile.bindings[(int)ControllerAction::SaveState] );
	} else {
		configs["input_load_state_btn"] = "nul";
		configs["input_load_state_axis"] = "nul";
		configs["input_save_state_btn"] = "nul";
		configs["input_save_state_axis"] = "nul";
	}

	configs["enable_device_vibration"] = enableRumble ? "true" : "false";

	configs["input_desktop_menu_toggle"] = "nul";
	configs["input_grab_mouse_toggle"] = "nul";
	configs["input_game_focus_toggle"] = "nul";
	configs["input_send_debug_info"] = "nul";
	configs["input_shader_next"] = "nul";
	configs["input_shader_prev"] = "nul";
	configs["input_netplay_game_watch"] = "nul";
	configs["input_netplay_host_toggle"] = "nul";
	configs["input_osk_toggle"] = "nul";

	const std::vector<int> hotkeys = FileController::loadHotkeys();
	assert( hotkeys.size() == (size_t)Hotkey::NUM_HOTKEYS );
	for( ubyte i = 0; i < (ubyte)Hotkey::NUM_HOTKEYS; i++ ) {
		configs[Hotkeys::ConfigNames[i]] = Keycode::getNames( hotkeys[i] ).retroConfigName;
	}

	saveConfig( configs );

	const char *const coreName = (emulator == EmulatorCore::ParallelN64) ? "parallel_n64_libretro" : "mupen64plus_next_libretro";

	fs::path corePath;
	if( configs.count( "libretro_directory" ) > 0 ) {
		corePath = expand_path( configs["libretro_directory"] ) / coreName;
	} else {
#ifdef _WIN32
		corePath = expand_path_helper( settings.retroInstallDir ) / "cores" / coreName;
#else
		corePath = RetroArch::configPath( settings.usingFlatpak ).parent_path() / "cores" / coreName;
#endif
	}

#ifdef _WIN32
	corePath += ".dll";
#else
	corePath += ".so";
#endif

	std::vector<string> args;
	args.reserve( 7 );
#ifndef _WIN32
	if( settings.usingFlatpak ) {
		args.insert( args.end(), { "run"s, "-p"s, "org.libretro.RetroArch"s });
	}
#endif
	args.insert( args.end(), { "-L"s, corePath.string(), "--config"s, (BaseDir::data() / "retroarch.cfg").string(), romPath.string() } );

	return AsyncProcess(
#ifdef _WIN32
		(expand_path_helper( settings.retroInstallDir ) / "retroarch.exe").string(),
#else
		settings.usingFlatpak ? "flatpak" : "retroarch",
#endif
		args
	);
}

static const std::regex s_falseRegex(
	"^\\s*false\\s*$",
	std::regex_constants::ECMAScript | std::regex_constants::optimize | std::regex_constants::icase
);

static fs::path s_cachedSaveDir;
static fs::file_time_type s_baseConfigLastModified;
static fs::file_time_type s_mainConfigLastModified;
static ConfigBehaviour s_lastConfigBehaviour = (ConfigBehaviour)0xFF;
#ifdef _WIN32
static fs::path s_lastRetroInstallDir;
#else
static bool s_lastUsingFlatpak;
#endif

static inline fs::path getSaveDir( const AppSettings &settings ) {
#ifdef _WIN32
	const fs::path baseConfigPath = expand_path_helper( settings.retroInstallDir ) / "retroarch.cfg";
#else
	const fs::path baseConfigPath = RetroArch::configPath( settings.usingFlatpak );
#endif
	const fs::path mainConfigPath = BaseDir::data() / "retroarch.cfg";

	std::error_code err;
	const fs::file_time_type baseConfigLastModified = fs::last_write_time( baseConfigPath, err );
	const fs::file_time_type mainConfigLastModified = fs::last_write_time( mainConfigPath, err );
	if(
		settings.configBehaviour == s_lastConfigBehaviour &&
		baseConfigLastModified == s_baseConfigLastModified &&
		(settings.configBehaviour != ConfigBehaviour::Persistent || mainConfigLastModified == s_baseConfigLastModified) &&
#ifdef _WIN32
		settings.retroInstallDir == s_lastRetroInstallDir &&
#else
		settings.usingFlatpak == s_lastUsingFlatpak &&
#endif
		!err
	) {
		return s_cachedSaveDir;
	}

	std::map<string,string> configs;
	loadConfig( settings, configs );

	fs::path saveDir;
	if( std::regex_match( configs["savefiles_in_content_dir"], s_falseRegex ) ) {
		saveDir = configs["savefile_directory"];
	}

	s_cachedSaveDir = saveDir;
	s_baseConfigLastModified = baseConfigLastModified;
	s_mainConfigLastModified = mainConfigLastModified;
	s_lastConfigBehaviour = settings.configBehaviour;
#ifdef _WIN32
	s_lastRetroInstallDir = settings.retroInstallDir;
#else
	s_lastUsingFlatpak = settings.usingFlatpak;
#endif
	return saveDir;
}

fs::path RetroArch::getSaveFilePath(
	const AppSettings &settings,
	const fs::path &romPath
) {
	const fs::path saveDir = getSaveDir( settings );
	if( saveDir.empty() ) {
		fs::path saveFilePath = expand_path( romPath );
		saveFilePath.replace_extension( ".srm" );
		return saveFilePath;
	} else {
		fs::path saveFilePath = expand_path( saveDir / romPath.filename() );
		saveFilePath.replace_extension( ".srm" );
		return saveFilePath;
	}

}

bool RetroArch::isRetroArchInstalled() {
	AppSettings settings = FileController::loadAppSettings();
#ifdef _WIN32
	return fs::is_regular_file( expand_path_helper( settings.retroInstallDir ) / "RetroArch.exe" );
#else
	if( settings.usingFlatpak ) {
		if( hasFlatpakInstall() ) {
			return true;
		} else if( std::system( "which retroarch > /dev/null" ) == 0 ) {
			settings.usingFlatpak = false;
			FileController::saveAppSettings( settings );
			return true;
		}
	} else if( std::system( "which retroarch > /dev/null" ) == 0 ) {
		return true;
	} else if( hasFlatpakInstall() ) {
		settings.usingFlatpak = true;
		FileController::saveAppSettings( settings );
		return true;
	}

	return false;
#endif
}

static bool isCoreInstalled( const char *core ) {
	std::map<string,string> configs;
	loadBaseConfig( configs, false );

	fs::path corePath = expand_path( fs::path( configs["libretro_directory"] ) );
	if( corePath.empty() ) return false;

	corePath = corePath / core;
#ifdef _WIN32
	corePath += ".dll";
#else
	corePath += ".so";
#endif

	return fs::is_regular_file( corePath );
}

bool RetroArch::isParallelN64Installed() {
	return isCoreInstalled( "parallel_n64_libretro" );
}

bool RetroArch::isMupenInstalled() {
	return isCoreInstalled( "mupen64plus_next_libretro" );
}

static const std::regex s_versionRegex(
	"^RetroArch.*v(\\d+)\\.(\\d+)(?:\\.\\d+)*(?:\\s|$)",
	std::regex_constants::ECMAScript | std::regex_constants::optimize | std::regex_constants::icase
);

bool RetroArch::tryGetVersion( ushort &versionMajor, ushort &versionMinor ) {
	AppSettings settings = FileController::loadAppSettings();

	string versionOutput;
	versionOutput.reserve( 128 );

	if( !Process::tryGetErrorOutput(
#ifdef _WIN32
		(expand_path_helper( settings.retroInstallDir ) / "retroarch.exe").string() + " --version",
#else
		settings.usingFlatpak ? "flatpak run org.libretro.RetroArch --version" : "retroarch --version",
#endif
		versionOutput
	)) return false;

	size_t lineStart = 0;
	while( lineStart < versionOutput.length() ) {
		size_t lineEnd = versionOutput.find( '\n', lineStart );
		if( lineEnd == string::npos ) {
			lineEnd = versionOutput.length();
		}

		std::smatch match;
		const string line = versionOutput.substr( lineStart, lineEnd - lineStart );
		if( std::regex_search( line, match, s_versionRegex ) ) {
			versionMajor = (ushort)Number::parseUInt( match[1] );
			versionMinor = (ushort)Number::parseUInt( match[2] );
			return true;
		}

		lineStart = lineEnd + 1;
	}

	return false;
}
