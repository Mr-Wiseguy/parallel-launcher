#ifndef SRC_CORE_FILESYSTEM_HPP_
#define SRC_CORE_FILESYSTEM_HPP_

#include <filesystem>
#include <functional>

namespace fs = std::filesystem;

template<> struct std::hash<fs::path> {
	inline size_t operator()(const fs::path &path) const noexcept {
		return hash_value( path );
	}
};

#endif /* SRC_CORE_FILESYSTEM_HPP_ */
