#ifndef SRC_CORE_RETROARCH_HPP_
#define SRC_CORE_RETROARCH_HPP_

#include "src/types.hpp"
#include "src/core/controller.hpp"
#include "src/core/settings.hpp"
#include "src/polyfill/process.hpp"

namespace RetroArch {

#ifndef _WIN32
	extern bool hasFlatpakInstall();
	extern fs::path configPath( bool useFlatpakInstall );
#endif

	extern ubyte resolveUpscaling(
		ParallelUpscaling requestedUpscaling,
		ubyte windowScale
	) noexcept;

	extern AsyncProcess launchRom(
		fs::path romPath,
		const AppSettings &settings,
		const std::vector<PlayerController> &players,
		EmulatorCore emulator,
		GfxPlugin plugin,
		bool overclockCPU,
		bool overclockVI,
		bool bindSavestate
	);

	extern fs::path getSaveFilePath(
		const AppSettings &settings,
		const fs::path &romPath
	);

	extern bool isRetroArchInstalled();
	extern bool isParallelN64Installed();
	extern bool isMupenInstalled();
	extern bool tryGetVersion( ushort &versionMajor, ushort &versionMinor );

}

#endif /* SRC_CORE_RETROARCH_HPP_ */
