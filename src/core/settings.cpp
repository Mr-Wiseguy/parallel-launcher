#include "src/core/settings.hpp"
#include "src/core/flags.hpp"

const AppSettings AppSettings::Default = {
	/* visibleColumns */ RomInfoColumn::LastPlayed | RomInfoColumn::PlayTime,
	/* defaultParallelPlugin */ GfxPlugin::ParaLLEl,
	/* defaultMupenPlugin */ GfxPlugin::ParaLLEl,
	/* defaultEmulator */ EmulatorCore::ParallelN64,
	/* upscaling */ ParallelUpscaling::Auto,
	/* configBehaviour */ ConfigBehaviour::Persistent,
	/* windowScale */ 4,
	/* vsync */ false,
	/* hideWhenPlaying */ false,
#ifdef _WIN32
	/* retroInstallDir */ fs::path( "~\\AppData\\Roaming\\RetroArch" ),
#else
	/* usingFlatpak */ false,
#endif
	/* preferredController */ std::nullopt,
#ifdef _WIN32
	/* windowsTheme */ "Fusion"
#endif
};

static constexpr char P_RETRO_INSTALL_PATH[] = "retroarch_install_path";
static constexpr char P_VISIBLE_COLUMNS[] = "visible_columns";
static constexpr char P_DEFAULT_PARALLEL_PLUGIN[] = "default_gfx_plugin";
static constexpr char P_DEFAULT_MUPEN_PLUGIN[] = "default_gfx_plugin_mupen";
static constexpr char P_DEFAULT_EMULATOR[] = "default_emulator";
static constexpr char P_UPSCALING[] = "parallel_upscaling";
static constexpr char P_CONFIG_BEHAVIOUR[] = "config_behaviour";
static constexpr char P_WINDOW_SCALE[] = "window_scale";
static constexpr char P_VSYNC[] = "enable_vsync";
static constexpr char P_HIDE_WHILE_PLAYING[] = "hide_while_playing";
static constexpr char P_USING_FLATPAK[] = "using_flatpak_install";
static constexpr char P_PREFERRED_CONTROLLER[] = "preferred_controller";
static constexpr char P_WINDOWS_THEME[] = "windows_theme";

template<> void JsonSerializer::serialize<AppSettings>( JsonWriter &jw, const AppSettings &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_VISIBLE_COLUMNS, obj.visibleColumns );
	jw.writeProperty( P_DEFAULT_PARALLEL_PLUGIN, obj.defaultParallelPlugin );
	jw.writeProperty( P_DEFAULT_MUPEN_PLUGIN, obj.defaultMupenPlugin );
	jw.writeProperty( P_DEFAULT_EMULATOR, obj.defaultEmulator );
	jw.writeProperty( P_UPSCALING, obj.upscaling );
	jw.writeProperty( P_CONFIG_BEHAVIOUR, obj.configBehaviour );
	jw.writeProperty( P_WINDOW_SCALE, obj.windowScale );
	jw.writeProperty( P_VSYNC, obj.vsync );
	jw.writeProperty( P_HIDE_WHILE_PLAYING, obj.hideWhenPlaying );
#ifdef _WIN32
	jw.writeProperty( P_RETRO_INSTALL_PATH, obj.retroInstallDir.string() );
#else
	jw.writeProperty( P_USING_FLATPAK, obj.usingFlatpak );
#endif
	jw.writePropertyName( P_PREFERRED_CONTROLLER );
	if( obj.preferredController.has_value() ) {
		jw.writeString( obj.preferredController.value().toString() );
	} else {
		jw.writeNull();
	}
#ifdef _WIN32
	jw.writeProperty( P_WINDOWS_THEME, obj.windowsTheme );
#endif
	jw.writeObjectEnd();
}

template<> AppSettings JsonSerializer::parse<AppSettings>( const Json &json ) {
	return AppSettings{
		json[P_VISIBLE_COLUMNS].getOrDefault<RomInfoColumn>( AppSettings::Default.visibleColumns ),
		json[P_DEFAULT_PARALLEL_PLUGIN].getOrDefault<GfxPlugin>( AppSettings::Default.defaultParallelPlugin ),
		json[P_DEFAULT_MUPEN_PLUGIN].getOrDefault<GfxPlugin>( AppSettings::Default.defaultMupenPlugin ),
		json[P_DEFAULT_EMULATOR].getOrDefault<EmulatorCore>( AppSettings::Default.defaultEmulator ),
		json[P_UPSCALING].getOrDefault<ParallelUpscaling>( AppSettings::Default.upscaling ),
		json[P_CONFIG_BEHAVIOUR].getOrDefault<ConfigBehaviour>( AppSettings::Default.configBehaviour ),
		json[P_WINDOW_SCALE].getOrDefault<ubyte>( AppSettings::Default.windowScale ),
		json[P_VSYNC].getOrDefault<bool>( AppSettings::Default.vsync ),
		json[P_HIDE_WHILE_PLAYING].getOrDefault<bool>( AppSettings::Default.hideWhenPlaying ),
#ifdef _WIN32
		std::filesystem::path( json[P_RETRO_INSTALL_PATH].getOrDefault<string>( AppSettings::Default.retroInstallDir.string() ) ),
#else
		json[P_USING_FLATPAK].getOrDefault<bool>( AppSettings::Default.usingFlatpak ),
#endif
		(json[P_PREFERRED_CONTROLLER].exists() && !json[P_PREFERRED_CONTROLLER].isNull()) ?
			Uuid::parse( json[P_PREFERRED_CONTROLLER].get<string>() ) : std::optional<Uuid>(),
#ifdef _WIN32
		json[P_WINDOWS_THEME].getOrDefault<string>( AppSettings::Default.windowsTheme )
#endif
	};
}
