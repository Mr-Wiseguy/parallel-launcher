#ifndef SRC_CORE_CONTROLLER_HPP_
#define SRC_CORE_CONTROLLER_HPP_

#include <functional>
#include "src/types.hpp"
#include "src/core/uuid.hpp"
#include "src/core/json.hpp"

enum class ControllerAction : ubyte {
	AnalogUp,
	AnalogDown,
	AnalogLeft,
	AnalogRight,
	CUp,
	CDown,
	CLeft,
	CRight,
	DPadUp,
	DPadDown,
	DPadLeft,
	DPadRight,
	ButtonA,
	ButtonB,
	TriggerL,
	TriggerZ,
	TriggerR,
	Start,

	SaveState,
	LoadState,

	NUM_ACTIONS
};

enum class BindingType : ubyte {
	None = 0,
	Button = 2,
	AxisPositive = 3,
	AxisNegative = 4
};

struct Binding {
	BindingType type;
	ushort buttonOrAxis;
};

struct ControllerId {
	ushort vendorId;
	ushort productId;

	inline bool operator==( const ControllerId &other ) const noexcept {
		return productId == other.productId && vendorId == other.vendorId;
	}

	inline bool operator!=( const ControllerId &other ) const noexcept {
		return productId != other.productId || vendorId != other.vendorId;
	}
};

template<> struct std::hash<ControllerId> {
	inline size_t operator()(const ControllerId &id) const noexcept {
		return ((size_t)id.vendorId << 16) | (size_t)id.productId;
	}
};

template<> struct std::hash<Binding> {
	inline size_t operator()(const Binding &binding) const noexcept {
		return ((size_t)binding.type << 16) | (size_t)binding.buttonOrAxis;
	}
};

struct ControllerInfo {
	string name;
	ControllerId controllerId;
	Uuid uuid;

	ushort numButtons;
	ushort numAxes;
};

struct ControllerProfile {
	string name;
	Binding bindings[(ubyte)ControllerAction::NUM_ACTIONS];
	double sensitivity;
	double deadzone;
	bool rumble;
};

struct PlayerController {
	ControllerProfile profile;
	Uuid deviceUuid;
};

namespace JsonSerializer {
	template<> void serialize<Binding>( JsonWriter &jw, const Binding &obj );
	template<> Binding parse<Binding>( const Json &json );

	template<> void serialize<ControllerProfile>( JsonWriter &jw, const ControllerProfile &obj );
	template<> ControllerProfile parse<ControllerProfile>( const Json &json );
}

#endif /* SRC_CORE_CONTROLLER_HPP_ */
