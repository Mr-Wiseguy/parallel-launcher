#include "src/core/file-controller.hpp"

#include "src/polyfill/base-directory.hpp"
#include "src/core/preset-controllers.hpp"
#include "src/core/hotkeys.hpp"
#include "src/ui/error-notifier.hpp"
#include <fstream>
#include <ios>

template<class T> class FileCache {

	private:
	T m_value;
	const fs::path m_filePath;
	void (*const m_setter)(JsonWriter&, const T &);

	void commit() const {
		try {
			std::ofstream file( m_filePath.string(), std::ios_base::out | std::ios_base::trunc );
#ifdef _WIN32
			char buffer[8096];
			file.rdbuf()->pubsetbuf( buffer, 8096 );
#endif
			JsonWriter writer( &file, true );
			m_setter( writer, m_value );
			file << std::flush;
		} catch( const std::exception &exception ) {
			string msg = "Failed to save data to '"s + m_filePath.string() + "'.\n";
			if( dynamic_cast<const JsonWriterException*>( &exception ) ) {
				msg += "Failed to serialize data to JSON. Please report this bug!\nDetails: ";
			} else if( dynamic_cast<const fs::filesystem_error*>( &exception ) ) {
				msg += "Filesystem Error: ";
			} else if( dynamic_cast<const std::ios_base::failure*>( &exception ) ) {
				msg += "I/O Error: ";
			}
			msg += exception.what();
			ErrorNotifier::alert( msg );
		}
	}

	public:
	FileCache(
		const fs::path &filePath,
		const T &defaultValue,
		T (*getter)(const Json&),
		void (*setter)(JsonWriter&, const T &)
	) : m_filePath( filePath ), m_setter( setter ) {
		if( !fs::is_regular_file( filePath ) ) {
			m_value = defaultValue;
		} else try {
			std::ifstream file( filePath.string() );
#ifdef _WIN32
			char buffer[8096];
			file.rdbuf()->pubsetbuf( buffer, 8096 );
#endif
			m_value = getter( Json::parse( file ) );
		} catch( const std::exception &exception ) {
			string msg = "Failed to load data from '"s + m_filePath.string() + "'.\n";
			if( dynamic_cast<const JsonReaderException*>( &exception ) ) {
				msg += "Failed to parse JSON data. Please report this bug!\nDetails: ";
			} else if( dynamic_cast<const fs::filesystem_error*>( &exception ) ) {
				msg += "Filesystem Error: ";
			} else if( dynamic_cast<const std::ios_base::failure*>( &exception ) ) {
				msg += "I/O Error: ";
			}
			msg += exception.what();
			ErrorNotifier::alert( msg );
			m_value = defaultValue;
		}
	}

	~FileCache() {}

	inline const T &get() const {
		return m_value;
	}

	inline void set( const T &value ) {
		m_value = value;
		commit();
	}

	inline void set( T &&value ) {
		m_value = std::move( value );
		commit();
	}

};

template<class T> static std::vector<T> parseJsonArray( const Json &json ) {
	const JArray &jsonArray = json.array();

	std::vector<T> parsedArray;
	parsedArray.reserve( jsonArray.size() );
	for( const Json &item : jsonArray ) {
		parsedArray.push_back( JsonSerializer::parse<T>( item ) );
	}
	return parsedArray;
}

template<class T> static void serializeToJsonArray( JsonWriter &writer, const std::vector<T> &array ) {
	writer.writeArrayStart();
	for( const T &item : array ) {
		JsonSerializer::serialize<T>( writer, item );
	}
	writer.writeArrayEnd();
}

static std::set<string> parseTags( const Json &json ) {
	const JArray &jsonArray = json.array();

	std::set<string> tags;
	for( const Json &tagJson : jsonArray ) {
		tags.insert( tagJson.get<string>() );
	}
	return tags;
}

static void serializeTags( JsonWriter &writer, const std::set<string> &tags ) {
	writer.writeArrayStart();
	for( const string &tag : tags ) {
		writer.writeString( tag );
	}
	writer.writeArrayEnd();
}

static std::map<string, ControllerProfile> parseProfiles( const Json &json ) {
	const JArray &jsonArray = json.array();

	std::map<string, ControllerProfile> profiles;
	for( const Json &profileJson : jsonArray ) {
		ControllerProfile profile = JsonSerializer::parse<ControllerProfile>( profileJson );
		profiles[profile.name] = profile;
	}
	profiles[DefaultProfile::Gamecube.name] = DefaultProfile::Gamecube;
	profiles[DefaultProfile::XBox360.name] = DefaultProfile::XBox360;
	return profiles;
}

static void serializeProfiles( JsonWriter &writer, const std::map<string, ControllerProfile> &profiles ) {
	writer.writeArrayStart();
	for( const auto &i : profiles ) {
		if( !DefaultProfile::exists( i.first ) ) {
			JsonSerializer::serialize<ControllerProfile>( writer, i.second );
		}
	}
	writer.writeArrayEnd();
}

static HashMap<Uuid,string> parseMappings( const Json &json ) {
	const JArray &jsonArray = json.array();

	HashMap<Uuid,string> mappings;
	for( const Json &mappingJson : jsonArray ) {
		const Uuid deviceId = Uuid::parse( mappingJson["device"].get<string>() );
		mappings[deviceId] = mappingJson["profile"].get<string>();
	}
	return mappings;
}

static void serializeMappings( JsonWriter &writer, const HashMap<Uuid,string> &mappings ) {
	writer.writeArrayStart();
	for( const auto &i : mappings ) {
		writer.writeObjectStart();
		writer.writeProperty( "device", i.first.toString() );
		writer.writeProperty( "profile", i.second );
		writer.writeObjectEnd();
	}
	writer.writeArrayEnd();
}

static std::vector<int> parseHotkeys( const Json &json ) {
	if( json.array().size() != (size_t)Hotkey::NUM_HOTKEYS ) {
		throw std::range_error( "Array of hotkeys is the wrong size" );
	}

	std::vector<int> hotkeys;
	hotkeys.reserve( (size_t)Hotkey::NUM_HOTKEYS );
	for( const Json &hotkey : json.array() ) {
		hotkeys.push_back( hotkey.get<int>() );
	}
	return hotkeys;
}

static void serializeHotkeys( JsonWriter &writer, const std::vector<int> &hotkeys ) {
	writer.writeArrayStart();
	for( int hotkey : hotkeys ) {
		writer.writeNumber( (long)hotkey );
	}
	writer.writeArrayEnd();
}


static FileCache<AppSettings> &appSettingsCache() {
	static FileCache<AppSettings> s_cache = FileCache<AppSettings>(
		BaseDir::config() / "settings.cfg",
		AppSettings::Default,
		JsonSerializer::parse<AppSettings>,
		JsonSerializer::serialize<AppSettings>
	);
	return s_cache;
}

static FileCache<std::vector<ROM>> &romListCache() {
	static FileCache<std::vector<ROM>> s_cache = FileCache<std::vector<ROM>>(
		BaseDir::data() / "roms.json",
		std::vector<ROM>(),
		parseJsonArray<ROM>,
		serializeToJsonArray<ROM>
	);
	return s_cache;
}

static FileCache<std::vector<RomSource>> &romSourcesCache() {
	static FileCache<std::vector<RomSource>> s_cache = FileCache<std::vector<RomSource>>(
		BaseDir::data() / "sources.json",
		std::vector<RomSource>(),
		parseJsonArray<RomSource>,
		serializeToJsonArray<RomSource>
	);
	return s_cache;
}

static FileCache<std::vector<fs::path>> &savedRomsCache() {
	static FileCache<std::vector<fs::path>> s_cache = FileCache<std::vector<fs::path>>(
		BaseDir::data() / "sources2.json",
		std::vector<fs::path>(),
		parseJsonArray<fs::path>,
		serializeToJsonArray<fs::path>
	);
	return s_cache;
}

static FileCache<std::set<string>> &tagsCache() {
	static FileCache<std::set<string>> s_cache = FileCache<std::set<string>>(
		BaseDir::data() / "groups.json",
		std::set<string>({ "Favourites" }),
		parseTags,
		serializeTags
	);
	return s_cache;
}

static FileCache<std::map<string, ControllerProfile>> &profilesCache() {
	static FileCache<std::map<string, ControllerProfile>> s_cache = FileCache<std::map<string, ControllerProfile>>(
		BaseDir::data() / "profiles.json",
		{
			{ DefaultProfile::Gamecube.name, DefaultProfile::Gamecube },
			{ DefaultProfile::XBox360.name, DefaultProfile::XBox360 }
		},
		parseProfiles,
		serializeProfiles
	);
	return s_cache;
}

static FileCache<HashMap<Uuid,string>> &mappingsCache() {
	static FileCache<HashMap<Uuid,string>> s_cache = FileCache<HashMap<Uuid,string>>(
		BaseDir::data() / "devices.json",
		HashMap<Uuid,string>(),
		parseMappings,
		serializeMappings
	);
	return s_cache;
}


static FileCache<UiState> &uiStateCache() {
	static FileCache<UiState> s_cache = FileCache<UiState>(
		BaseDir::cache() / "ui-state.json",
		UiState::Default,
		JsonSerializer::parse<UiState>,
		JsonSerializer::serialize<UiState>
	);
	return s_cache;
}

static FileCache<std::vector<int>> &hotkeysCache() {
	static FileCache<std::vector<int>> s_cache = FileCache<std::vector<int>>(
		BaseDir::config() / "hotkeys.json",
		std::vector<int>( Hotkeys::Default, Hotkeys::Default + (int)Hotkey::NUM_HOTKEYS ),
		parseHotkeys,
		serializeHotkeys
	);
	return s_cache;
}



const AppSettings &FileController::loadAppSettings() {
	return appSettingsCache().get();
}

void FileController::saveAppSettings( const AppSettings &settings ) {
	appSettingsCache().set( settings );
}

const std::vector<ROM> &FileController::loadRomList() {
	return romListCache().get();
}

void FileController::saveRomList( const std::vector<ROM> &roms ) {
	romListCache().set( roms );
}

const std::vector<RomSource> &FileController::loadRomSources() {
	return romSourcesCache().get();
}

void FileController::saveRomSources( const std::vector<RomSource> &sources ) {
	romSourcesCache().set( sources );
}

const std::vector<fs::path> &FileController::loadSavedRoms() {
	return savedRomsCache().get();
}

void FileController::saveSavedRoms( const std::vector<fs::path> &roms ) {
	savedRomsCache().set( roms );
}

const std::set<string> &FileController::loadTags() {
	return tagsCache().get();
}

void FileController::saveTags( const std::set<string> &tags ) {
	tagsCache().set( tags );
}

const std::map<string, ControllerProfile> &FileController::loadControllerProfiles() {
	return profilesCache().get();
}

void FileController::saveControllerProfiles( const std::map<string, ControllerProfile> &profiles ) {
	profilesCache().set( profiles );
}

const HashMap<Uuid,string> &FileController::loadControllerMappings() {
	return mappingsCache().get();
}

void FileController::saveControllerMappings( const HashMap<Uuid,string> &mappings ) {
	mappingsCache().set( mappings );
}

const UiState &FileController::loadUiState() {
	return uiStateCache().get();
}

void FileController::saveUiState( const UiState &state ) {
	uiStateCache().set( state );
}

const std::vector<int> &FileController::loadHotkeys() {
	return hotkeysCache().get();
}

void FileController::saveHotkeys( const std::vector<int> &hotkeys ) {
	return hotkeysCache().set( hotkeys );
}
