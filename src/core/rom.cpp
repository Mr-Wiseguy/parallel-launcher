#include "src/core/rom.hpp"

#include <unordered_map>
#include <utility>
#include <fstream>
#include <chrono>
#include <ios>
#include "src/polyfill/file-search.hpp"
#include "src/thirdparty/crc32.h"

static constexpr char P_NAME[] = "name";
static constexpr char P_INTERNAL_NAME[] = "internal_rom_name";
static constexpr char P_LAST_VERSION[] = "file_last_version";
static constexpr char P_PATH[] = "file_path";
static constexpr char P_EMULATOR[] = "emulator_core";
static constexpr char P_PARALLEL_PLUGIN[] = "gfx_plugin";
static constexpr char P_MUPEN_PLUGIN[] = "gfx_plugin_mupen";
static constexpr char P_LAST_PLAYED[] = "last_played";
static constexpr char P_PLAY_TIME[] = "play_time";
static constexpr char P_TAGS[] = "tags";
static constexpr char P_NOTES[] = "notes";
static constexpr char P_OVERCLOCK_CPU[] = "overclock_cpu";
static constexpr char P_OVERCLOCK_VI[] = "overclock_vi";
static constexpr char P_FOLDER[] = "folder_path";
static constexpr char P_RECURSIVE[] = "recursive";
static constexpr char P_IGNORE_HIDDEN[] = "ignore_hidden";
static constexpr char P_FOLLOW_SYMLINKS[] = "follow_symlinks";
static constexpr char P_MAX_DEPTH[] = "max_depth";
static constexpr char P_CRC32[] = "crc32";

template<> void JsonSerializer::serialize<ROM>( JsonWriter &jw, const ROM &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_NAME, obj.name );
	jw.writeProperty( P_INTERNAL_NAME, obj.internalName );
	jw.writeProperty( P_PATH, obj.path.string() );
	jw.writeProperty( P_EMULATOR, obj.emulator );
	jw.writeProperty( P_PARALLEL_PLUGIN, obj.parallelPlugin );
	jw.writeProperty( P_MUPEN_PLUGIN, obj.mupenPlugin );
	jw.writeProperty( P_LAST_PLAYED, obj.lastPlayed );
	jw.writeProperty( P_PLAY_TIME, obj.playTime );
	jw.writePropertyName( P_TAGS );
	jw.writeArrayStart();
	for( const string &tag : obj.tags ) {
		jw.writeString( tag );
	}
	jw.writeArrayEnd();
	jw.writeProperty( P_NOTES, obj.notes );
	jw.writeProperty( P_OVERCLOCK_CPU, obj.overclockCPU );
	jw.writeProperty( P_OVERCLOCK_VI, obj.overclockVI );
	jw.writeProperty( P_LAST_VERSION, obj.fileVersion );
	jw.writeProperty( P_CRC32, obj.crc32 );
	jw.writeObjectEnd();
}

template<> ROM JsonSerializer::parse<ROM>( const Json &json ) {
	std::set<string> tags;
	std::filesystem::path romPath;
	std::optional<uint> crc32Optional;
	uint crc32;

	for( const Json &tag : json[P_TAGS].array() ) {
		tags.insert( tag.get<string>() );
	}

	romPath = std::filesystem::path( json[P_PATH].get<string>() );
	crc32Optional = json[P_CRC32].tryGet<uint>();

	if (crc32Optional.has_value()) {
		crc32 = crc32Optional.value();
	} else {
		crc32 = RomUtil::getCrc32( romPath );
	}

	return ROM{
		json[P_NAME].get<string>(),
		json[P_INTERNAL_NAME].getOrDefault<string>( "" ),
		romPath,
		json[P_EMULATOR].getOrDefault<EmulatorCore>( EmulatorCore::ParallelN64 ),
		json[P_PARALLEL_PLUGIN].get<GfxPlugin>(),
		json[P_MUPEN_PLUGIN].getOrDefault<GfxPlugin>( GfxPlugin::UseDefault ),
		json[P_LAST_PLAYED].get<int64>(),
		json[P_PLAY_TIME].get<int64>(),
		std::move( tags ),
		json[P_NOTES].get<string>(),
		json[P_OVERCLOCK_CPU].getOrDefault<bool>( true ),
		json[P_OVERCLOCK_VI].getOrDefault<bool>( false ),
		json[P_LAST_VERSION].getOrDefault<int64>( -1 ),
		crc32
	};
}

template<> void JsonSerializer::serialize<RomSource>( JsonWriter &jw, const RomSource &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_FOLDER, obj.folder.string() );
	jw.writeProperty( P_RECURSIVE, obj.recursive );
	jw.writeProperty( P_IGNORE_HIDDEN, obj.ignoreHidden && obj.recursive );
	jw.writeProperty( P_FOLLOW_SYMLINKS, obj.followSymlinks && obj.recursive );
	jw.writeProperty( P_MAX_DEPTH, obj.recursive ? obj.maxDepth : 5 );
	jw.writePropertyName( P_TAGS );
	jw.writeArrayStart();
	for( const string &tag : obj.autoTags ) {
		jw.writeString( tag );
	}
	jw.writeArrayEnd();
	jw.writeObjectEnd();
}

template<> RomSource JsonSerializer::parse<RomSource>( const Json &json ) {
	std::set<string> tags;
	for( const Json &tag : json[P_TAGS].array() ) {
		tags.insert( tag.get<string>() );
	}

	return RomSource{
		std::filesystem::path( json[P_FOLDER].get<string>() ),
		json[P_RECURSIVE].get<bool>(),
		json[P_IGNORE_HIDDEN].get<bool>(),
		json[P_FOLLOW_SYMLINKS].get<bool>(),
		json[P_MAX_DEPTH].get<ubyte>(),
		std::move( tags )
	};
}

template<> void JsonSerializer::serialize<fs::path>( JsonWriter &jw, const fs::path &obj ) {
	jw.writeString( obj.string() );
}

template<> fs::path JsonSerializer::parse<fs::path>( const Json &json ) {
	return fs::path( json.get<string>() );
}

std::vector<ROM> RomFinder::scan(
	const std::vector<ROM> &currentRomList,
	const std::vector<RomSource> &sources,
	const std::vector<fs::path> &additionalRomPaths,
	CancellationToken &cancellationToken
) {
	HashMap<fs::path,ROM> previousRoms;
	previousRoms.reserve( currentRomList.size() );
	for( const ROM &rom : currentRomList ) {
		previousRoms[rom.path] = rom;
	}

	HashMap<fs::path,std::set<string>> foundRoms;
	for( const RomSource &source : sources ) {
		if( cancellationToken.isCancelled() ) {
			return std::vector<ROM>();
		}

		const std::vector<fs::path> paths = FileSearch::find(
			source.folder,
#ifdef _WIN32
			"*.?64",
#else
			"*.[nvz]64",
#endif
			source.recursive ? source.maxDepth : 1,
			source.followSymlinks,
			source.ignoreHidden,
			cancellationToken
		);

		for( fs::path romPath : paths ) {
#ifdef _WIN32
			const char romType = romPath.extension().string().c_str()[1];
			if(
				romType != 'n' && romType != 'N' &&
				romType != 'v' && romType != 'V' &&
				romType != 'z' && romType != 'Z'
			) continue;
#endif

			if( fs::is_symlink( romPath ) ) {
				if( !source.followSymlinks ) continue;
				for( int i = 0; i < 9 && fs::is_symlink( romPath ); i++ ) {
					romPath = fs::read_symlink( romPath );
				}
			}

			if( fs::is_regular_file( romPath ) ) {
				foundRoms[romPath].insert( source.autoTags.begin(), source.autoTags.end() );
			}
		}
	}

	for( const fs::path &romPath : additionalRomPaths ) {
		if( foundRoms.count( romPath ) > 0 ) continue;

		if( cancellationToken.isCancelled() ) {
			return std::vector<ROM>();
		}

		if( fs::is_regular_file( romPath ) ) {
			foundRoms[romPath] = std::set<string>();
		}
	}

	std::vector<ROM> roms;
	roms.reserve( foundRoms.size() );
	for( const auto &r : foundRoms ) {
		auto knownRom = previousRoms.find( r.first );
		const int64 romLastModified = RomUtil::getLastModified( r.first );
		if( knownRom != previousRoms.end() ) {
			if( knownRom->second.fileVersion < romLastModified ) {
				knownRom->second.internalName = RomUtil::getInternalName( r.first );
				knownRom->second.fileVersion = romLastModified;
			}
			roms.push_back( knownRom->second );
		} else {
			roms.push_back( ROM{
				/* name */ r.first.stem().string(),
				/* internalName */ RomUtil::getInternalName( r.first ),
				/* folder */ r.first,
				/* emulator */ EmulatorCore::UseDefault,
				/* parallelPlugin */ GfxPlugin::UseDefault,
				/* mupenPlugin */ GfxPlugin::UseDefault,
				/* lastPlayed */ 0,
				/* playTime */ 0,
				/* tags */ std::move( r.second ),
				/* notes */ "",
				/* overclockCPU */ true,
				/* overclockVI */ false,
				/* fileVersion */ romLastModified,
				/* crc32 */ RomUtil::getCrc32( r.first )
			});
		}
	}

	return roms;
}

static inline void unscrambleN64( ubyte *name ) {
	for( int i = 0; i < 20; i+= 4 ) {
		std::swap( name[i], name[i+3] );
		std::swap( name[i+1], name[i+2] );
	}
}

static inline void unscrambleV64( ubyte *name ) {
	for( int i = 0; i < 20; i+= 2 ) {
		std::swap( name[i], name[i+1] );
	}
}

static const ushort s_kanaTable[64] = {
	0x00A0, 0x8082, 0x808C, 0x808D, 0x8081, 0x83BB, 0x83B2, 0x82A1, 0x82A3, 0x82A5, 0x82A7, 0x82A9, 0x83A3, 0x83A5, 0x83A7, 0x8383,
	0x83BC, 0x82A2, 0x82A4, 0x82A6, 0x82A8, 0x82AA, 0x82AB, 0x82AD, 0x82AF, 0x82B1, 0x82B3, 0x82B5, 0x82B7, 0x82B9, 0x82BB, 0x82BD,
	0x82BF, 0x8381, 0x8384, 0x8386, 0x8388, 0x838A, 0x838B, 0x838C, 0x838D, 0x838E, 0x838F, 0x8392, 0x8395, 0x8398, 0x839B, 0x839E,
	0x839F, 0x83A0, 0x83A1, 0x83A2, 0x83A4, 0x83A6, 0x83A8, 0x83A9, 0x83AA, 0x83AB, 0x83AC, 0x83AD, 0x83AF, 0x83B3, 0x829B, 0x829C
};

string RomUtil::getInternalName( const fs::path romPath ) {
	ubyte sjsName[20];
	std::ifstream romFile( romPath.string(), std::ios_base::in | std::ios_base::binary );

	romFile.seekg( 0 );
	const ubyte firstByte = (ubyte)romFile.get();

	romFile.seekg( 0x20 );
	romFile.read( (char*)sjsName, 20 );
	if( !romFile.good() ) return string();
	romFile.close();

	if( firstByte == 0x40 ) {
		unscrambleN64( sjsName );
	} else if( firstByte == 0x37 ) {
		unscrambleV64( sjsName );
	}

	string name;
	for( int i = 0; i < 20; i++ ) {
		const ubyte c = sjsName[i];

		if( c == 0x5C ) {
			name += (char)0xC2;
			name += (char)0xA5;
		} else if( c == 0x7E ) {
			name += (char)0xE2;
			name += (char)0x80;
			name += (char)0xBE;
		} else if( c > 0xA0 && sjsName[i] < 0xE0 ) {
			const ushort wc = s_kanaTable[c - 0xA0];
			name += (char)0xE3;
			name += (char)(wc >> 8);
			name += (char)(wc & 0xFF);
		} else if( c == 0 ) {
			name += ' ';
		} else {
			name += (char)c;
		}
	}

	int n = (int)name.length();
	while( n > 0 && name[--n] == ' ' );

	return name.substr( 0, n + 1 );
}

int64 RomUtil::getLastModified( const fs::path romPath ) {
	const fs::file_time_type lastModified = fs::last_write_time( romPath );
	const int64 relativeTime = (int64)std::chrono::duration_cast<std::chrono::seconds>( lastModified - fs::file_time_type::clock::now() ).count();
	const int64 unixTime = (int64)std::chrono::duration_cast<std::chrono::seconds>( std::chrono::system_clock::now().time_since_epoch() ).count();
	return unixTime + relativeTime;
}

static constexpr size_t CRC_READ_BUFFER_SIZE = 8096;

uint RomUtil::getCrc32( const fs::path &romPath ) {
	std::uintmax_t fileSize = std::filesystem::file_size( romPath );
	std::ifstream romFile( romPath.string(), std::ios_base::in | std::ios_base::binary );
	char *readBuffer = new char[ CRC_READ_BUFFER_SIZE ];
	char *fileBuffer = new char[ fileSize + 1 ];
	uint crc;

	romFile.rdbuf()->pubsetbuf( readBuffer, CRC_READ_BUFFER_SIZE );
	romFile.read( fileBuffer, fileSize );
	romFile.close();
	delete[] readBuffer;

	crc = crc32( (const uint8_t*)fileBuffer, fileSize );
	delete[] fileBuffer;

	return crc;
}
