#ifndef SRC_CORE_SETTINGS_HPP_
#define SRC_CORE_SETTINGS_HPP_

#include <optional>
#include "src/core/filesystem.hpp"
#include "src/core/flags.hpp"
#include "src/core/json.hpp"
#include "src/core/uuid.hpp"

enum class GfxPlugin : ubyte {
	UseDefault = 0,
	ParaLLEl = 1,
	Glide64 = 2,
	Angrylion = 3,
	GlideN64 = 4,
	Rice = 5
};

enum class ParallelUpscaling : ubyte {
	Auto = 0,
	None = 1,
	x2 = 2,
	x4 = 4,
	x8 = 8
};

enum class ConfigBehaviour : ubyte {
	Transient = 0,
	Persistent = 1,
	Inherited = 2
};

enum class RomInfoColumn : ubyte {
	Path = 0x1,
	LastPlayed = 0x2,
	PlayTime = 0x4,
	Notes = 0x8,
	InternalName = 0x10
};

enum class EmulatorCore : ubyte {
	UseDefault = 0,
	ParallelN64 = 1,
	Mupen64plusNext = 2
};

DEFINE_FLAG_OPERATIONS( RomInfoColumn, ubyte )

struct AppSettings {
	RomInfoColumn visibleColumns;
	GfxPlugin defaultParallelPlugin;
	GfxPlugin defaultMupenPlugin;
	EmulatorCore defaultEmulator;
	ParallelUpscaling upscaling;
	ConfigBehaviour configBehaviour;
	ubyte windowScale;
	bool vsync;
	bool hideWhenPlaying;
#ifdef _WIN32
	fs::path retroInstallDir;
#else
	bool usingFlatpak;
#endif
	std::optional<Uuid> preferredController;
#ifdef _WIN32
	string windowsTheme;
#endif

	static const AppSettings Default;
};

namespace JsonSerializer {
	template<> void serialize<AppSettings>( JsonWriter &jw, const AppSettings &obj );
	template<> AppSettings parse<AppSettings>( const Json &json );
}

#endif /* SRC_CORE_SETTINGS_HPP_ */
