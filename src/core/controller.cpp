#include "src/core/controller.hpp"
#include "src/core/numeric-string.hpp"

template<> void JsonSerializer::serialize<Binding>(
	JsonWriter &jw,
	const Binding &obj
) {
	switch( obj.type ) {
		case BindingType::None:
			jw.writeNull();
			break;
		case BindingType::Button:
			jw.writeString( Number::toString( obj.buttonOrAxis ) );
			break;
		case BindingType::AxisNegative:
			jw.writeString( "-"s + Number::toString( obj.buttonOrAxis ) );
			break;
		case BindingType::AxisPositive:
			jw.writeString( "+"s + Number::toString( obj.buttonOrAxis ) );
			break;
	}
}

template<> Binding JsonSerializer::parse<Binding>(
	const Json &json
) {
	if( json.isNull() ) return { BindingType::None, 0 };
	const string inputString = json.get<string>();

	if( inputString.at( 0 ) == '+' ) {
		return { BindingType::AxisPositive, (ushort)Number::parseUInt( inputString.substr( 1 ) ) };
	} else if( inputString.at( 0 ) == '-' ) {
		return { BindingType::AxisNegative, (ushort)-Number::parseInt( inputString ) };
	} else {
		return { BindingType::Button, (ushort)Number::parseUInt( inputString ) };
	}
}

static constexpr char P_NAME[] = "name";
static constexpr char P_BINDINGS[] = "bindings";
static constexpr char P_SENSITIVITY[] = "sensitivity";
static constexpr char P_DEADZONE[] = "deadzone";
static constexpr char P_RUMBLE[] = "rumble";

template<> void JsonSerializer::serialize<ControllerProfile>(
	JsonWriter &jw,
	const ControllerProfile &obj
) {
	jw.writeObjectStart();
	jw.writeProperty( P_NAME, obj.name );
	jw.writePropertyName( P_BINDINGS );
	jw.writeArrayStart();
	for( int i = 0; i < (int)ControllerAction::NUM_ACTIONS; i++ ) {
		JsonSerializer::serialize<Binding>( jw, obj.bindings[i] );
	}
	jw.writeArrayEnd();
	jw.writeProperty( P_SENSITIVITY, obj.sensitivity );
	jw.writeProperty( P_DEADZONE, obj.deadzone );
	jw.writeProperty( P_RUMBLE, obj.rumble );
	jw.writeObjectEnd();
}

template<> ControllerProfile JsonSerializer::parse<ControllerProfile>(
	const Json &json
) {
	ControllerProfile profile;
	profile.name = json[P_NAME].get<string>();
	const JArray bindingsJson = json[P_BINDINGS].array();
	for( int i = 0; i < (int)ControllerAction::NUM_ACTIONS; i++ ) {
		profile.bindings[i] = JsonSerializer::parse<Binding>( bindingsJson[i] );
	}
	profile.sensitivity = json[P_SENSITIVITY].get<double>();
	profile.deadzone = json[P_DEADZONE].get<double>();
	profile.rumble = json[P_RUMBLE].get<bool>();
	return profile;
}
