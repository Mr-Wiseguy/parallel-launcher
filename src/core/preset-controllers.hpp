#ifndef SRC_CORE_PRESET_CONTROLLERS_HPP_
#define SRC_CORE_PRESET_CONTROLLERS_HPP_

#include "src/core/controller.hpp"

enum class ControllerType {
	Gamecube,
	XBox360,
	Other
};

ControllerType getControllerType( const ControllerId &controllerId );

namespace DefaultProfile {
	extern const ControllerProfile Gamecube;
	extern const ControllerProfile XBox360;

	extern bool exists( const string &name );
}


#endif /* SRC_CORE_PRESET_CONTROLLERS_HPP_ */
