#include "src/types.hpp"

#if defined(DEBUG) && !defined(_WIN32)
#include "src/core/traceable-exception.hpp"
#include <execinfo.h>
#include <exception>
#include <iostream>
#include <csignal>
#include <cstdlib>
#endif

#include <QGuiApplication>
#include <QApplication>
#include <QMessageBox>
#include "src/core/retroarch.hpp"
#include "src/ui/main-window.hpp"
#include "src/ui/ui-fixes.hpp"
#ifdef _WIN32
#include <QInputDialog>
#include "src/core/file-controller.hpp"

static const char *const m_msgInstallRetroArch = ""
	"RetroArch does not appear to be installed. You can install it by going to "
	"https://www.retroarch.com/ and clicking 'Get RetroArch' then 'Download Stable'.";
#else
#include "src/polyfill/base-directory.hpp"

static const char *const m_msgInstallRetroArch = ""
	"RetroArch does not appear to be installed. You can install it by going to "
	"https://www.retroarch.com/ and clicking 'Get RetroArch' then 'Download Stable'. "
	"If you are using a custom build of RetroArch, ensure that the folder containing "
	"your RetroArch binary is in your $PATH variable.";
#endif

static int run( QApplication &app, const fs::path &romArg ) {
	QGuiApplication::setQuitOnLastWindowClosed( false );

	MainWindow mainWindow( romArg );
	mainWindow.show();

	const int statusCode = app.exec();
#ifndef _WIN32
	std::error_code err;
	fs::remove_all( BaseDir::homeTemp(), err );
#endif
	return statusCode;
}

#if defined(DEBUG) && !defined(_WIN32)
extern "C" void onSegfault( [[maybe_unused]] int signal ) {
	void *trace[32];
	int frames = backtrace( trace, 32 );
	backtrace_symbols_fd( trace, frames, 2 );
	std::cerr << "Segmentation Fault" << std::endl << std::flush;
	std::_Exit( EXIT_FAILURE );
}

static void onTerminate() {
	const std::exception_ptr ep = std::current_exception();
	if( !ep ) return;

	try {
		std::rethrow_exception( ep );
	} catch( const std::exception &exception ) {
		std::cerr << "Uncaught Exception: " << std::endl << exception.what() << std::endl;
		const TraceableException *traceableException = dynamic_cast<const TraceableException*>( &exception );
		if( traceableException != nullptr ) {
			traceableException->printBacktrace();
		}
		std::cerr << std::flush;
	} catch( ... ) {
		std::cerr << "Uncaught Exception: (non-std::exception)" << std::endl << std::flush;
	}

	std::abort();
}
#endif

static fs::path tryGetRomPath( char *arg ) {
	try {
		const fs::path romPath = fs::path( arg );
		if( !fs::is_regular_file( romPath ) ) {
			return fs::path();
		}

		const string extension = romPath.extension().string();
		if(
			extension != ".z64" &&
			extension != ".n64" &&
			extension != ".v64" &&
			extension != ".Z64" &&
			extension != ".N64" &&
			extension != ".V64" &&
			extension != ".bps"
		) return fs::path();

		if( romPath.is_relative() ) {
			return fs::current_path() / romPath;
		}

		return romPath;

	} catch( ... ) {}

	return fs::path();
}

int main( int argc, char *argv[] ) {
#if defined(DEBUG) && !defined(_WIN32)
	std::signal( SIGSEGV, onSegfault );
	std::set_terminate( onTerminate );
#endif

	QApplication app( argc, argv );
	applyUiFixes( app );

#ifdef _WIN32
	AppSettings settings = FileController::loadAppSettings();
	while( !RetroArch::isRetroArchInstalled() ) {
		const QString installDir = QInputDialog::getText( nullptr, "RetroArch Not Installed", m_msgInstallRetroArch, QLineEdit::Normal, settings.retroInstallDir.string().c_str() );
		if( installDir.isNull() ) {
			return 0;
		} else if( !installDir.isEmpty() ) {
			settings.retroInstallDir = fs::path( installDir.toStdString() );
			FileController::saveAppSettings( settings );
		}
	}
#else
	if( !RetroArch::isRetroArchInstalled() ) {
		QMessageBox::critical( nullptr, "RetroArch Not Installed", m_msgInstallRetroArch );
		return 0;
	}
#endif

	ushort major, minor;
	if( RetroArch::tryGetVersion( major, minor ) ) {
		if( major < 1 || (major == 1 && minor < 9) ) {
			QMessageBox::warning( nullptr, "Old RetroArch Version", "You are using an old version of RetroArch. Please upgrade to version 1.9.0 or later." );
		}
	} else {
		QMessageBox::warning( nullptr, "Unknown RetroArch Version", "Unable to determine RetroArch version. If you are using a version of RetroArch older than 1.9.0, some features may not work correctly." );
	}

	return run( app, (argc >= 2) ? tryGetRomPath( argv[1] ) : fs::path() );
}
