#ifndef SRC_POLYFILL_FILE_DIALOG_HPP_
#define SRC_POLYFILL_FILE_DIALOG_HPP_

#include "src/core/filesystem.hpp"

namespace FileDialog {

	fs::path getDirectory( const char *dialogTitle );

}



#endif /* SRC_POLYFILL_FILE_DIALOG_HPP_ */
