#ifndef SRC_POLYFILL_BASE_DIRECTORY_HPP_
#define SRC_POLYFILL_BASE_DIRECTORY_HPP_

#include "src/core/filesystem.hpp"
#include "src/types.hpp"

namespace BaseDir {

	const fs::path &home();
	const fs::path &config();
	const fs::path &data();
	const fs::path &cache();
	const fs::path &temp();
	const fs::path &program();
#ifndef _WIN32
	const fs::path &homeTemp();
#endif

}

#endif /* SRC_POLYFILL_BASE_DIRECTORY_HPP_ */
