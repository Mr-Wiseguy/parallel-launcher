#include "src/polyfill/file-dialog.hpp"

// QFileDialog fails for WSL paths on Windows, so we need to reimplement it :(

#ifdef _WIN32

#include <windows.h>
#include <shobjidl.h>
#include <string>
#include <locale>
#include <codecvt>

static inline std::string toUtf8( const std::wstring &src ) {
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,wchar_t> converter;
	return converter.to_bytes( src );
}

static inline std::wstring toUtf16( const std::string &src ) {
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
	return converter.from_bytes( src );
}

fs::path FileDialog::getDirectory( const char *dialogTitle ) {
	IFileOpenDialog *dialog;
	IShellItem *folder;
	fs::path result;
	HRESULT status;

	CoInitializeEx( nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE );
	CoCreateInstance( CLSID_FileOpenDialog, nullptr, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>( &dialog ) );
	dialog->SetTitle( toUtf16( dialogTitle ).c_str() );
	dialog->SetOptions( FOS_PICKFOLDERS | FOS_FILEMUSTEXIST | FOS_FORCESHOWHIDDEN );
	dialog->Show( nullptr );
	status = dialog->GetResult( &folder );
	if( SUCCEEDED( status ) ) {
		wchar_t *pathString;
		status = folder->GetDisplayName( SIGDN_FILESYSPATH, &pathString );
		if( SUCCEEDED( status ) ) {
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,wchar_t> converter;
			result = fs::path( toUtf8( pathString ) );
			CoTaskMemFree( pathString );
		}

		folder->Release();
	}

	dialog->Release();
	CoUninitialize();

	return result;
}


#else

#include <QFileDialog>

fs::path FileDialog::getDirectory( const char *dialogTitle ) {
	const QString path = QFileDialog::getExistingDirectory( nullptr, dialogTitle );
	return path.isNull() ? fs::path() : fs::path( path.toStdString() );
}

#endif
