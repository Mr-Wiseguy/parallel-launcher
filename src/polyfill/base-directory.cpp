#include "src/polyfill/base-directory.hpp"

#ifdef _WIN32
#include <windows.h>
#include <knownfolders.h>
#include <shlobj.h>
#include <libloaderapi.h>
#include <exception>
#include <string>
#include <locale>
#include <codecvt>
#else
#include <cstdlib>
#endif

struct Locations {
	fs::path home;
	fs::path config;
	fs::path data;
	fs::path cache;
	fs::path temp;
	fs::path program;
};

static constexpr char APP_NAME[] = "parallel-launcher";

#ifdef _WIN32
static inline std::string toUtf8( const wchar_t *wtsr ) {
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,wchar_t> converter;
	return converter.to_bytes( std::wstring( wtsr ) );
}

Locations getLocations() {
	wchar_t *buffer;

	if( SHGetKnownFolderPath(
		FOLDERID_Profile,
		KF_FLAG_NO_ALIAS | KF_FLAG_DEFAULT_PATH | KF_FLAG_NOT_PARENT_RELATIVE,
		nullptr,
		&buffer
	) != S_OK ) {
		if( buffer != nullptr ) {
			CoTaskMemFree( buffer );
		}
		throw std::runtime_error( "Failed to get home directory location from Windows." );
	}

	if( buffer != nullptr ) {
		CoTaskMemFree( buffer );
	}

	const fs::path home = fs::path( toUtf8( buffer ) );
	fs::path appData;

	buffer = nullptr;
	if( SHGetKnownFolderPath(
		FOLDERID_LocalAppData,
		KF_FLAG_CREATE | KF_FLAG_NO_ALIAS | KF_FLAG_DONT_VERIFY | KF_FLAG_DEFAULT_PATH | KF_FLAG_NOT_PARENT_RELATIVE,
		nullptr,
		&buffer
	) == S_OK ) {
		appData = fs::path( toUtf8( buffer ) );
	} else {
		appData = home / "AppData" / "Local";
	}

	appData = appData / APP_NAME;
	if( buffer != nullptr ) {
		CoTaskMemFree( buffer );
	}

	char currentExePath[261];
	if( GetModuleFileNameA( nullptr, currentExePath, 261 ) == 0 ) {
		throw std::runtime_error( "Failed to get current executable path from Windows." );
	}

	return Locations{
		home,
		appData / "config",
		appData / "data",
		appData / "cache",
#ifdef _WIN64
		fs::temp_directory_path() / APP_NAME,
#else
		appData / "Temp",
#endif
		fs::path( currentExePath ).parent_path()
	};
}
#else
static fs::path xdg( const char *varName, const fs::path &homePath, const char *defaultRelativePath ) {
	char *xdgVal = getenv( varName );
	if( xdgVal != nullptr && xdgVal[0] != '\0' ) {
		return fs::path( xdgVal );
	}
	return homePath / defaultRelativePath;
}

Locations getLocations() {
	const fs::path home = fs::path( getenv( "HOME" ) );
	return Locations{
		home,
		xdg( "XDG_CONFIG_HOME", home, ".config" ) / APP_NAME,
		xdg( "XDG_DATA_HOME", home, ".local/share" ) / APP_NAME,
		xdg( "XDG_CACHE_HOME", home, ".cache" ) / APP_NAME,
		fs::temp_directory_path() / APP_NAME,
		fs::read_symlink( "/proc/self/exe" ).parent_path()
	};
}
#endif

static inline Locations fetchOrCreateLocations() {
	const Locations locations = getLocations();
	fs::create_directories( locations.config );
	fs::create_directories( locations.data );
	fs::create_directories( locations.cache );
	return locations;
}

static inline const Locations &lazyGetLocations() {
	static const Locations s_locations = fetchOrCreateLocations();
	return s_locations;
}

const fs::path &BaseDir::home() {
	return lazyGetLocations().home;
}

const fs::path &BaseDir::config() {
	return lazyGetLocations().config;
}

const fs::path &BaseDir::data() {
	return lazyGetLocations().data;
}

const fs::path &BaseDir::cache() {
	return lazyGetLocations().cache;
}

const fs::path &BaseDir::temp() {
	return lazyGetLocations().temp;
}

const fs::path &BaseDir::program() {
	return lazyGetLocations().program;
}

#ifndef _WIN32
static inline fs::path getHomeTemp() {
	const fs::path homeTemp = lazyGetLocations().home / ".temp" / "parallel-launcher";
	fs::create_directories( homeTemp );
	return homeTemp;
}

const fs::path &BaseDir::homeTemp() {
	static const fs::path s_homeTemp = getHomeTemp();
	return s_homeTemp;
}
#endif
