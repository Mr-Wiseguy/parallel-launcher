#include "src/ui/ui-state.hpp"

const UiState UiState::Default = {
	/* romList */ {
		/* selectedRom */ fs::path(),
		/* selectedGroup */ "",
		/* expandedGroups */ std::vector<string>()
	},
	/* windowWidth */ 450,
	/* windowHeight */ 600,
	/* showAllPlugins */ false
};

static constexpr char P_SELECTED_ROM[] = "selected_rom";
static constexpr char P_SELECTED_ROM_TAG[] = "selected_rom_tag";
static constexpr char P_EXPANDED_GROUPS[] = "expanded_groups";
static constexpr char P_WINDOW_WIDTH[] = "window_width";
static constexpr char P_WINDOW_HEIGHT[] = "window_height";
static constexpr char P_SHOW_ALL_PLUGINS[] = "show_all_plugins";

template<> void JsonSerializer::serialize<UiState>( JsonWriter &jw, const UiState &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_SELECTED_ROM, obj.romList.selectedRom.string() );
	jw.writeProperty( P_SELECTED_ROM_TAG, obj.romList.selectedGroup );
	jw.writePropertyName( P_EXPANDED_GROUPS );
	jw.writeArrayStart();
	for( const string &group : obj.romList.expandedGroups ) {
		jw.writeString( group );
	}
	jw.writeArrayEnd();
	jw.writeProperty( P_WINDOW_WIDTH, obj.windowWidth );
	jw.writeProperty( P_WINDOW_HEIGHT, obj.windowHeight );
	jw.writeProperty( P_SHOW_ALL_PLUGINS, obj.showAllPlugins );
	jw.writeObjectEnd();
}

template<> UiState JsonSerializer::parse<UiState>( const Json &json ) {
	UiState state = {
		{
			fs::path( json[P_SELECTED_ROM].getOrDefault<string>( "" ) ),
			json[P_SELECTED_ROM_TAG].getOrDefault<string>( "" ),
			std::vector<string>()
		},
		json[P_WINDOW_WIDTH].getOrDefault<int>( UiState::Default.windowWidth ),
		json[P_WINDOW_HEIGHT].getOrDefault<int>( UiState::Default.windowHeight ),
		json[P_SHOW_ALL_PLUGINS].getOrDefault<bool>( false )
	};

	if( json[P_EXPANDED_GROUPS].isArray() ) {
		const JArray &groupArray = json[P_EXPANDED_GROUPS].array();
		state.romList.expandedGroups.reserve( groupArray.size() );

		for( const Json &group : groupArray ) {
			state.romList.expandedGroups.push_back( group.get<string>() );
		}
	}

	return state;
}
