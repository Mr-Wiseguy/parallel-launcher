#ifndef SRC_UI_ROM_LIST_FETCHER_HPP_
#define SRC_UI_ROM_LIST_FETCHER_HPP_

#include <QThread>
#include <memory>
#include "src/core/rom.hpp"
#include "src/core/async.hpp"

typedef std::shared_ptr<std::vector<ROM>> FetchedRomList;
Q_DECLARE_METATYPE( FetchedRomList )

class _RomListFetcherThread : public QThread {
	Q_OBJECT

	private:
	CancellationToken m_cancellationToken;

	public:
	_RomListFetcherThread() : QThread() {}
	virtual ~_RomListFetcherThread() {}


	virtual void run() override;
	inline void cancel() { m_cancellationToken.cancel(); }

	signals:
	void done( FetchedRomList );
};

class RomListFetcher : public QObject {
	Q_OBJECT

	_RomListFetcherThread *m_thread;

	public:
	RomListFetcher();
	virtual ~RomListFetcher();

	public slots:
	void fetchAsync();

	signals:
	void romListFetched( FetchedRomList );

};



#endif /* SRC_UI_ROM_LIST_FETCHER_HPP_ */
