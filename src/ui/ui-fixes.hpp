#ifndef SRC_UI_UI_FIXES_HPP_
#define SRC_UI_UI_FIXES_HPP_

#include <QApplication>

#ifdef _WIN32
#include <QEvent>

class WindowsIconFixer : public QObject {
	Q_OBJECT

	public:
	WindowsIconFixer( QObject *parent = nullptr ) : QObject( parent ) {}
	~WindowsIconFixer() {}

	bool eventFilter( QObject *object, QEvent *event ) override;

};
#endif

void applyUiFixes( QApplication &app );

#endif /* SRC_UI_UI_FIXES_HPP_ */
