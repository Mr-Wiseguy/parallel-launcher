#include "src/ui/rom-list-view.hpp"

#include <QMenu>
#include <QHeaderView>
#include <QMessageBox>
#include <QInputDialog>
#include <cassert>
#include "src/ui/rom-list-model.hpp"
#include "src/core/file-controller.hpp"

RomListRenderer::RomListRenderer( QAbstractItemDelegate *innerDelegate ) :
	QAbstractItemDelegate( innerDelegate->parent() ),
	m_inner( innerDelegate )
{
	assert( parent() != nullptr );
}

void RomListRenderer::paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const {
	if( index.isValid() && !index.parent().isValid() ) {
		QStyleOptionViewItem newOption = option;
		newOption.font.setBold( true );

		const QTreeView *treeView = dynamic_cast<const QTreeView*>( parent() );
		assert( treeView != nullptr );

		int width = 0;
		const int numColumns = treeView->model()->columnCount();
		for( int i = 0; i < numColumns; i++ ) {
			width += treeView->columnWidth( i );
		}

		newOption.rect.setWidth( width );
		m_inner->paint( painter, newOption, index );
	} else {
		m_inner->paint( painter, option, index );
	}
}

QSize RomListRenderer::sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const {
	QSize size = m_inner->sizeHint( option, index );
	if( index.isValid() && !index.parent().isValid() ) {
		size.setWidth( 0 );
	}
	return size;
}

RomListView::RomListView( QWidget *parent ) : QTreeView( parent ) {
	setModel( new RomListModel( this ) );
	setContextMenuPolicy( Qt::CustomContextMenu );
	setItemDelegate( new RomListRenderer( itemDelegate() ) );
	header()->setSectionResizeMode( QHeaderView::Stretch );
	header()->setSectionResizeMode( 0, QHeaderView::Stretch );
	header()->setSectionResizeMode( 1, QHeaderView::ResizeToContents );
	header()->setSectionResizeMode( 2, QHeaderView::Interactive );
	header()->setSectionResizeMode( 3, QHeaderView::ResizeToContents );
	header()->setSectionResizeMode( 4, QHeaderView::ResizeToContents );
	header()->setSectionResizeMode( 5, QHeaderView::Interactive );
	header()->setSectionsMovable( false );
	header()->setStretchLastSection( false );

	connect( this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onRightClick(const QPoint &)) );
}

RomListView::~RomListView() {
	model()->deleteLater();
}

void RomListView::updateRoms( const std::vector<ROM> &roms ) {
	static_cast<RomListModel*>( model() )->update( roms );
}

const ROM *RomListView::tryGetSelectedRom() const {
	return static_cast<const RomListModel*>( model() )->tryGetRomInfo( currentIndex() );
}

bool RomListView::selectRom( const string &tag, const fs::path &rom ) {
	const QModelIndex index = static_cast<const RomListModel*>( model() )->tryGetIndex( tag, rom );
	if( index.isValid() ) {
		setCurrentIndex( index );
		return true;
	}
	return false;
}

static constexpr int ACTION_DELETE_SAVE = 1;
static constexpr int ACTION_EDIT_SAVE = 2;
static constexpr int ACTION_ADD_TAG = 3;
static constexpr int ACTION_CREATE_TAG = 4;
static constexpr int ACTION_REMOVE_TAG = 5;

void RomListView::onRightClick( const QPoint &point ) {
	const QModelIndex romEntry = indexAt( point );
	if( !romEntry.isValid() ) return;
	this->setCurrentIndex( romEntry );

	const ROM *rom = tryGetSelectedRom();
	if( rom == nullptr ) return;

	const AppSettings &settings = FileController::loadAppSettings();
	const std::set<string> &allTags = FileController::loadTags();
	const fs::path saveFilePath = RetroArch::getSaveFilePath( settings, rom->path );
	const string tag = static_cast<const RomListModel*>( model() )->tryGetGroup( romEntry.parent() );

	QMenu contextMenu( this );
	if( fs::is_regular_file( saveFilePath ) ) {
		contextMenu.addAction( "Delete Save File" )->setData( QVariant::fromValue<int>( ACTION_DELETE_SAVE ) );
		contextMenu.addAction( "[SM64] Edit Save File" )->setData( QVariant::fromValue<int>( ACTION_EDIT_SAVE ) );
		contextMenu.addSeparator();
	}

	bool existingTagOptions = false;
	QMenu *tagMenu = contextMenu.addMenu( "Add to..." );
	for( const string &romTag : allTags ) {
		if( romTag == tag ) continue;
		existingTagOptions = true;
		tagMenu->addAction( romTag.c_str() )->setData( QVariant::fromValue<int>( ACTION_ADD_TAG ) );
	}
	if( existingTagOptions ) tagMenu->addSeparator();
	tagMenu->addAction( "New Group" )->setData( QVariant::fromValue<int>( ACTION_CREATE_TAG ) );

	if( !tag.empty() && tag != "Uncategorized" ) {
		contextMenu.addAction( ("Remove from "s + tag).c_str() )->setData( QVariant::fromValue<int>( ACTION_REMOVE_TAG ) );
	}

	const QAction *action = contextMenu.exec( mapToGlobal( point ) );
	if( action != nullptr && action->data().type() == QVariant::Int ) {
		switch( action->data().value<int>() ) {
			case ACTION_DELETE_SAVE: {
				if( QMessageBox::question( nullptr, "Confirm Delete", "Are you sure you want to delete your save file?" ) == QMessageBox::Yes ) {
					fs::remove( saveFilePath );
				}
				break;
			}
			case ACTION_ADD_TAG: {
				const string newTag = action->text().toStdString();
				emit addTag( rom, newTag );
				break;
			}
			case ACTION_CREATE_TAG: {
				const QString tagString = QInputDialog::getText( nullptr, "Create Group", "Enter a name for your new group" );
				if( tagString.isEmpty() || tagString.isNull() ) return;

				const string newTag = tagString.toStdString();
				if( allTags.count( newTag ) > 0 ) {
					QMessageBox::critical( nullptr, "Group Exists", "A group with this name already exists" );
					return;
				}

				std::set<string> newTags = allTags;
				newTags.insert( newTag );
				FileController::saveTags( newTags );

				emit addTag( rom, newTag );
				break;
			}
			case ACTION_EDIT_SAVE: {
				emit editSave( rom, saveFilePath );
				break;
			}
			case ACTION_REMOVE_TAG: {
				emit removeTag( rom, tag );
				break;
			}
			default: break;
		}
	}

}
