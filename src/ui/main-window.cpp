#include "src/ui/main-window.hpp"
#include "ui_main-window.h"

#include <QCoreApplication>
#include <QMessageBox>
#include <QCheckBox>
#include <chrono>
#include "src/ui/icons.hpp"
#include "src/ui/rom-list-model.hpp"
#include "src/ui/rom-source-dialog.hpp"
#include "src/ui/settings-dialog.hpp"
#include "src/ui/controller-select-dialog.hpp"
#include "src/ui/keyboard-config-dialog.hpp"
#include "src/ui/process-awaiter.hpp"
#include "src/ui/now-playing-window.hpp"
#include "src/ui/singleplayer-controller-select-dialog.hpp"
#include "src/ui/multiplayer-controller-select-dialog.hpp"
#include "src/ui/save-file-editor-dialog.hpp"
#include "src/ui/save-slot-editor-dialog.hpp"
#include "src/core/file-controller.hpp"
#include "src/core/retroarch.hpp"
#include "src/core/rom.hpp"
#include "src/core/preset-controllers.hpp"
#include "src/input/gamepad-controller.hpp"
#include "src/core/bps.hpp"

#include "src/ui/error-notifier.hpp"

MainWindow::MainWindow( const fs::path &romArg ) :
	QMainWindow( nullptr ),
	m_ui( new Ui::MainWindow ),
	m_showAllPlugins( false ),
	m_settings( FileController::loadAppSettings() )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );

	m_ui->pluginAngrylionRadio->setVisible( false );
	m_ui->pluginGlidenRadio->setVisible( false );
	m_ui->pluginRiceRadio->setVisible( false );

	m_ui->parallelPluginRadioGroup->setId( m_ui->pluginParallelRadio, (int)GfxPlugin::ParaLLEl );
	m_ui->parallelPluginRadioGroup->setId( m_ui->pluginGlideRadio, (int)GfxPlugin::Glide64 );
	m_ui->parallelPluginRadioGroup->setId( m_ui->pluginAngrylionRadio, (int)GfxPlugin::Angrylion );
	m_ui->parallelPluginRadioGroup->setId( m_ui->pluginGlidenRadio, (int)GfxPlugin::GlideN64 );
	m_ui->parallelPluginRadioGroup->setId( m_ui->pluginRiceRadio, (int)GfxPlugin::Rice );

	m_ui->mupenPluginRadioGroup->setId( m_ui->pluginParallelRadio2, (int)GfxPlugin::ParaLLEl );
	m_ui->mupenPluginRadioGroup->setId( m_ui->pluginAngrylionRadio2, (int)GfxPlugin::Angrylion );
	m_ui->mupenPluginRadioGroup->setId( m_ui->pluginGlidenRadio2, (int)GfxPlugin::GlideN64 );

	m_ui->controllerConfigButton->setIcon( Icon::gamepad() );
	m_ui->menuButton->setIcon( Icon::menu() );
	m_ui->refreshButton->setIcon( Icon::refresh() );
	m_ui->playSingleplayerButton->setIcon( Icon::play() );
	m_ui->playMultiplayerButton->setIcon( Icon::group() );

	const UiState uiState = FileController::loadUiState();
	std::vector<ROM> romList = FileController::loadRomList();

	if( romArg.empty() ) {
		m_ui->romList->updateRoms( romList );
		m_ui->romView->setCurrentIndex( romList.empty() ? 0 : 1 );
		restoreTreeState( uiState.romList );
	} else {
		bool foundRom = false;
		bool skipAdding = false;
		bool isBps = Bps::isBpsFile( romArg );
		string firstTag = "Uncategorized";
		fs::path romPath = romArg;
		int64 romLastModified = 0;
		int64 bpsLastModified;

		// If this is a bps file, change the rom path to point to the z64 that will get created by the patch
		if ( isBps ) {
			bpsLastModified = RomUtil::getLastModified( romArg );
			romPath.replace_extension(".z64");
		}
		
		for( const ROM &rom : romList ) {
			if( rom.path == romPath ) {
				foundRom = true;
				firstTag = *rom.tags.begin();
				break;
			}
		}
		
		// If the rom exists already, get the update timestamp
		if ( foundRom ) {
			romLastModified = RomUtil::getLastModified( romPath );
		}

		// If the provided path was a bps file and if either the rom didn't exist or the bps file is newer than the rom, patch the bps file
		if ( isBps && ( !foundRom || bpsLastModified > romLastModified ) ) {
			Bps::BpsApplyError applyErr = Bps::tryApplyBps( romArg, romList );
			if ( applyErr != Bps::BpsApplyError::None ) {
				switch ( applyErr ) {
					case Bps::BpsApplyError::NoBaserom:
						ErrorNotifier::alert( "No base ROM found for " + romArg.filename().string() );
						break;
					case Bps::BpsApplyError::InvalidBps:
						ErrorNotifier::alert( romArg.filename().string() + " is not a valid bps file." );
						break;
					case Bps::BpsApplyError::PatchFailed:
						ErrorNotifier::alert( "Failed to patch " + romArg.filename().string() + ", file may be corrupt." );
						break;
					default:
						ErrorNotifier::alert( "Unknown error while patching " + romArg.filename().string() );
						break;
				}
				// Don't add the patched rom to the list because the patching failed
				skipAdding = true;
			}
		}

		// If the rom is new (and a patch wasn't both attempted and failed)
		if( !foundRom && !skipAdding ) {
			romList.push_back( ROM{
				/* name */ romPath.stem().string(),
				/* internalName */ RomUtil::getInternalName( romPath ),
				/* folder */ romPath,
				/* emulator */ EmulatorCore::UseDefault,
				/* parallelPlugin */ GfxPlugin::UseDefault,
				/* mupenPlugin */ GfxPlugin::UseDefault,
				/* lastPlayed */ 0,
				/* playTime */ 0,
				/* tags */ std::set<string>(),
				/* notes */ "",
				/* overclockCPU */ true,
				/* overclockVI */ false,
				/* fileVersion */ RomUtil::getLastModified( romPath ),
				/* crc32 */ RomUtil::getCrc32( romPath )
			});

			std::vector<fs::path> savedRoms = FileController::loadSavedRoms();
			savedRoms.push_back( romPath );
			FileController::saveSavedRoms( savedRoms );
			FileController::saveRomList( romList );
		}

		m_ui->romList->updateRoms( romList );
		restoreTreeState( uiState.romList );
		m_ui->romList->selectRom( firstTag, romPath );

		if( foundRom ) {
			QMetaObject::invokeMethod( this, &MainWindow::playSingleplayer, Qt::QueuedConnection );
		}
	}

	m_ui->actionSettings->setIcon( Icon::configure() );
	m_ui->actionManageSources->setIcon( Icon::search() );
	m_ui->actionConfigureControllers->setIcon( Icon::gamepad() );
	m_ui->actionConfigureKeyboard->setIcon( Icon::keyboard() );

	m_ui->menuButton->addAction( m_ui->actionSettings );
	m_ui->menuButton->addAction( m_ui->actionManageSources );
	m_ui->menuButton->addAction( m_ui->actionConfigureControllers );
	m_ui->menuButton->addAction( m_ui->actionConfigureKeyboard );

	resize( uiState.windowWidth, uiState.windowHeight );

	if( uiState.showAllPlugins ) {
		morePluginsToggled();
	}

	connect( m_ui->refreshButton, SIGNAL(clicked()), this, SLOT(refreshRomList()) );
	connect( &m_romListFetcher, SIGNAL(romListFetched(FetchedRomList)), this, SLOT(romListFetched(FetchedRomList)) );
	connect( m_ui->romList->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(romSelectionChanged()) );
	connect( m_ui->parallelPluginRadioGroup, SIGNAL(buttonToggled(int,bool)), this, SLOT(parallelPluginChanged(int,bool)) );
	connect( m_ui->mupenPluginRadioGroup, SIGNAL(buttonToggled(int,bool)), this, SLOT(mupenPluginChanged(int,bool)) );
	connect( m_ui->romList, SIGNAL(addTag(const ROM*,string)), this, SLOT(addTag(const ROM*,string)) );
	connect( m_ui->romList, SIGNAL(removeTag(const ROM*,string)), this, SLOT(removeTag(const ROM*,string)) );
	connect( m_ui->romList, SIGNAL(editSave(const ROM*,fs::path)), this, SLOT(editSave(const ROM*,fs::path)) );

	reloadSettings();
	romSelectionChanged();

	refreshRomList();
}

MainWindow::~MainWindow() {
	m_ui->romList->model()->deleteLater();
	delete m_ui;
}

void MainWindow::reloadSettings() {
	m_settings = FileController::loadAppSettings();

	m_ui->romList->setColumnHidden( 0, false );
	m_ui->romList->setColumnHidden( 1, !Flags::has( m_settings.visibleColumns, RomInfoColumn::InternalName ) );
	m_ui->romList->setColumnHidden( 2, !Flags::has( m_settings.visibleColumns, RomInfoColumn::Path ) );
	m_ui->romList->setColumnHidden( 3, !Flags::has( m_settings.visibleColumns, RomInfoColumn::LastPlayed ) );
	m_ui->romList->setColumnHidden( 4, !Flags::has( m_settings.visibleColumns, RomInfoColumn::PlayTime ) );
	m_ui->romList->setColumnHidden( 5, !Flags::has( m_settings.visibleColumns, RomInfoColumn::Notes ) );
}

void MainWindow::refreshRomList() {
	m_ui->searchIndicator->setVisible( true );
	m_romListFetcher.fetchAsync();
}

void MainWindow::romListFetched( FetchedRomList roms ) {
	const TreeUiState uiState = saveTreeState();
	m_ui->romView->setCurrentIndex( roms->empty() ? 0 : 1 );
	m_ui->romList->updateRoms( *roms );
	FileController::saveRomList( *roms );
	restoreTreeState( uiState );
	m_ui->searchIndicator->setVisible( false );
}

void MainWindow::romSelectionChanged() {
	const ROM *rom = static_cast<const RomListModel*>( m_ui->romList->model() )->tryGetRomInfo( m_ui->romList->currentIndex() );
	const bool romSelected = (rom != nullptr);

	m_ui->overclockCpuCheckbox->setEnabled( romSelected );
	m_ui->overclockViCheckbox->setEnabled( romSelected );
	m_ui->pluginGroup->setEnabled( romSelected );
	m_ui->mupenPluginGroup->setEnabled( romSelected );
	m_ui->playSingleplayerButton->setEnabled( romSelected );
	m_ui->playMultiplayerButton->setEnabled( romSelected );

	if( romSelected ) {
		const EmulatorCore emulatorCore = (rom->emulator == EmulatorCore::UseDefault) ? m_settings.defaultEmulator : rom->emulator;
		const GfxPlugin parallelPlugin = (rom->parallelPlugin == GfxPlugin::UseDefault) ? m_settings.defaultParallelPlugin : rom->parallelPlugin;
		const GfxPlugin mupenPlugin = (rom->mupenPlugin == GfxPlugin::UseDefault) ? m_settings.defaultMupenPlugin : rom->mupenPlugin;
		m_ui->overclockCpuCheckbox->setChecked( rom->overclockCPU );
		m_ui->overclockViCheckbox->setChecked( rom->overclockVI );
		m_ui->parallelPluginRadioGroup->button( (int)parallelPlugin )->setChecked( true );
		m_ui->mupenPluginRadioGroup->button( (int)mupenPlugin )->setChecked( true );
		m_ui->emulatorTabs->setCurrentIndex( (int)emulatorCore - 1 );
	}

	overclockCpuToggled();
	overclockViToggled();
}

void MainWindow::updateRom( const ROM &updatedRom ) {
	//TODO: should make this more efficient
	std::vector<ROM> romList = FileController::loadRomList();
	for( ROM &rom : romList ) {
		if( rom.path != updatedRom.path ) continue;
		rom.emulator = updatedRom.emulator;
		rom.parallelPlugin = updatedRom.parallelPlugin;
		rom.mupenPlugin = updatedRom.mupenPlugin;
		rom.lastPlayed = updatedRom.lastPlayed;
		rom.playTime = updatedRom.playTime;
		rom.tags = updatedRom.tags;
		rom.notes = updatedRom.notes;
		rom.overclockCPU = updatedRom.overclockCPU;
		rom.overclockVI = updatedRom.overclockVI;
		break;
	}
	FileController::saveRomList( romList );
	const TreeUiState uiState = saveTreeState();
	m_ui->romList->updateRoms( romList );
	restoreTreeState( uiState );
}

void MainWindow::overclockCpuToggled() {
	const bool optimizationsDisabled = m_ui->overclockCpuCheckbox->isEnabled() && !m_ui->overclockCpuCheckbox->isChecked();
	m_ui->performanceWarningLabel->setVisible( optimizationsDisabled );

	const ROM *romPtr = m_ui->romList->tryGetSelectedRom();
	if( romPtr == nullptr ) return;

	if( romPtr->overclockCPU == optimizationsDisabled ) {
		ROM rom = *romPtr;
		rom.overclockCPU = !optimizationsDisabled;
		updateRom( rom );
	}
}

void MainWindow::overclockViToggled() {
	const bool overclock = m_ui->overclockViCheckbox->isEnabled() && m_ui->overclockViCheckbox->isChecked();

	const ROM *romPtr = m_ui->romList->tryGetSelectedRom();
	if( romPtr == nullptr ) return;

	if( romPtr->overclockVI != overclock ) {
		ROM rom = *romPtr;
		rom.overclockVI = overclock;
		updateRom( rom );
	}
}

void MainWindow::parallelPluginChanged( int pluginId, bool checked ) {
	if( !checked ) return;
	const GfxPlugin plugin = (GfxPlugin)pluginId;

	const ROM *romPtr = m_ui->romList->tryGetSelectedRom();
	if( romPtr == nullptr || romPtr->parallelPlugin == plugin ) return;

	ROM rom = *romPtr;
	rom.parallelPlugin = plugin;
	updateRom( rom );
}

void MainWindow::mupenPluginChanged( int pluginId, bool checked ) {
	if( !checked ) return;
	const GfxPlugin plugin = (GfxPlugin)pluginId;

	const ROM *romPtr = m_ui->romList->tryGetSelectedRom();
	if( romPtr == nullptr || romPtr->mupenPlugin == plugin ) return;

	ROM rom = *romPtr;
	rom.mupenPlugin = plugin;
	updateRom( rom );
}

void MainWindow::emulatorChanged() {
	const EmulatorCore emulator = (EmulatorCore)(m_ui->emulatorTabs->currentIndex() + 1);
	assert( emulator == EmulatorCore::ParallelN64 || emulator == EmulatorCore::Mupen64plusNext );

	const ROM *romPtr = m_ui->romList->tryGetSelectedRom();
	if( romPtr == nullptr || romPtr->emulator == emulator) return;

	ROM rom = *romPtr;
	rom.emulator = emulator;
	updateRom( rom );
}

void MainWindow::morePluginsToggled() {
	m_showAllPlugins = !m_showAllPlugins;
	m_ui->pluginAngrylionRadio->setVisible( m_showAllPlugins );
	m_ui->pluginGlidenRadio->setVisible( m_showAllPlugins );
	m_ui->pluginRiceRadio->setVisible( m_showAllPlugins );
	m_ui->showMorePluginsLinks->setText( m_showAllPlugins ?
		"<a href=\"#\">Show Fewer Plugins</a>" :
		"<a href=\"#\">Show More Plugins</a>"
	);
}

static const char *s_crashMessageParallel = ""
	"The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then "
	"this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics "
	"plugins. Alternatively, if you have a very old onboard graphics card, it is possible that Vulkan is not "
	"supported on your system. In either case, using the Glide64 graphics plugin might resolve the issue.";

static const char *s_crashMessageAngrylion = ""
	"The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then "
	"this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics "
	"plugins. If this is the case, try running the ROM with the Glide64 graphics plugin instead.";

#ifndef _WIN32
static const char *s_crashMessageFlatpak = ""
	"The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then "
	"this ROM is most likely corrupt. If you are not able to launch any ROMs, verify that you have not updated "
	"your graphics drivers since installing RetroArch in Flatpak. If you have, run `flatpak update` to sync "
	"the drivers in Flatpak with the rest of your system.";
#endif

static const char *s_crashMessageDefault = ""
	"The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then "
	"this ROM is most likely corrupt. If you are not able to launch any ROMs, check that the ParallelN64 "
	"emulator core is installed and that you are using RetroArch version 1.9 or later.";

static inline ConnectedGamepad getActiveController( AppSettings &settings ) {
	std::vector<ConnectedGamepad> connectedControllers = GamepadController::instance().getConnected();
	if( connectedControllers.empty() ) {
		return ConnectedGamepad{ -1, ControllerInfo() };
	} else if( connectedControllers.size() == 1 ) {
		return connectedControllers[0];
	}

	if( settings.preferredController.has_value() ) {
		for( const ConnectedGamepad &controller : connectedControllers ) {
			if( controller.info.uuid == settings.preferredController.value() ) {
				return controller;
			}
		}
	}

	SingleplayerControllerSelectDialog dialog( connectedControllers );
	dialog.exec();
	return dialog.getSelectedController();
}

static inline ControllerProfile getControllerProfile( const ConnectedGamepad &controller ) {
	if( controller.id < 0 ) {
		return DefaultProfile::XBox360;
	}

	const HashMap<Uuid,string> mappings = FileController::loadControllerMappings();
	if( mappings.count( controller.info.uuid ) > 0 ) {
		const string &activeProfile = mappings.at( controller.info.uuid );

		const std::map<string, ControllerProfile> profiles = FileController::loadControllerProfiles();
		if( profiles.count( activeProfile ) > 0 ) {
			return profiles.at( activeProfile );
		}
	}

	if( getControllerType( controller.info.controllerId ) == ControllerType::Gamecube ) {
		return DefaultProfile::Gamecube;
	}

	return DefaultProfile::XBox360;
}

void MainWindow::playSingleplayer() {
	const ConnectedGamepad activeController = getActiveController( m_settings );
	const std::vector<PlayerController> players = {
		{ getControllerProfile( activeController ), activeController.info.uuid }
	};
	launchEmulator( players, true );
}

void MainWindow::playMultiplayer() {
	MultiplayerControllerSelectDialog dialog;
	if( dialog.exec() != QDialog::Accepted ) return;

	const std::array<ConnectedGamepad,4> controllers = dialog.getControllers();
	std::vector<PlayerController> players;
	players.reserve( 4 );

	for( size_t i = 0; i < 4; i++ ) {
		if( controllers[i].id < 0 ) continue;

		while( i > players.size() ) players.push_back({ DefaultProfile::XBox360, Uuid() });
		players.push_back({ getControllerProfile( controllers[i] ), controllers[i].info.uuid });
	}

	if( players.empty() ) {
		players.push_back({ DefaultProfile::XBox360, 0 });
	}

	launchEmulator( players, dialog.canBindSavestates() );
}

static const char *const s_parallelNotInstalledMessage = ""
	"ParallelN64 is not installed. To install it, open RetroArch then go to Load Core, "
	"then Download a Core, then scroll down to find Nintendo - Nintendo 64 (ParaLLEl N64)."
	"\n\n"
	"If you have already installed ParallelN64 but are still seeing this message, you may "
	"have multiple RetroArch installs on your system. Verify that Parallel Launcher is "
	"using the correct one by going to Settings.";

static const char *const s_mupenNotInstalledMessage = ""
	"Mupen64plus-next is not installed. To install it, open RetroArch then go to Load Core, "
	"then Download a Core, then scroll down to find Nintendo - Nintendo 64 (Mupen64Plus-Next)."
	"\n\n"
	"If you have already installed Mupen64plus-next but are still seeing this message, you may "
	"have multiple RetroArch installs on your system. Verify that Parallel Launcher is "
	"using the correct one by going to Settings.";

void MainWindow::launchEmulator(
	const std::vector<PlayerController> &players,
	bool bindSavestate
) {
	const ROM *rom = m_ui->romList->tryGetSelectedRom();
	assert( rom != nullptr );

	const EmulatorCore emulatorCore = (rom->emulator == EmulatorCore::UseDefault) ? m_settings.defaultEmulator : rom->emulator;

	if( emulatorCore == EmulatorCore::ParallelN64 && !RetroArch::isParallelN64Installed() ) {
		QMessageBox::critical( this, "Missing emulator core", s_parallelNotInstalledMessage );
		return;
	}

	if( emulatorCore == EmulatorCore::Mupen64plusNext && !RetroArch::isMupenInstalled() ) {
		QMessageBox::critical( this, "Missing emulator core", s_mupenNotInstalledMessage );
		return;
	}

	const GfxPlugin gfxPlugin = (emulatorCore == EmulatorCore::ParallelN64) ?
		((rom->parallelPlugin == GfxPlugin::UseDefault) ? m_settings.defaultParallelPlugin : rom->parallelPlugin) :
		((rom->mupenPlugin == GfxPlugin::UseDefault) ? m_settings.defaultMupenPlugin : rom->mupenPlugin);

	AsyncProcess *emulator = new AsyncProcess();
	try {
		*emulator = RetroArch::launchRom(
			rom->path,
			m_settings,
			players,
			emulatorCore,
			gfxPlugin,
			m_ui->overclockCpuCheckbox->isChecked(),
			m_ui->overclockViCheckbox->isChecked(),
			bindSavestate
		);
	} catch( ... ) {
		QMessageBox::critical( this, "Emulator Missing", "Failed to launch emulator. It does not appear to be installed." );
		return;
	}

	const QRect winGeo = geometry();
	hide();

	NowPlayingWindow *nowPlayingWindow = nullptr;
	if( !m_settings.hideWhenPlaying ) {
		nowPlayingWindow = new NowPlayingWindow( emulator, rom->name, rom->playTime );
		nowPlayingWindow->show();
	}

	ProcessAwaiter::QtSafeAwait(
		emulator,
		[=]( [[maybe_unused]] int64 exitCode, int64 runtimeMs ) {
			delete emulator;
			if( nowPlayingWindow != nullptr ) {
				nowPlayingWindow->close();
				nowPlayingWindow->deleteLater();
			}

			if( runtimeMs > 0 ) {
				ROM updatedRom = *rom;
				updatedRom.playTime += runtimeMs;
				updatedRom.lastPlayed = std::chrono::duration_cast<std::chrono::milliseconds>(
					std::chrono::system_clock::now().time_since_epoch()
				).count();
				updateRom( updatedRom );
			}

			this->setGeometry( winGeo );
			this->show();
			QTimer::singleShot( 50, [=](){ this->setGeometry( winGeo ); } );

			if( runtimeMs < 2000 ) {
				const char *errorMessage;
				switch( gfxPlugin ) {
					case GfxPlugin::ParaLLEl:
						errorMessage = s_crashMessageParallel;
						break;
					case GfxPlugin::Angrylion:
						errorMessage = s_crashMessageAngrylion;
						break;
					default:
#ifdef _WIN32
						errorMessage = s_crashMessageDefault;
#else
						errorMessage = m_settings.usingFlatpak ? s_crashMessageFlatpak : s_crashMessageDefault;
#endif
						break;
				}
				QMessageBox::critical( this, "Possible ROM Error", errorMessage );
			}
		}
	);
}

void MainWindow::configureController() {
	ControllerSelectDialog dialog;
	dialog.exec();
}

void MainWindow::configureKeyboard() {
	KeyboardConfigDialog dialog;
	dialog.exec();
}

void MainWindow::editSettings() {
	SettingsDialog *dialog = new SettingsDialog();
	dialog->exec();
	delete dialog;
	reloadSettings();
}

void MainWindow::manageRomSources() {
	RomSourceDialog *dialog = new RomSourceDialog();
	dialog->exec();
	delete dialog;
	refreshRomList();
}

void MainWindow::closeEvent( QCloseEvent *event ) {
	const UiState uiState = {
		saveTreeState(),
		width(),
		height(),
		m_ui->pluginRiceRadio->isVisible()
	};

	FileController::saveUiState( uiState );
	QMainWindow::closeEvent( event );
	QCoreApplication::quit();
}

TreeUiState MainWindow::saveTreeState() const {
	TreeUiState uiState;

	const ROM *selectedRom = m_ui->romList->tryGetSelectedRom();
	if( selectedRom != nullptr ) {
		uiState.selectedRom = selectedRom->path;
		uiState.selectedGroup = static_cast<const RomListModel*>( m_ui->romList->model() )->tryGetGroup( m_ui->romList->currentIndex().parent() );
	}

	const int numGroups = m_ui->romList->model()->rowCount();
	for( int i = 0; i < numGroups; i++ ) {
		const QModelIndex index = m_ui->romList->model()->index( i, 0 );
		if( m_ui->romList->isExpanded( index ) ) {
			uiState.expandedGroups.push_back(
				static_cast<const RomListModel*>( m_ui->romList->model() )->tryGetGroup( index )
			);
		}
	}

	return uiState;
}

void MainWindow::restoreTreeState( const TreeUiState &state ) {
	for( const string &group : state.expandedGroups ) {
		const QModelIndex index = static_cast<const RomListModel*>( m_ui->romList->model() )->tryGetIndex( group );
		if( index.isValid() ) {
			m_ui->romList->expand( index );
		}
	}

	if( !state.selectedRom.empty() ) {
		m_ui->romList->selectRom( state.selectedGroup, state.selectedRom );
	}

	romSelectionChanged();
}


void MainWindow::addTag( const ROM *rom, string tag ) {
	ROM updatedRom = *rom;
	updatedRom.tags.insert( tag );
	updateRom( updatedRom );
}

void MainWindow::removeTag( const ROM *rom, string tag ) {
	ROM updatedRom = *rom;
	updatedRom.tags.erase( tag );
	updateRom( updatedRom );
}

void MainWindow::editSave( [[maybe_unused]] const ROM *rom, fs::path saveFilePath ) {
	SaveFileEditorDialog dialog( saveFilePath );
	if( dialog.exec() == QDialog::Accepted ) {
		SaveSlotEditorDialog dialog2( saveFilePath, dialog.getSaveSlot() );
		dialog2.exec();
	}
}
