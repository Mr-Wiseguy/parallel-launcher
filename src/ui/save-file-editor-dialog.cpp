#include "src/ui/save-file-editor-dialog.hpp"
#include "ui_save-file-editor-dialog.h"

#include <fstream>
#include <cstring>
#include "src/core/numeric-string.hpp"
#include "src/ui/icons.hpp"

static inline string getSlotName( int slotIndex, const SM64::SaveFile &saveFile ) {
	const SM64::SaveSlot &slot = saveFile.slot( slotIndex );
	if( !Flags::has( slot.flags, SM64::SaveFileFlag::Exists ) ) {
		return "Slot "s + (char)('A' + (char)slotIndex) + " (Empty)";
	}

	return "Slot "s + (char)('A' + (char)slotIndex) + " (" + Number::toString( slot.countStars() ) + " Stars)";
}

static inline void commitChanges( const fs::path &filePath, const SM64::SaveFile &saveData ) {
	std::ofstream file( filePath.string(), std::ios_base::out | std::ios_base::binary | std::ios_base::trunc );
	saveData.write( file );
}

SaveFileEditorDialog::SaveFileEditorDialog( const fs::path &saveFilePath ) :
	QDialog( nullptr ),
	m_ui( new Ui::SaveFileEditorDialog ),
	m_filePath( saveFilePath )
{
	m_ui->setupUi( this );

	std::ifstream file( saveFilePath, std::ios_base::in | std::ios_base::binary );
	m_saveFile = SM64::SaveFile::read( file );

	for( int i = 0; i < 4; i++ ) {
		m_ui->saveSlot->setItemText( i, getSlotName( i, m_saveFile ).c_str() );
	}

	m_ui->deleteButton->setIcon( Icon::delet() );
	m_ui->editButton->setIcon( Icon::edit() );
	setWindowIcon( Icon::appIcon() );

	m_ui->saveSlot->setCurrentIndex( 0 );
	slotSelected( 0 );
}

SaveFileEditorDialog::~SaveFileEditorDialog() {
	delete m_ui;
}

int SaveFileEditorDialog::getSaveSlot() const {
	return m_ui->saveSlot->currentIndex();
}

void SaveFileEditorDialog::slotSelected( int slotIndex ) {
	const SM64::SaveSlot &slot = m_saveFile.slot( slotIndex );
	if( Flags::has( slot.flags, SM64::SaveFileFlag::Exists ) ) {
		m_ui->deleteButton->setVisible( true );
		m_ui->editButton->setText( "Edit Save Slot" );
		m_ui->editButton->setIcon( Icon::edit() );
	} else {
		m_ui->deleteButton->setVisible( false );
		m_ui->editButton->setText( "Create Save Slot" );
		m_ui->editButton->setIcon( Icon::add() );
	}
}

void SaveFileEditorDialog::deleteSlot() {
	const int slotIndex = m_ui->saveSlot->currentIndex();
	SM64::SaveSlot &slot = m_saveFile.slot( slotIndex );
	std::memset( &slot, 0, sizeof( SM64::SaveSlot ) );
	commitChanges( m_filePath, m_saveFile );
	m_ui->saveSlot->setItemText( slotIndex, getSlotName( slotIndex, m_saveFile ).c_str() );
	slotSelected( slotIndex );
}

void SaveFileEditorDialog::accept() {
	SM64::SaveSlot &slot = m_saveFile.slot( m_ui->saveSlot->currentIndex() );
	if( !Flags::has( slot.flags, SM64::SaveFileFlag::Exists ) ) {
		std::memset( &slot, 0, sizeof( SM64::SaveSlot ) );
		slot.flags = SM64::SaveFileFlag::Exists;
		commitChanges( m_filePath, m_saveFile );
	}

	QDialog::accept();
}
