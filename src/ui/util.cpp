#include "src/ui/util.hpp"

#include <QWidget>

static void freeLayoutItem( QLayoutItem *item ) {
	if( item->layout() ) {
		UiUtil::clearLayout( item->layout() );
		item->layout()->deleteLater();
	}

	if( item->widget() ) {
		item->widget()->deleteLater();
	}

	delete item;
}

void UiUtil::clearLayout( QLayout *layout ) {
	while( QLayoutItem *item = layout->takeAt( 0 ) ) {
		freeLayoutItem( item );
	}
}

void UiUtil::shrinkToFit( QDialog *dialog, Qt::Orientation shrinkDirection ) {
	QRect geo = dialog->geometry();
	if( shrinkDirection & Qt::Horizontal ) {
		geo.setWidth( 0 );
	}
	if( shrinkDirection & Qt::Vertical ) {
		geo.setHeight( 0 );
	}
	dialog->setGeometry( geo );
}
