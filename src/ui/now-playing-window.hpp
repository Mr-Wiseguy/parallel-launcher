#ifndef SRC_UI_NOW_PLAYING_WINDOW_HPP_
#define SRC_UI_NOW_PLAYING_WINDOW_HPP_

#include <QMainWindow>
#include <QTimer>
#include "src/polyfill/process.hpp"

namespace Ui {
	class NowPlayingWindow;
}

class NowPlayingWindow : public QMainWindow {
	Q_OBJECT

	private:
	Ui::NowPlayingWindow *const m_ui;
	AsyncProcess *const m_process;
	QTimer m_timer;
	const int64 m_pastPlayTime;
	const int64 m_startTime;

	public:
	explicit NowPlayingWindow( AsyncProcess *process, const string &romName, int64 pastPlayTime );
	virtual ~NowPlayingWindow();

	int64 getSessionTime() const;

	private slots:
	void killEmulator();
	void updateTimers();

};



#endif /* SRC_UI_NOW_PLAYING_WINDOW_HPP_ */
