#ifndef SRC_UI_UI_STATE_HPP_
#define SRC_UI_UI_STATE_HPP_

#include "src/core/filesystem.hpp"
#include "src/core/json.hpp"
#include "src/types.hpp"
#include <vector>

struct TreeUiState {
	fs::path selectedRom;
	string selectedGroup;
	std::vector<string> expandedGroups;
};

struct UiState {
	TreeUiState romList;
	int windowWidth;
	int windowHeight;
	bool showAllPlugins;

	static const UiState Default;
};

namespace JsonSerializer {
	template<> void serialize<UiState>( JsonWriter &jw, const UiState &obj );
	template<> UiState parse<UiState>( const Json &json );
}

#endif /* SRC_UI_UI_STATE_HPP_ */
