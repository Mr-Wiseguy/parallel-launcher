#include "src/ui/rom-list-fetcher.hpp"
#include "src/core/file-controller.hpp"

RomListFetcher::RomListFetcher() :
	QObject( nullptr ),
	m_thread( nullptr )
{
	[[maybe_unused]] static const int s_typeId = qRegisterMetaType<FetchedRomList>();
}

RomListFetcher::~RomListFetcher() {
	if( m_thread == nullptr ) return;
	m_thread->cancel();
	m_thread->wait();
	delete m_thread;
}

void RomListFetcher::fetchAsync() {
	if( m_thread != nullptr ) {
		m_thread->cancel();
		m_thread->wait();
		delete m_thread;
	}

	m_thread = new _RomListFetcherThread;
	QObject::connect( m_thread, &_RomListFetcherThread::done, this, &RomListFetcher::romListFetched, Qt::QueuedConnection );
	m_thread->start();
}

static inline FetchedRomList packResult( std::vector<ROM> &stackVal ) {
	std::vector<ROM> *heapVal = new std::vector<ROM>;
	*heapVal = std::move( stackVal );
	return FetchedRomList( heapVal );
}

void _RomListFetcherThread::run() {
	const std::vector<ROM> romListSnapshot = FileController::loadRomList();
	const std::vector<RomSource> romSourcesSnapshot = FileController::loadRomSources();
	const std::vector<fs::path> additonalRomsSnapshot = FileController::loadSavedRoms();

	std::vector<ROM> roms = RomFinder::scan(
		romListSnapshot,
		romSourcesSnapshot,
		additonalRomsSnapshot,
		m_cancellationToken
	);

	if( !m_cancellationToken.isCancelled() ) {
		emit done( packResult( roms ) );
	}
}
