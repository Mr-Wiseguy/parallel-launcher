#include "src/ui/controller-config-dialog.hpp"
#include "ui_controller-config-dialog.h"

#include <cassert>
#include <cstring>
#include <QInputDialog>
#include <QMessageBox>
#include "src/input/gamepad-controller.hpp"
#include "src/core/preset-controllers.hpp"
#include "src/core/file-controller.hpp"
#include "src/core/numeric-string.hpp"
#include "src/ui/bind-input-dialog.hpp"
#include "src/ui/neutral-input-dialog.hpp"
#include "src/ui/icons.hpp"

static const char *s_actionNames[] = {
	"Analog Up",
	"Analog Down",
	"Analog Left",
	"Analog Right",
	"C Up",
	"C Down",
	"C Left",
	"C Right",
	"D-Pad Up",
	"D-Pad Down",
	"D-Pad Left",
	"D-Pad Right",
	"A Button",
	"B Button",
	"L Trigger",
	"Z Trigger",
	"R Trigger",
	"Start Button",
	"Save State",
	"Load State"
};

static_assert( sizeof( s_actionNames) == sizeof(char*) * (size_t)ControllerAction::NUM_ACTIONS );

ControllerConfigDialog::ControllerConfigDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ControllerConfigDialog ),
	m_controllerId( -1 )
{
	m_ui->setupUi( this );
	m_profile = DefaultProfile::XBox360;

	setWindowIcon( Icon::appIcon() );
	m_ui->quickConfigureButton->setIcon( Icon::fastForward() );
	m_ui->saveAsButton->setIcon( Icon::saveAs() );
	m_ui->saveButton->setIcon( Icon::save() );
	m_ui->cancelButton->setIcon( Icon::cancel() );

	connect( m_ui->bindingButtonGroup, SIGNAL( buttonClicked(QAbstractButton*) ), this, SLOT( bindSingle(QAbstractButton*) ) );
}

ControllerConfigDialog::~ControllerConfigDialog() {
	delete m_ui;
}

static void updateButtonText( QAbstractButton *button, const ControllerProfile &profile ) {
	const Binding &binding = profile.bindings[button->property( "controllerBinding" ).toInt()];
	switch( binding.type ) {
		case BindingType::None:
			button->setText( "Not Bound" );
			break;
		case BindingType::Button:
			button->setText( ("Button "s + Number::toString( binding.buttonOrAxis )).c_str() );
			break;
		case BindingType::AxisNegative:
			button->setText( ("Axis -"s + Number::toString( binding.buttonOrAxis )).c_str() );
			break;
		case BindingType::AxisPositive:
			button->setText( ("Axis +"s + Number::toString( binding.buttonOrAxis )).c_str() );
			break;
	}
}

void ControllerConfigDialog::setActiveController(
	const string &name,
	const Uuid &uuid,
	int id,
	const ControllerProfile &profile
) {
	m_ui->controllerNameLabel->setText( name.c_str() );
	m_profile = profile;
	m_controllerUuid = uuid;
	m_controllerId = id;

	m_ui->rumbleCheckbox->setChecked( profile.rumble );
	m_ui->sensitivitySpinner->setValue( profile.sensitivity );
	m_ui->deadzoneSpinner->setValue( profile.deadzone );

	for( QAbstractButton *button : m_ui->bindingButtonGroup->buttons() ) {
		updateButtonText( button, profile );
	}

	m_ui->saveButton->setEnabled( !DefaultProfile::exists( profile.name ) );
}

void ControllerConfigDialog::save() {
	assert( !DefaultProfile::exists( m_profile.name ) );

	m_profile.rumble = m_ui->rumbleCheckbox->isChecked();
	m_profile.sensitivity = m_ui->sensitivitySpinner->value();
	m_profile.deadzone = m_ui->deadzoneSpinner->value();

	std::map<string, ControllerProfile> savedProfiles = FileController::loadControllerProfiles();
	savedProfiles[m_profile.name] = m_profile;
	FileController::saveControllerProfiles( savedProfiles );

	accept();
}

void ControllerConfigDialog::saveAs() {
	QString profileName = QInputDialog::getText( this, "Enter Profile Name", "Enter a name for your new controller profile." );
	if( profileName.isNull() || profileName.isEmpty() ) {
		return;
	}

	if( DefaultProfile::exists( profileName.toStdString() ) ) {
		QMessageBox::critical( this, "Failed to Save Profile", "A default controller profile with that name already exists." );
		return;
	}

	//TODO: warn if overwriting an existing profile

	m_profile.name = profileName.toStdString();
	save();
}

void ControllerConfigDialog::bindAll() {
	Binding previousBindings[(ubyte)ControllerAction::NUM_ACTIONS];
	std::memcpy( previousBindings, m_profile.bindings, sizeof( m_profile.bindings ) );

	bool cancelled = false;
	for( QAbstractButton *button : m_ui->bindingButtonGroup->buttons() ) {
		const int action = button->property( "controllerBinding" ).toInt();

		BindInputDialog dialog( s_actionNames[action], m_controllerId, true );
		m_profile.bindings[action] = dialog.getBinding( cancelled );
		updateButtonText( button, m_profile );

		if( cancelled ) break;

		if( m_profile.bindings[action].type != BindingType::None ) {
			NeutralInputDialog( m_controllerId ).exec();
		}
	}

	if( cancelled ) {
		std::memcpy( m_profile.bindings, previousBindings, sizeof( m_profile.bindings ) );
		for( QAbstractButton *button : m_ui->bindingButtonGroup->buttons() ) {
			updateButtonText( button, m_profile );
		}
	}
}

void ControllerConfigDialog::bindSingle( QAbstractButton *button ) {
	const int action = button->property( "controllerBinding" ).toInt();

	BindInputDialog dialog( s_actionNames[action], m_controllerId, false );
	m_profile.bindings[action] = dialog.getBinding();

	updateButtonText( button, m_profile );
}
