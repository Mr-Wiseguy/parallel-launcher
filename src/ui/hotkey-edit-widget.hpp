#ifndef SRC_UI_HOTKEY_EDIT_WIDGET_HPP_
#define SRC_UI_HOTKEY_EDIT_WIDGET_HPP_

#include <QPushButton>

class HotkeyEditWidget : public QPushButton {
	Q_OBJECT

	private:
	Qt::Key m_hotkey;
	bool m_active;

	public:
	HotkeyEditWidget( QWidget *parent = nullptr );
	virtual ~HotkeyEditWidget() {}

	inline Qt::Key value() const { return m_hotkey; }
	inline bool hasValue() const { return m_hotkey != 0; }

	protected:
	virtual void focusOutEvent( QFocusEvent *event ) override;
	virtual void keyPressEvent( QKeyEvent *event ) override;

	public slots:
	void setValue( Qt::Key hotkey );
	void clear();

	private slots:
	void onClick();

	signals:
	void hotkeyChanged( Qt::Key );
};

namespace Ui {
	class HotkeyEditPanel;
}

class HotkeyEditPanel : public QWidget {
	Q_OBJECT

	private:
	Ui::HotkeyEditPanel *m_ui;

	public:
	HotkeyEditPanel( QWidget *parent = nullptr );
	virtual ~HotkeyEditPanel();

	Qt::Key value() const;
	inline bool hasValue() const { return value() != 0; }

	public slots:
	void setValue( Qt::Key hotkey );
	void clear();

	signals:
	void hotkeyChanged( Qt::Key );
};


#endif /* SRC_UI_HOTKEY_EDIT_WIDGET_HPP_ */
