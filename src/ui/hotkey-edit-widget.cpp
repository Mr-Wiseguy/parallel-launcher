#include "src/ui/hotkey-edit-widget.hpp"
#include "ui_hotkey-edit-panel.h"

#include <QKeyEvent>
#include "src/input/keyboard.hpp"
#include "src/ui/icons.hpp"

static const char *const s_textNone = "None";
static constexpr Qt::Key NO_KEY = (Qt::Key)0;

HotkeyEditWidget::HotkeyEditWidget( QWidget *parent ) :
	QPushButton( parent ),
	m_hotkey( NO_KEY ),
	m_active( false )
{
	connect( this, SIGNAL( clicked() ), this, SLOT( onClick() ) );
	setIcon( Icon::configure() );
	setText( s_textNone );
	setStyleSheet( "text-align:left;" );
}

void HotkeyEditWidget::setValue( Qt::Key hotkey ) {
	hotkey = (Qt::Key)(hotkey & 0x21FFFFFF);

	const char *const keyName = Keycode::getNames( hotkey ).displayName;
	const Qt::Key prevKey = m_hotkey;
	if( keyName[0] == '\0' ) {
		m_hotkey = NO_KEY;
		if( !m_active ) setText( s_textNone );
	} else {
		m_hotkey = hotkey;
		if( !m_active ) setText( keyName );
	}

	if( m_hotkey != prevKey ) {
		emit hotkeyChanged( m_hotkey );
	}
}

void HotkeyEditWidget::clear() {
	setValue( NO_KEY );
}

void HotkeyEditWidget::onClick() {
	m_active = true;
	setText( "Input..." );
}

void HotkeyEditWidget::focusOutEvent( QFocusEvent *event ) {
	m_active = false;
	const char *const keyName = Keycode::getNames( m_hotkey ).displayName;
	setText( keyName[0] == '\0' ? s_textNone : keyName );
	QPushButton::focusOutEvent( event );
}

void HotkeyEditWidget::keyPressEvent( QKeyEvent *event ) {
	if( !m_active ) {
		QPushButton::keyPressEvent( event );
		return;
	}

	Qt::Key key = (Qt::Key)(event->key() & 0x21FFFFFF);
#ifdef _WIN32
	if( event->nativeVirtualKey() >= 96 && event->nativeVirtualKey() <= 111 ) {
#else
	if( (event->nativeVirtualKey() & 0xFF80) == 0xFF80 ) {
#endif
		key = (Qt::Key)(key | Qt::KeypadModifier );
	}

	const char *keyName = Keycode::getNames( key ).displayName;
	if( keyName[0] == '\0' && (key & Qt::KeypadModifier) ) {
		key = (Qt::Key)(key - Qt::KeypadModifier);
		keyName = Keycode::getNames( key ).displayName;
	}

	if( keyName[0] == '\0' ) {
		QPushButton::keyPressEvent( event );
		return;
	}

	m_active = false;
	setValue( key );
}

HotkeyEditPanel::HotkeyEditPanel( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::HotkeyEditPanel )
{
	m_ui->setupUi( this );
	m_ui->clearButton->setIcon( Icon::clear() );
	connect( m_ui->hotkeyEdit, SIGNAL( hotkeyChanged( Qt::Key ) ), this, SIGNAL( hotkeyChanged( Qt::Key ) ) );
}

HotkeyEditPanel::~HotkeyEditPanel() {
	delete m_ui;
}

Qt::Key HotkeyEditPanel::value() const {
	return m_ui->hotkeyEdit->value();
}

void HotkeyEditPanel::setValue( Qt::Key hotkey ) {
	m_ui->hotkeyEdit->setValue( hotkey );
}

void HotkeyEditPanel::clear() {
	m_ui->hotkeyEdit->clear();
}
