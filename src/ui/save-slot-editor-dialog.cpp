#include "src/ui/save-slot-editor-dialog.hpp"
#include "ui_save-slot-editor-dialog.h"

#include <fstream>
#include <cstring>
#include <QHBoxLayout>
#include "src/core/sm64.hpp"
#include "src/ui/util.hpp"
#include "src/ui/icons.hpp"

static const char *REDS = "8 Red Coins";
static const char *COINS = "100 Coins";

static const char *s_starNames[26][7] = {
	/* Castle */
	{
		"Toad 1",
		"Toad 2",
		"Toad 3",
		"MIPS 1",
		"MIPS 2",
		nullptr,
		nullptr
	},
	/* Bob-Omb's Battlefield */
	{
		"Big Bob-Omb on the Summit",
		"Footface with Koopa the Quick",
		"Shoot to the Island in the Sky",
		"Find the 8 Red Coins",
		"Mario Wings to the Sky",
		"Behind Chain Chomp's Gate",
		COINS
	},
	/* Whomp's Fortress */
	{
		"Chip Off Whomp's Block",
		"To the Top of the Fortress",
		"Shoot into the Wild Blue",
		"Red Coins on the Floating Isle",
		"Fall onto the Caged Island",
		"Blast Away the Wall",
		COINS
	},
	/* Jolly Rodger Bay */
	{
		"Plunder in the Sunken Ship",
		"Can the Eel Come Out to Play?",
		"Treasure of the Ocean Cave",
		"Red Coins on the Ship Afloat",
		"Blast to the Stone Pillar",
		"Through the Jet Stream",
		COINS
	},
	/* Cool Cool Mountain */
	{
		"Slip Slidin' Away",
		"Li'l Penguin Lost",
		"Big Penguin Race",
		"Frosty Slide for 8 Red Coins",
		"Snowman's Lost his Head",
		"Wall Kicks Will Work",
		COINS
	},
	/* Big Boo's Haunt */
	{
		"Go on a Ghost Hunt",
		"Ride Big Boo's Merry-Go-Round",
		"Secret of the Haunted Books",
		"Seek the 8 Red Coins",
		"Big Boo's Balcony",
		"Eye to Eye in the Secret Room",
		COINS
	},
	/* Hazy Maze Cave */
	{
		"Swimming Beast in the Cavern",
		"Elevate for 8 Red Coins",
		"Metal-Head Mario Can Move!",
		"Navigating the Toxic Maze",
		"A-maze-ing Emergency Exit",
		"Watch for Rolling Rocks",
		COINS
	},
	/* Lethal Lava Land */
	{
		"Boil the Big Bully",
		"Bully the Bullies",
		"8-Coin Puzzle with 15 Pieces",
		"Red-Hot Log Rolling",
		"Hot-Foot-it into the Volcano",
		"Elevator Tour in the Volcano",
		COINS
	},
	/* Shifting Sand Land */
	{
		"In the Talons of the Big Bird",
		"Shining Atop the Pyramid",
		"Inside the Ancient Pyramid",
		"Stand Tall on the Four Pillars",
		"Free Flying for 8 Red Coins",
		"Pyramid Puzzle",
		COINS
	},
	/* Dire Dire Docks */
	{
		"Board Bowser's Sub",
		"Chests in the Current",
		"Pole-Jumping for Red Coins",
		"Through the Jet Stream",
		"The Manta Ray's Reward",
		"Collect the Caps...",
		COINS
	},
	/* Snowman's Land */
	{
		"Snowman's Big Head",
		"Chill with the Bully",
		"In the Deep Freeze",
		"Whirl from the Freezing Pond",
		"Shell Shreddin' for Red Coins",
		"Into the Igloo",
		COINS
	},
	/* Wet Dry World */
	{
		"Shocking Arrow Lifts!",
		"Top o' the Town",
		"Secrets in the Shallows & Sky",
		"Express Evelator--Hurry Up!",
		"Go to Town for Red Coins",
		"Quick Race through Downtown!",
		COINS
	},
	/* Tall, Tall Mountain */
	{
		"Scale the Mountain",
		"Mystery of the Monkey's Cage",
		"Scary 'Shrooms, Red Coins",
		"Mysterious Mountainside",
		"Breathtaking View from Bridge",
		"Blast to the Lonely Mushroom",
		COINS
	},
	/* Tiny-Huge Island */
	{
		"Pluck the Piranha Flower",
		"The Tip Top of the Huge Island",
		"Rematch with Koopa the Quick",
		"Five Itty Bitty Secrets",
		"Wiggler's Red Coins",
		"Make Wiggler Squirm",
		COINS
	},
	/* Tick Tock Clock */
	{
		"Roll into the Cage",
		"The Pit and the Pendulums",
		"Get a Hand",
		"Stomp on the Thwomp",
		"Timed Jumps on Moving Bars",
		"Stop Time for Red Coins",
		COINS
	},
	/* Rainbow Ride */
	{
		"Cruiser Crossing the Rainbow",
		"The Big House in the Sky",
		"Coins Amassed in a Maze",
		"Swingin' in the Breeze",
		"Tricky Triangles!",
		"Somewhere Over the Rainbow",
		COINS
	},
	/* Bowser in the Dark World */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Bowser in the Fire Sea */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Bowser in the Sky */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Peach's Secret Slide */
	{ "Box Star", "Timer Star", nullptr, nullptr, nullptr, nullptr },
	/* Cavern of the Metal Cap */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Tower of the Wing Cap */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Vanish Cap Under the Moat */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Winged Mario Over the Rainbow */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* Secret Aquarium */
	{ REDS, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
	/* End Screen */
	{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr }
};

const bool s_vanillaCannons[26] = {
	false,
	true, // Bob-Omb's Battlefield
	true, // Whomp's Fortress
	true, // Jolly Rodger Bay
	true, // Cool Cool Mountain
	false,
	false,
	false,
	true, // Shifting Sand Land
	false,
	true, // Snowman's Land
	true, // Wet Dry World
	true, // Tall Tall Mountain
	true, // Tiny-Huge Island
	false,
	true, // Rainbow Ride
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	true, // Winged Mario Over the Rainbow
	false,
	false
};

static const char *s_columnHeaders[9] = {
	"Act 1 Star",
	"Act 2 Star",
	"Act 3 Star",
	"Act 4 Star",
	"Act 5 Star",
	"Act 6 Star",
	"100 Coin Star",
	"Cannon Status",
	"Coin High Score"
};

static const char *s_courseNames[26] = {
	"Bob-Omb's Battlefield",
	"Whomp's Fortress",
	"Jolly Rodger Bay",
	"Cool Cool Mountain",
	"Big Boo's Haunt",
	"Hazy Maze Cave",
	"Lethal Lava Land",
	"Shifting Sand Land",
	"Dire Dire Docks",
	"Snowman's Land",
	"Wet Dry World",
	"Tall, Tall Mountain",
	"Tiny-Huge Island",
	"Tick Tock Clock",
	"Rainbow Ride",
	"Bowser in the Dark World",
	"Bowser in the Fire Sea",
	"Bowser in the Sky",
	"Peach's Secret Slide",
	"Cavern of the Metal Cap",
	"Tower of the Wing Cap",
	"Vanish Cap Under the Moat",
	"Winged Mario Over the Rainbow",
	"Secret Aquarium",
	"End Screen",
	"Peach's Castle"
};

static const SM64::LevelId s_levelIdMap[25] = {
	SM64::LevelId::BobOmbBattlefield,
	SM64::LevelId::WhompsFortress,
	SM64::LevelId::JollyRodgerBay,
	SM64::LevelId::CoolCoolMountain,
	SM64::LevelId::BigBoosHaunt,
	SM64::LevelId::HazyMazeCave,
	SM64::LevelId::LethalLavaLand,
	SM64::LevelId::ShiftingSandLand,
	SM64::LevelId::DireDireDocks,
	SM64::LevelId::SnowmansLands,
	SM64::LevelId::WetDryWorld,
	SM64::LevelId::TallTallMountain,
	SM64::LevelId::TinyHugeIsland,
	SM64::LevelId::TickTockClock,
	SM64::LevelId::RainbowRide,
	SM64::LevelId::BowserInTheDarkWorld,
	SM64::LevelId::BowserInTheFireSea,
	SM64::LevelId::BowserInTheSky,
	SM64::LevelId::PeachsSecretSlide,
	SM64::LevelId::CavernOfTheMetalCap,
	SM64::LevelId::TowerOfTheWingCap,
	SM64::LevelId::VanishCapUnderTheMoat,
	SM64::LevelId::WingedMarioOverTheRainbow,
	SM64::LevelId::SecretAquarium,
	SM64::LevelId::EndScreen
};

inline static void addLevelEntry( QComboBox *select, const char *name, SM64::LevelId levelId ) {
	select->addItem( name, QVariant::fromValue<uint>( (uint)levelId ) );
}

SaveSlotEditorDialog::SaveSlotEditorDialog( const fs::path &saveFilePath, int slot ) :
	QDialog( nullptr ),
	m_ui( new Ui::SaveSlotEditorDialog ),
	m_filePath( saveFilePath ),
	m_slot( slot )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );

	m_starIcons.reserve( 26 * 8 );
	m_unusedStars.reserve( 77 );

	std::ifstream file( saveFilePath.string(), std::ios_base::in | std::ios_base::binary );
	const SM64::SaveFile saveFile = SM64::SaveFile::read( file );
	const SM64::SaveSlot &saveSlot = saveFile.slot( slot );

	addLevelEntry( m_ui->capLevel, "-- None --", SM64::LevelId::Null );
	addLevelEntry( m_ui->capLevel, "Castle Grounds", SM64::LevelId::CastleGrounds );
	addLevelEntry( m_ui->capLevel, "Inside the Castle", SM64::LevelId::CastleInterior );
	addLevelEntry( m_ui->capLevel, "Castle Courtyard", SM64::LevelId::CastleCourtyard );
	for( int i = 0; i < 25; i++ ) {
		addLevelEntry( m_ui->capLevel, s_courseNames[i], s_levelIdMap[i] );
	}
	addLevelEntry( m_ui->capLevel, "Bowser 1", SM64::LevelId::Bowser1 );
	addLevelEntry( m_ui->capLevel, "Bowser 2", SM64::LevelId::Bowser2 );
	addLevelEntry( m_ui->capLevel, "Bowser 3", SM64::LevelId::Bowser3 );

	for( int i = 0; i < m_ui->capLevel->count(); i++ ) {
		if( m_ui->capLevel->itemData( i, Qt::UserRole ).value<uint>() == (uint)saveSlot.capLevel ) {
			m_ui->capLevel->setCurrentIndex( i );
			break;
		}
	}
	m_ui->capArea->setValue( (int)saveSlot.capArea );

	for( QAbstractButton *radio : m_ui->flagButtons->buttons() ) {
		const SM64::SaveFileFlag flag = (SM64::SaveFileFlag)radio->property( "saveFlag" ).value<uint>();
		radio->setChecked( Flags::has( saveSlot.flags, flag ) );
	}

	for( int i = 0; i < 9; i++ ) {
		m_ui->gridLayout->addWidget( new QLabel( s_columnHeaders[i] ), 0, i+1, Qt::AlignCenter );
	}

	for( int i = 0; i < 26; i++ ) {
		m_ui->gridLayout->addWidget( new QLabel( s_courseNames[i] ), i+1, 0, Qt::AlignRight | Qt::AlignVCenter );
	}

	QPixmap coinIcon = QIcon( ":/sm64/coin.svg" ).pixmap( 32, 32 );
	for( ubyte i = 1; i < 26; i++ ) {
		for( ubyte j = 0; j < 8; j++ ) {
			if( i == 25 && j == 7 ) continue;

			StarIcon *icon = new StarIcon( m_ui->starGrid, i, j, saveSlot.starFlags[i-1] & (1 << j) );
			m_starIcons.push_back( icon );
			if( j == 7 ) {
				if( !s_vanillaCannons[i] ) {
					m_unusedStars.push_back( icon );
				}
			} else {
				const char *starName = s_starNames[i][j];
				if( starName == nullptr ) {
					m_unusedStars.push_back( icon );
				} else {
					icon->setToolTip( starName );
				}
			}

			m_ui->gridLayout->addWidget( icon, i, j+1 );
		}

		if( i <= 15 ) {
			QHBoxLayout *layout = new QHBoxLayout();
			QSpinBox *spinner = new QSpinBox();
			spinner->setMinimum( 0 );
			spinner->setMaximum( 255 );
			spinner->setValue( (int)saveSlot.coinScores[i-1] );
			QLabel *icon = new QLabel();
			icon->setPixmap( coinIcon );
			layout->addWidget( spinner, 1 );
			layout->addWidget( icon, 0 );
			m_coinInputs[i-1] = spinner;
			m_ui->gridLayout->addLayout( layout, i, 9 );
		}
	}

	for( ubyte j = 0; j < 7; j++ ) {
		StarIcon *icon = new StarIcon( m_ui->starGrid, 0, j, saveSlot.castleStars & (1 << j) );
		m_starIcons.push_back( icon );
		if( j < 5 ) {
			icon->setToolTip( s_starNames[0][j] );
		} else {
			m_unusedStars.push_back( icon );
		}

		m_ui->gridLayout->addWidget( icon, 26, j+1 );
	}

}

SaveSlotEditorDialog::~SaveSlotEditorDialog() {
	UiUtil::clearLayout( m_ui->starGrid->layout() );
	delete m_ui;
}

void SaveSlotEditorDialog::setShowUnused( bool showUnused ) {
	m_ui->unusedFlagsGroup->setVisible( showUnused );
	for( StarIcon *icon : m_unusedStars ) {
		icon->setVisible( showUnused );
	}
}

void SaveSlotEditorDialog::accept() {
	using namespace SM64;

	SM64::SaveFile saveFile;
	{
		std::ifstream inFile( m_filePath, std::ios_base::in | std::ios_base::binary );
		saveFile = SaveFile::read( inFile );
	}

	SaveSlot &slot = saveFile.slot( m_slot );

	slot.capLevel = (LevelId)m_ui->capLevel->currentData( Qt::UserRole ).value<uint>();
	slot.capArea = (ubyte)m_ui->capArea->value();

	slot.flags = SaveFileFlag::Exists;
	for( const QAbstractButton *radio : m_ui->flagButtons->buttons() ) {
		if( radio->isChecked() ) {
			slot.flags |= (SaveFileFlag)radio->property( "saveFlag" ).value<uint>();
		}
	}

	std::memset( slot.starFlags, 0, sizeof( slot.starFlags ) );
	slot.castleStars = 0;

	for( const StarIcon *icon : m_starIcons ) {
		if( !icon->isCollected() ) continue;

		if( icon->courseId() == 0 ) {
			slot.castleStars |= 1 << icon->starId();
		} else {
			slot.starFlags[icon->courseId() - 1] |= 1 << icon->starId();
		}
	}

	for( int i = 0; i < 15; i++ ) {
		slot.coinScores[i] = (ubyte)m_coinInputs[i]->value();
	}

	std::ofstream outFile( m_filePath, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc );
	saveFile.write( outFile );

	QDialog::accept();
}
