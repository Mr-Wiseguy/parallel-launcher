#ifndef SRC_UI_SINGLEPLAYER_CONTROLLER_SELECT_DIALOG_HPP_
#define SRC_UI_SINGLEPLAYER_CONTROLLER_SELECT_DIALOG_HPP_

#include <QDialog>
#include <QButtonGroup>
#include <vector>
#include "src/input/gamepad-controller.hpp"

namespace Ui {
	class SingleplayerControllerSelectDialog;
}

class SingleplayerControllerSelectDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::SingleplayerControllerSelectDialog *m_ui;
	QButtonGroup *const m_radioGroup;

	public:
	SingleplayerControllerSelectDialog( const std::vector<ConnectedGamepad> &controllers );
	virtual ~SingleplayerControllerSelectDialog();

	const ConnectedGamepad &getSelectedController() const;

};



#endif /* SRC_UI_SINGLEPLAYER_CONTROLLER_SELECT_DIALOG_HPP_ */
