#include "src/ui/keyboard-config-dialog.hpp"
#include "ui_keyboard-config-dialog.h"

#include <QFormLayout>
#include <QLabel>
#include <cassert>
#include "src/core/file-controller.hpp"
#include "src/core/hotkeys.hpp"
#include "src/ui/hotkey-edit-widget.hpp"
#include "src/ui/icons.hpp"

static constexpr size_t NUM_BINDS = (size_t)Hotkey::NUM_HOTKEYS;
static constexpr size_t NUM_CONTROLS = (size_t)ControllerAction::SaveState;
static constexpr size_t NUM_HOTKEYS = NUM_BINDS - NUM_CONTROLS;

static const char *const s_actionNames[] = {
	"Analog Up",
	"Analog Down",
	"Analog Left",
	"Analog Right",
	"C Up",
	"C Down",
	"C Left",
	"C Right",
	"D-Pad Up",
	"D-Pad Down",
	"D-Pad Left",
	"D-Pad Right",
	"A Button",
	"B Button",
	"L Trigger",
	"Z Trigger",
	"R Trigger",
	"Start Button",

	"Save State",
	"Load State",
	"Previous State",
	"Next State",
	"Previous Cheat",
	"Next Cheat",
	"Toggle Cheat",
	"Toggle FPS Display",
	"Pause/Unpause",
	"Frame Advance",
	"Fast Forward (Hold)",
	"Fast Forward (Toggle)",
	"Slow Motion (Hold)",
	"Slow Motion (Toggle)",
	"Rewind*",
	"Quit Emulator",
	"Hard Reset",
	"Toggle Fullscreen",
	"RetroArch Menu",
	"Record Video",
	"Record Inputs",
	"Take Screenshot",
	"Raise Volume",
	"Lower Volume",
	"Mute/Unmute"
};
static_assert( sizeof( s_actionNames ) == NUM_BINDS * sizeof(void*) );

KeyboardConfigDialog::KeyboardConfigDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::KeyboardConfigDialog )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );

	QFormLayout *controlsLayout = dynamic_cast<QFormLayout*>( m_ui->controlsArea->layout() );
	QFormLayout *hotkeysLayout = dynamic_cast<QFormLayout*>( m_ui->hotkeysArea->layout() );

	assert( controlsLayout != nullptr );
	for( size_t i = 0; i < NUM_CONTROLS; i++ ) {
		QLabel *label = new QLabel( s_actionNames[i] );
		label->setAlignment( Qt::AlignRight | Qt::AlignBottom );
		controlsLayout->addRow(	label, new HotkeyEditPanel( m_ui->controlsArea ) );
	}

	assert( hotkeysLayout != nullptr );
	for( size_t i = 0; i < NUM_HOTKEYS; i++ ) {
		QLabel *label = new QLabel( s_actionNames[i + NUM_CONTROLS] );
		label->setAlignment( Qt::AlignRight | Qt::AlignBottom );
		hotkeysLayout->addRow( label, new HotkeyEditPanel( m_ui->hotkeysArea ) );
	}

	QLayoutItem *rewindLabel = hotkeysLayout->itemAt( (int)((size_t)Hotkey::Rewind - NUM_CONTROLS), QFormLayout::LabelRole );
	assert( rewindLabel != nullptr );
	assert( rewindLabel->widget() != nullptr );

	rewindLabel->widget()->setToolTip( "You must enable rewind functionality in the RetroArch menu to use this hotkey." );
}

KeyboardConfigDialog::~KeyboardConfigDialog() {
	delete m_ui;
}

static inline HotkeyEditPanel *getPanel( QWidget *container, size_t row ) {
	QFormLayout *layout = dynamic_cast<QFormLayout*>( container->layout() );
	assert( layout != nullptr );
	QLayoutItem *field = layout->itemAt( (int)row, QFormLayout::FieldRole );
	assert( field != nullptr );
	HotkeyEditPanel *panel = dynamic_cast<HotkeyEditPanel*>( field->widget() );
	assert( panel != nullptr );
	return panel;
}

void KeyboardConfigDialog::showEvent( QShowEvent *event ) {
	const std::vector<int> &hotkeys = FileController::loadHotkeys();
	for( size_t i = 0; i < NUM_CONTROLS; i++ ) {
		getPanel( m_ui->controlsArea, i )->setValue( (Qt::Key)hotkeys[i] );
	}
	for( size_t i = 0; i < NUM_HOTKEYS; i++ ) {
		getPanel( m_ui->hotkeysArea, i )->setValue( (Qt::Key)hotkeys[NUM_CONTROLS + i] );
	}
	QDialog::showEvent( event );
}

void KeyboardConfigDialog::dialogButtonClicked( QAbstractButton *button ) {
	if( button == m_ui->buttonTray->button( QDialogButtonBox::Apply ) ) {
		std::vector<int> hotkeys;
		hotkeys.reserve( NUM_BINDS );

		for( size_t i = 0; i < NUM_CONTROLS; i++ ) {
			hotkeys.push_back( getPanel( m_ui->controlsArea, i )->value() );
		}

		for( size_t i = 0; i < NUM_HOTKEYS; i++ ) {
			hotkeys.push_back( getPanel( m_ui->hotkeysArea, i )->value() );
		}

		assert( hotkeys.size() == NUM_BINDS );
		FileController::saveHotkeys( hotkeys );
		QDialog::accept();
	} else if( button == m_ui->buttonTray->button( QDialogButtonBox::Discard ) ) {
		QDialog::reject();
	}
}
