#ifndef SRC_UI_MAIN_WINDOW_HPP_
#define SRC_UI_MAIN_WINDOW_HPP_

#include <QWidget>
#include <QMainWindow>
#include <memory>
#include "src/ui/rom-list-fetcher.hpp"
#include "src/ui/ui-state.hpp"
#include "src/core/settings.hpp"
#include "src/core/async.hpp"
#include "src/core/rom.hpp"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow {
	Q_OBJECT

	private:
	Ui::MainWindow *m_ui;
	bool m_showAllPlugins;
	AppSettings m_settings;
	RomListFetcher m_romListFetcher;

	TreeUiState saveTreeState() const;
	void restoreTreeState( const TreeUiState &state );
	void updateRom( const ROM &updatedRom );
	void launchEmulator( const std::vector<PlayerController> &players, bool bindSavestate );

	private slots:
	void reloadSettings();

	void refreshRomList();
	void romListFetched( FetchedRomList roms );

	void romSelectionChanged();
	void overclockCpuToggled();
	void overclockViToggled();
	void morePluginsToggled();
	void playSingleplayer();
	void playMultiplayer();
	void configureController();
	void configureKeyboard();
	void manageRomSources();
	void editSettings();
	void emulatorChanged();
	void parallelPluginChanged( int pluginId, bool checked );
	void mupenPluginChanged( int pluginId, bool checked );

	void addTag( const ROM *rom, string tag );
	void removeTag( const ROM *rom, string tag );
	void editSave( const ROM *rom, fs::path saveFilePath );

	public:
	MainWindow( const fs::path &romArg );
	virtual ~MainWindow();

	protected:
	virtual void closeEvent( QCloseEvent *event ) override;

};



#endif /* SRC_UI_MAIN_WINDOW_HPP_ */
