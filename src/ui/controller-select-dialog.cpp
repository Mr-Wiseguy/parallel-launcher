#include "src/ui/controller-select-dialog.hpp"
#include "ui_controller-select-dialog.h"

#include <cassert>
#include <QMessageBox>
#include "src/core/file-controller.hpp"
#include "src/core/preset-controllers.hpp"
#include "src/ui/controller-config-dialog.hpp"
#include "src/ui/icons.hpp"

ControllerSelectDialog::ControllerSelectDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ControllerSelectDialog ),
	m_disableEvents( true )
{
	m_ui->setupUi( this );
	m_profiles = FileController::loadControllerProfiles();
	m_profileMap = FileController::loadControllerMappings();

	setWindowIcon( Icon::appIcon() );
	m_ui->editButton->setIcon( Icon::edit() );
	m_ui->deleteButton->setIcon( Icon::delet() );

	for( const auto &i : m_profiles ) {
		m_ui->profileList->addItem( i.first.c_str() );
	}

	connect( &GamepadController::instance(), SIGNAL( gamepadConnected( GamepadConnectedEvent ) ), this, SLOT( controllerConnected( GamepadConnectedEvent ) ) );
	connect( &GamepadController::instance(), SIGNAL( gamepadDisconnected( GamepadDisconnectedEvent ) ), this, SLOT( controllerDisconnected( GamepadDisconnectedEvent ) ) );

	m_disableEvents = false;
	GamepadController::instance().start();
	refreshDeviceList();
}

ControllerSelectDialog::~ControllerSelectDialog() {
	GamepadController::instance().stop();
	delete m_ui;
}

void ControllerSelectDialog::refreshDeviceList() {
	m_disableEvents = true;

	GamepadId lastSelected = m_ui->deviceList->currentRow();
	int selectedRow = 0;

	m_ui->deviceList->clear();
	for( const auto &i : m_devices ) {
		QListWidgetItem *item = new QListWidgetItem;
		item->setData( Qt::UserRole, QVariant::fromValue<int>( i.first ) );

		if( !i.second.name.empty() ) {
			item->setText( i.second.name.c_str() );
		} else {
			item->setText( ("Unknown ("s + i.second.uuid.toString() + ')').c_str() );
		}

		m_ui->deviceList->addItem( item );
		if( i.first == lastSelected ) {
			selectedRow = m_ui->deviceList->count() - 1;
		}
	}

	if( !m_devices.empty() ) {
		m_ui->deviceList->setCurrentRow( selectedRow );
	}

	m_disableEvents = false;
	deviceSelected();
}

void ControllerSelectDialog::deviceSelected() {
	if( m_disableEvents ) return;

	if( m_devices.empty() || m_ui->deviceList->currentItem() == nullptr ) {
		m_ui->profileList->setEnabled( false );
		m_ui->editButton->setEnabled( false );
		m_ui->deleteButton->setEnabled( false );
		return;
	}

	m_ui->profileList->setEnabled( true );
	m_ui->editButton->setEnabled( true );

	GamepadId id = m_ui->deviceList->currentItem()->data( Qt::UserRole ).toInt();
	const ControllerInfo &info = m_devices.at( id );

	string activeProfile;
	if( m_profileMap.count( info.uuid ) ) {
		activeProfile = m_profileMap.at( info.uuid );
		if( m_profiles.count( activeProfile ) <= 0 ) {
			activeProfile.clear();
		}
	}

	if( activeProfile.empty() ) {
		switch( getControllerType( info.controllerId ) ) {
			case ControllerType::Gamecube:
				activeProfile = DefaultProfile::Gamecube.name;
				break;
			default:
				activeProfile = DefaultProfile::XBox360.name;
				break;
		}
	}

	m_ui->deleteButton->setEnabled( !DefaultProfile::exists( activeProfile ) );

	m_disableEvents = true;
	for( int i = 0; i < m_ui->profileList->count(); i++ ) {
		if( m_ui->profileList->item( i )->text().toStdString() == activeProfile ) {
			m_ui->profileList->setCurrentRow( i );
			break;
		}
	}
	m_disableEvents = false;
}

void ControllerSelectDialog::profileSelected() {
	if( m_disableEvents || m_ui->deviceList->currentItem() == nullptr || m_ui->profileList->currentItem() == nullptr ) return;

	const GamepadId id = m_ui->deviceList->currentItem()->data( Qt::UserRole ).toInt();
	if( m_devices.count( id ) <= 0 ) return;

	const Uuid &uuid = m_devices.at( id ).uuid;
	const string profile = m_ui->profileList->currentItem()->text().toStdString();
	m_profileMap[uuid] = profile;
	m_ui->deleteButton->setEnabled( !DefaultProfile::exists( profile ) );
	FileController::saveControllerMappings( m_profileMap );
}

void ControllerSelectDialog::editProfile() {
	const GamepadId deviceId = m_ui->deviceList->currentItem()->data( Qt::UserRole ).toInt();
	const ControllerInfo &info = m_devices.at( deviceId );

	ControllerConfigDialog dialog;
	dialog.setActiveController( info.name, info.uuid, deviceId, m_profiles.at( m_ui->profileList->currentItem()->text().toStdString() ) );
	dialog.exec();

	m_profiles = FileController::loadControllerProfiles();
	const string &activeProfile = dialog.getProfileName();
	int profileIndex = 0;
	int i = 0;

	m_disableEvents = true;
	m_ui->profileList->clear();
	for( const auto &p : m_profiles ) {
		m_ui->profileList->addItem( p.first.c_str() );
		if( p.first == activeProfile ) {
			profileIndex = i;
		}
		i++;
	}

	m_ui->profileList->setCurrentRow( profileIndex );
	m_disableEvents = false;
	profileSelected();
}

void ControllerSelectDialog::deleteProfile() {
	assert( m_ui->profileList->currentItem() != nullptr );

	const string profile = m_ui->profileList->currentItem()->text().toStdString();
	assert( !DefaultProfile::exists( profile ) );

	if( QMessageBox::question( nullptr, "Confirm Delete", ("Are you sure you want to delete controller profile '"s + profile + "'?").c_str() ) != QMessageBox::Yes ) {
		return;
	}

	m_profiles.erase( profile );
	FileController::saveControllerProfiles( m_profiles );
	delete m_ui->profileList->takeItem( m_ui->profileList->currentRow() );

	m_ui->profileList->setCurrentRow( 0 );
	profileSelected();
}

void ControllerSelectDialog::controllerConnected( GamepadConnectedEvent event ) {
	m_devices[event.id] = event.info;
	refreshDeviceList();
}

void ControllerSelectDialog::controllerDisconnected( GamepadDisconnectedEvent event ) {
	m_devices.erase( event.id );
	refreshDeviceList();
}
