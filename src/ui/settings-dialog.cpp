#include "src/ui/settings-dialog.hpp"
#include "ui_settings-dialog.h"

#include "src/ui/icons.hpp"
#include "src/core/file-controller.hpp"
#include "src/core/numeric-string.hpp"
#include "src/core/retroarch.hpp"
#include "src/ui/util.hpp"

#ifdef _WIN32
#include <QStyleFactory>
#endif

static const GfxPlugin m_mupenPlugins[] = {
	GfxPlugin::ParaLLEl,
	GfxPlugin::Angrylion,
	GfxPlugin::GlideN64
};

SettingsDialog::SettingsDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::SettingsDialog )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );

#ifdef _WIN32
	m_ui->useFlatpakCheckbox->setEnabled( false );
	m_ui->useFlatpakCheckbox->setChecked( false );
	m_ui->useFlatpakCheckbox->setToolTip( "This option is not available on your operating system" );
	m_ui->themeSelect->addItems( QStyleFactory::keys() );
#else
	m_ui->themePanel->setVisible( false );
	m_ui->configDirInput->setEnabled( false );
	connect( m_ui->useFlatpakCheckbox, &QAbstractButton::toggled, this, [&](bool usingFlatpak){
		m_ui->configDirInput->setText( RetroArch::configPath( usingFlatpak ).parent_path().c_str() );
	});
#endif
}

SettingsDialog::~SettingsDialog() {
	delete m_ui;
}

void SettingsDialog::showEvent( QShowEvent *event ) {
	const AppSettings settings = FileController::loadAppSettings();

	m_ui->windowScaleSelect->setCurrentIndex( (int)settings.windowScale - 1 );
	m_ui->upscalingSelect->setCurrentIndex( (int)settings.upscaling );
	m_ui->parallelPluginSelect->setCurrentIndex( (int)settings.defaultParallelPlugin - 1 );
	switch( settings.defaultMupenPlugin ) {
		case GfxPlugin::GlideN64: m_ui->mupenPluginSelect->setCurrentIndex( 2 ); break;
		case GfxPlugin::Angrylion: m_ui->mupenPluginSelect->setCurrentIndex( 1 ); break;
		default: m_ui->mupenPluginSelect->setCurrentIndex( 0 ); break;
	}
	m_ui->emulatorCoreSelect->setCurrentIndex( (int)settings.defaultEmulator - 1 );
	m_ui->vsyncCheckbox->setChecked( settings.vsync );
	m_ui->hideLauncherCheckbox->setChecked( settings.hideWhenPlaying );
	m_ui->showInternalNameCheckbox->setChecked( Flags::has( settings.visibleColumns, RomInfoColumn::InternalName ) );
	m_ui->showFilePathCheckbox->setChecked( Flags::has( settings.visibleColumns, RomInfoColumn::Path ) );
	m_ui->showLastPlayedCheckbox->setChecked( Flags::has( settings.visibleColumns, RomInfoColumn::LastPlayed ) );
	m_ui->showPlayTimeCheckbox->setChecked( Flags::has( settings.visibleColumns, RomInfoColumn::PlayTime ) );
	m_ui->showNotesCheckbox->setChecked( Flags::has( settings.visibleColumns, RomInfoColumn::Notes ) );
	m_ui->configBehaviourSelect->setCurrentIndex( (int)settings.configBehaviour );
#ifdef _WIN32
	m_ui->configDirInput->setText( settings.retroInstallDir.string().c_str() );
	m_ui->themeSelect->setCurrentText( settings.windowsTheme.c_str() );
#else
	m_ui->useFlatpakCheckbox->setChecked( settings.usingFlatpak );
	m_ui->configDirInput->setText( RetroArch::configPath( settings.usingFlatpak ).parent_path().c_str() );
#endif

	m_ui->showNotesCheckbox->setChecked( false );
	m_ui->showNotesCheckbox->setVisible( false );

	windowScaleChanged( (int)settings.windowScale - 1 );

	QDialog::showEvent( event );
	UiUtil::shrinkToFit( this, Qt::Vertical );
}

void SettingsDialog::closeEvent( QCloseEvent *event ) {
	save();
	QDialog::closeEvent( event );
}

void SettingsDialog::save() const {
	AppSettings settings = FileController::loadAppSettings();

	settings.visibleColumns = (RomInfoColumn)0;
	if( m_ui->showInternalNameCheckbox->isChecked() ) settings.visibleColumns |= RomInfoColumn::InternalName;
	if( m_ui->showFilePathCheckbox->isChecked() ) settings.visibleColumns |= RomInfoColumn::Path;
	if( m_ui->showLastPlayedCheckbox->isChecked() ) settings.visibleColumns |= RomInfoColumn::LastPlayed;
	if( m_ui->showPlayTimeCheckbox->isChecked() ) settings.visibleColumns |= RomInfoColumn::PlayTime;
	if( m_ui->showNotesCheckbox->isChecked() ) settings.visibleColumns |= RomInfoColumn::Notes;

#ifdef _WIN32
	settings.retroInstallDir = m_ui->configDirInput->text().toStdString();
	if( fs::is_regular_file( settings.retroInstallDir ) ) {
		settings.retroInstallDir = settings.retroInstallDir.parent_path();
	}
	settings.windowsTheme = m_ui->themeSelect->currentText().toStdString();
#else
	settings.usingFlatpak = m_ui->useFlatpakCheckbox->isChecked();
#endif

	settings.defaultParallelPlugin = (GfxPlugin)(m_ui->parallelPluginSelect->currentIndex() + 1);
	settings.defaultMupenPlugin = m_mupenPlugins[ m_ui->mupenPluginSelect->currentIndex() ];
	settings.defaultEmulator = (EmulatorCore)(m_ui->emulatorCoreSelect->currentIndex() + 1);
	settings.upscaling = (ParallelUpscaling)m_ui->upscalingSelect->currentIndex();
	settings.configBehaviour = (ConfigBehaviour)m_ui->configBehaviourSelect->currentIndex();
	settings.windowScale = (ubyte)(m_ui->windowScaleSelect->currentIndex() + 1);
	settings.vsync = m_ui->vsyncCheckbox->isChecked();
	settings.hideWhenPlaying = m_ui->hideLauncherCheckbox->isChecked();

	FileController::saveAppSettings( settings );
}

void SettingsDialog::windowScaleChanged( int index ) {
	int autoScale;
	switch( index ) {
		case 0: autoScale = 1; break;
		case 1: autoScale = 2; break;
		case 7: autoScale = 8; break;
		default: autoScale = 4; break;
	}

	const int autoWidth = 320 * autoScale;
	const int autoHeight = 240 * autoScale;

	const string autoText = "Auto (x"s + Number::toString( autoScale ) + " - " + Number::toString( autoWidth ) + 'x' + Number::toString( autoHeight ) + ')';
	m_ui->upscalingSelect->setItemText( 0, autoText.c_str() );
}
