#include "src/ui/rom-list-model.hpp"

#include <QFont>
#include <QDateTime>
#include <algorithm>
#include <cstdio>
#include <map>
#include "src/core/numeric-string.hpp"

typedef _RomListModelIndex Index;
typedef _RomListModelGroup Group;

static bool compareName( const ROM &a, const ROM &b ) {
	return a.name < b.name;
}

static bool compareInternalName( const ROM &a, const ROM &b ) {
	return a.internalName < b.internalName;
}

static bool comparePath( const ROM &a, const ROM &b ) {
	return a.path < b.path;
}

static bool compareLastPlayed( const ROM &a, const ROM &b ) {
	return a.lastPlayed < b.lastPlayed;
}

static bool comparePlayTime( const ROM &a, const ROM &b ) {
	return a.playTime < b.playTime;
}

static bool compareNotes( const ROM &a, const ROM &b ) {
	return a.notes < b.notes;
}

static bool (*s_romComparers[])(const ROM&,const ROM&) = {
	compareName,
	compareInternalName,
	comparePath,
	compareLastPlayed,
	comparePlayTime,
	compareNotes
};

static Group buildGroup(
	const string &groupName,
	int groupIndex,
	const std::vector<const ROM*> &roms,
	const std::map<const ROM*,size_t> &indexMap
) {
	std::vector<std::array<Index,6>> children;
	children.reserve( roms.size() );

	int i = 0;
	for( const ROM *rom : roms ) {
		std::array<Index,6> props;
		for( int j = 0; j < 6; j++ ) {
			props[j] = Index{
				/* group */ groupIndex,
				/* row */ i,
				/* col */ j,
				/* lookupIndex */ (int)indexMap.at( rom )
			};
		}
		children.push_back( std::move( props ) );
		i++;
	}

	return Group{
		/* name */ groupName,
		/* self */ Index{
			/* group */ groupIndex,
			/* row */ -1,
			/* col */ 0,
			/* lookupIndex */ -1
		},
		/* children */ std::move( children )
	};
}

void RomListModel::update( const std::vector<ROM> &roms ) {
	beginResetModel();

	m_roms.clear();
	m_groups.clear();

	m_roms.reserve( roms.size() );

	std::map<string,std::vector<const ROM*>> romGroups;
	std::vector<const ROM*> uncategorizedRoms;
	std::map<const ROM*,size_t> indexMap;

	for( const ROM &rom : roms ) {
		m_roms.push_back( rom );
		indexMap[&rom] = m_roms.size() - 1;

		if( rom.tags.empty() ) {
			uncategorizedRoms.push_back( &rom );
		} else for( const string &tag : rom.tags ) {
			romGroups[tag].push_back( &rom );
		}
	}

	m_groups.reserve( romGroups.size() + 1 );
	for( const auto &g : romGroups ) {
		m_groups.push_back( buildGroup( g.first, (int)m_groups.size(), g.second, indexMap ) );
	}

	if( !uncategorizedRoms.empty() ) {
		m_groups.push_back( buildGroup( "Uncategorized", (int)m_groups.size(), uncategorizedRoms, indexMap ) );
	}

	sortInternal();
	endResetModel();
}

static constexpr int64 SECONDS = 1000;
static constexpr int64 MINUTES = 60 * SECONDS;
static constexpr int64 HOURS = 60 * MINUTES;

static inline string formatTimeSpan( int64 timeSpan ) {
	const int64 hours = timeSpan / HOURS;
	const int64 minutes = (timeSpan % HOURS) / MINUTES;
	const int64 seconds = (timeSpan % MINUTES) / SECONDS;

	string timeString;
	timeString.reserve( 16 );

	if( hours > 0 ) {
		timeString += Number::toString( hours );
		timeString += "h ";
	}

	if( minutes > 0 || hours > 0 ) {
		timeString += Number::toString( minutes );
		timeString += "m ";
	}

	timeString += Number::toString( seconds );
	timeString += 's';

	return timeString;
}

static inline QString plainText( const string &str ) {
	return QString( str.c_str() ).toHtmlEscaped();
}

QVariant RomListModel::data( const QModelIndex &index, int role ) const {
	const Index *i = static_cast<const Index*>( index.internalPointer() );
	if( i == nullptr ) return QVariant();

	if( i->row < 0 ) {
		if( role == Qt::DisplayRole ) {
			string label = plainText( m_groups.at( i->group ).name ).toStdString();
			label += " (";
			label += Number::toString( (int)m_groups.at( i->group ).children.size() );
			label += ')';
			return QVariant::fromValue<QString>( label.c_str() );
		}
		return QVariant();
	}

	if( role != Qt::DisplayRole && ( role != Qt::EditRole || index.column() != COL_NOTES ) ) {
		return QVariant();
	}

	const ROM &romInfo = m_roms.at( i->lookupIndex );
	switch( i->col ) {
		case COL_NAME:
			return QVariant::fromValue<QString>( plainText( romInfo.name ) );
		case COL_INTERNAL_NAME:
			return QVariant::fromValue<QString>( plainText( romInfo.internalName ) );
		case COL_PATH:
			return QVariant::fromValue<QString>( plainText( romInfo.path.string() ) );
		case COL_LAST_PLAYED:
			if( romInfo.lastPlayed == 0 ) {
				return QVariant::fromValue<QString>( "" );
			}
			return QVariant::fromValue<QString>( QDateTime::fromMSecsSinceEpoch( romInfo.lastPlayed ).toString( "yyyy-MM-dd h:mm AP" ) );
		case COL_PLAY_TIME:
			return QVariant::fromValue<QString>( formatTimeSpan( romInfo.playTime ).c_str() );
		case COL_NOTES:
			return QVariant::fromValue<QString>( plainText( romInfo.notes ) );
		default:
			return QVariant();
	}
}

Qt::ItemFlags RomListModel::flags( const QModelIndex &index ) const {
	const Index *i = static_cast<const Index*>( index.internalPointer() );
	if( i == nullptr ) return Qt::NoItemFlags;

	if( i->row < 0 ) {
		if( i->col != 0 ) return Qt::NoItemFlags;
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	}

	Qt::ItemFlags _flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
	if( i->col == COL_NOTES ) {
		_flags |= Qt::ItemIsEditable;
	}

	return _flags;
}

QVariant RomListModel::headerData( int section, Qt::Orientation orientation, int role ) const {
	if( orientation == Qt::Vertical ) return QVariant();
	if( role != Qt::DisplayRole ) return QAbstractItemModel::headerData( section, orientation, role );
	switch( section ) {
		case COL_NAME: return QVariant::fromValue<QString>( "Name" );
		case COL_INTERNAL_NAME: return QVariant::fromValue<QString>( "Internal Name" );
		case COL_PATH: return QVariant::fromValue<QString>( "File Path" );
		case COL_LAST_PLAYED: return QVariant::fromValue<QString>( "Last Played" );
		case COL_PLAY_TIME: return QVariant::fromValue<QString>( "Play Time" );
		case COL_NOTES: return QVariant::fromValue<QString>( "Notes" );
		default: return QVariant();
	}
}

QModelIndex RomListModel::index( int row, int column, const QModelIndex &parent ) const {
	if( m_groups.empty() ) return QModelIndex();
	if( parent == QModelIndex() ) {
		if( column != 0 ) return QModelIndex();
		return createIndex( row, column, (void*)&m_groups.at( row ).self );
	}
	const Index *i = static_cast<const Index*>( parent.internalPointer() );
	if( i == nullptr || i->row >= 0 ) return QModelIndex();
	return createIndex( row, column, (void*)&m_groups.at( i->group ).children.at( row ).at( column ) );
}

QModelIndex RomListModel::parent( const QModelIndex &index ) const {
	if( m_groups.empty() ) return QModelIndex();
	const Index *i = static_cast<const Index*>( index.internalPointer() );
	if( i == nullptr || i->row < 0 ) return QModelIndex();
	const Index *parentIndex = &m_groups.at( i->group ).self;
	return createIndex( parentIndex->group, 0, (void*)parentIndex );
}

bool RomListModel::hasChildren( const QModelIndex &parent ) const {
	if( !parent.isValid() ) {
		return !m_groups.empty();
	}

	const Index *i = static_cast<const Index*>( parent.internalPointer() );
	return i != nullptr && i->row < 0;
}

int RomListModel::rowCount( const QModelIndex &parent ) const {
	if( !parent.isValid() ) {
		return (int)m_groups.size();
	}

	const Index *i = static_cast<const Index*>( parent.internalPointer() );
	if( i == nullptr || i->row >= 0 ) return 0;
	return (int)m_groups.at( i->group ).children.size();
}

void RomListModel::sortInternal() {
	typedef std::array<_RomListModelIndex,6> Leaf;
	for( Group &group : m_groups ) {
		std::stable_sort( group.children.begin(), group.children.end(), [&](const Leaf &a, const Leaf &b){
			const ROM &romA = m_roms.at( a[0].lookupIndex );
			const ROM &romB = m_roms.at( b[0].lookupIndex );
			const bool reverse = (m_sortOrder == Qt::DescendingOrder);
			return s_romComparers[m_sortBy]( reverse ? romB : romA, reverse ? romA : romB );
		});

		int i = 0;
		for( std::array<Index,6> &child : group.children ) {
			for( Index &index : child ) {
				index.row = i;
			}
			i++;
		}
	}
}

void RomListModel::sort( int column, Qt::SortOrder order ) {
	m_sortBy = column;
	m_sortOrder = order;
	beginResetModel();
	sortInternal();
	endResetModel();
}

string RomListModel::tryGetGroup( const QModelIndex &index ) const {
	const Index *i = static_cast<const Index*>( index.internalPointer() );
	return (i == nullptr) ? string() : m_groups.at( i->group ).name;
}

const ROM *RomListModel::tryGetRomInfo( const QModelIndex &index ) const {
	const Index *i = static_cast<const Index*>( index.internalPointer() );
	if( i == nullptr || i->lookupIndex < 0 ) return nullptr;
	return &m_roms.at( i->lookupIndex );
}

QModelIndex RomListModel::tryGetIndex( const string &group, const fs::path &rom ) const {
	for( const Group &g : m_groups ) {
		if( g.name != group ) continue;
		for( const std::array<Index,6> &i : g.children ) {
			if( m_roms.at( i[0].lookupIndex ).path == rom ) {
				return createIndex( i[0].row, 0, (void*)&i );
			}
		}
	}

	return QModelIndex();
}

QModelIndex RomListModel::tryGetIndex( const string &group ) const {
	for( const Group &g : m_groups ) {
		if( g.name == group ) {
			return createIndex( g.self.group, 0, (void*)&g.self );
		}
	}

	return QModelIndex();
}
