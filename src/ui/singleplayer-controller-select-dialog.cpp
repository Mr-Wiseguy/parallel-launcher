#include "src/ui/singleplayer-controller-select-dialog.hpp"
#include "ui_singleplayer-controller-select-dialog.h"

#include "src/ui/util.hpp"
#include "src/ui/icons.hpp"
#include <QRadioButton>

SingleplayerControllerSelectDialog::SingleplayerControllerSelectDialog( const std::vector<ConnectedGamepad> &controllers ) :
	QDialog( nullptr ),
	m_ui( new Ui::SingleplayerControllerSelectDialog ),
	m_radioGroup( new QButtonGroup( this ) )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );

	for( const ConnectedGamepad &controller : controllers ) {
		QRadioButton *radio = new QRadioButton( this );
		if( controller.info.name.empty() ) {
			radio->setText( ("Unknown Controller ("s + controller.info.uuid.toString() + ')').c_str() );
		} else {
			radio->setText( controller.info.name.c_str() );
		}
		radio->setProperty( "gamepad", QVariant::fromValue( (void*)&controller ) );
		m_ui->controllerLayout->addWidget( radio );
		m_radioGroup->addButton( radio );
	}

	m_radioGroup->buttons().first()->setChecked( true );
}

SingleplayerControllerSelectDialog::~SingleplayerControllerSelectDialog() {
	UiUtil::clearLayout( m_ui->controllerLayout );
	delete m_ui;
}

const ConnectedGamepad &SingleplayerControllerSelectDialog::getSelectedController() const {
	for( const QAbstractButton *radio : m_radioGroup->buttons() ) {
		if( radio->isChecked() ) {
			return *static_cast<const ConnectedGamepad*>( radio->property( "gamepad" ).value<void*>() );
		}
	}
	return *static_cast<const ConnectedGamepad*>( m_radioGroup->buttons().first()->property( "gamepad" ).value<void*>() );
}
