#ifndef SRC_UI_UTIL_HPP_
#define SRC_UI_UTIL_HPP_

#include <QLayout>
#include <QDialog>

namespace UiUtil {

	extern void clearLayout( QLayout *layout );
	extern void shrinkToFit( QDialog *dialog, Qt::Orientation shrinkDirection );

}



#endif /* SRC_UI_UTIL_HPP_ */
