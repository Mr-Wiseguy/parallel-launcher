#include "src/ui/ui-fixes.hpp"

#include "src/ui/tooltip-hack.hpp"
#include "src/ui/icons.hpp"

#ifdef _WIN32
#include <QLibrary>
#include <QPluginLoader>
#include <QTextStream>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QAbstractButton>
#include <QIcon>
#include <QFont>
#include <QPalette>
#include <QColor>
#include <iostream>
#include "src/core/file-controller.hpp"
#include "src/polyfill/base-directory.hpp"

static constexpr QMessageBox::StandardButtons s_positiveButtons =
	QMessageBox::Ok | QMessageBox::Apply | QMessageBox::Yes | QMessageBox::YesToAll;

static constexpr QMessageBox::StandardButtons s_negativeButtons =
	QMessageBox::Cancel | QMessageBox::Abort | QMessageBox::No | QMessageBox::NoToAll | QMessageBox::Close;

bool WindowsIconFixer::eventFilter( QObject *object, QEvent *event ) {
	if( event->type() != QEvent::Show ) {
		return QObject::eventFilter( object, event );
	}

	QMessageBox *dialog = dynamic_cast<QMessageBox*>( object );
	if( dialog != nullptr ) {
		for( QAbstractButton *dialogButton : dialog->buttons() ) {
			const QMessageBox::StandardButtons buttonType = dialog->standardButton( dialogButton );
			if( buttonType & s_positiveButtons ) {
				dialogButton->setIcon( Icon::ok() );
			} else if( buttonType & s_negativeButtons ) {
				dialogButton->setIcon( Icon::cancel() );
			} else if( buttonType & (QMessageBox::Save | QMessageBox::SaveAll) ) {
				dialogButton->setIcon( Icon::save() );
			} else if( buttonType & QMessageBox::Open ) {
				dialogButton->setIcon( Icon::browse() );
			}
		}
	}

	QDialogButtonBox *buttonTray = dynamic_cast<QDialogButtonBox*>( object );
	if( buttonTray != nullptr ) {
		for( QAbstractButton *dialogButton : buttonTray->buttons() ) {
			const QDialogButtonBox::StandardButton buttonType = buttonTray->standardButton( dialogButton );
			switch( buttonType ) {
				case QDialogButtonBox::StandardButton::Ok:
				case QDialogButtonBox::StandardButton::Apply:
				case QDialogButtonBox::StandardButton::Yes:
				case QDialogButtonBox::StandardButton::YesToAll:
					dialogButton->setIcon( Icon::ok() ); break;
				case QDialogButtonBox::StandardButton::Open:
					dialogButton->setIcon( Icon::browse() ); break;
				case QDialogButtonBox::StandardButton::Save:
				case QDialogButtonBox::StandardButton::SaveAll:
					dialogButton->setIcon( Icon::save() ); break;
				case QDialogButtonBox::StandardButton::Cancel:
					dialogButton->setIcon( Icon::cancel() ); break;
				case QDialogButtonBox::StandardButton::Close:
				case QDialogButtonBox::StandardButton::No:
				case QDialogButtonBox::StandardButton::NoToAll:
				case QDialogButtonBox::StandardButton::Abort:
					dialogButton->setIcon( Icon::close() ); break;
				case QDialogButtonBox::StandardButton::Reset:
				case QDialogButtonBox::StandardButton::Retry:
					dialogButton->setIcon( Icon::refresh() ); break;
				default: break;
			}
		}
	}

	return QObject::eventFilter( object, event );
};

static const char *s_cssTweaks = R"THEME_DEF(

QPushButton, QToolButton {
	padding: 7px;
}

QComboBox {
	padding: 5px;
}

QSpinBox, QDoubleSpinBox {
	padding: 4px 2px;
}

QLineEdit {
	padding: 4px;
}

QListWidget::item, QTreeView::item {
	padding: 4px 1px;
}

QMessageBox QAbstractButton, QDialogButtonBox QAbstractButton {
	min-width: 66px;
}

QGroupBox::title {
	subcontrol-position: top center;
}

QPushButton {
	qproperty-iconSize: 16px 16px;
}

QMainWindow#MainWindow QPushButton#refreshButton,
QMainWindow#MainWindow QPushButton#controllerConfigButton,
QMainWindow#MainWindow QToolButton#menuButton {
	qproperty-iconSize: 24px 24px;
}

)THEME_DEF";

#define PALETTE_DEF1( role, colour ) \
	palette.setColor( QPalette::role, colour );
#define PALETTE_DEF2( role, colour1, colour2 ) \
	palette.setColor( QPalette::role, colour1 ); \
	palette.setColor( QPalette::Disabled, QPalette::role, colour2 );
#define PALETTE_DEF3( role, colour1, colour2, colour3 ) \
	palette.setColor( QPalette::Active, QPalette::role, colour1 ); \
	palette.setColor( QPalette::Inactive, QPalette::role, colour2 ); \
	palette.setColor( QPalette::Disabled, QPalette::role, colour3 );

static inline void applyWindowsFixes( QApplication &app ) {
	fs::directory_iterator pluginDir( BaseDir::program() / "styles" );
	for( const auto &plugin : pluginDir ) {
		const QString pluginPath = plugin.path().string().c_str();
		if( !QLibrary::isLibrary( pluginPath ) ) continue;

		QPluginLoader *pluginLoader = new QPluginLoader( pluginPath, &app );
		if( !pluginLoader->load() ) {
			std::cerr << "Failed to load style plugin " << plugin.path().filename().string() << std::endl << std::flush;
			pluginLoader->deleteLater();
		}
	}

	app.setStyle( FileController::loadAppSettings().windowsTheme.c_str() );
	app.setStyleSheet( s_cssTweaks );
	QIcon::setThemeName( "breeze-fallback" );

	QFont defaultFont = app.font();
	if( defaultFont.pointSize() < 10 ) {
		defaultFont.setPointSize( 10 );
		app.setFont( defaultFont );
	}

	const QColor backgroundColour = QColor::fromRgb( 239, 240, 241 );
	const QColor textColour = QColor::fromRgb( 35, 38, 39 );
	const QColor disabledText = QColor::fromRgb( 160, 162, 162 );
	const QColor disabledBackground = QColor::fromRgb( 227, 229, 231 );
	const QColor white = QColor::fromRgb( 255, 255, 255 );
	const QColor offwhite = QColor::fromRgb( 252, 252, 252 );

	QPalette palette = app.palette();
	PALETTE_DEF2( WindowText, textColour, disabledText )
	PALETTE_DEF2( Button, backgroundColour, disabledBackground )
	PALETTE_DEF1( Light, white )
	PALETTE_DEF2( Midlight, QColor::fromRgb( 247, 247, 248 ), QColor::fromRgb( 236, 237, 238 ) )
	PALETTE_DEF2( Dark, QColor::fromRgb( 136, 142, 147 ), QColor::fromRgb( 136, 142, 147 ) )
	PALETTE_DEF2( Mid, QColor::fromRgb( 196, 201, 205 ), QColor::fromRgb( 188, 192, 197 ) )
	PALETTE_DEF2( Text, textColour, QColor::fromRgb( 168, 169, 269 ) )
	PALETTE_DEF1( BrightText, white )
	PALETTE_DEF2( ButtonText, textColour, disabledText )
	PALETTE_DEF2( Base, offwhite, QColor::fromRgb( 241, 241, 241 ) )
	PALETTE_DEF2( Window, backgroundColour, disabledBackground )
	PALETTE_DEF1( Shadow, QColor::fromRgb( 71, 74, 76 ) )
	PALETTE_DEF3( Highlight, QColor::fromRgb( 61, 174, 233 ), QColor::fromRgb( 194, 224, 245 ), disabledBackground )
	PALETTE_DEF3( HighlightedText, offwhite, textColour, disabledText )
	PALETTE_DEF2( Link, QColor::fromRgb( 41, 128, 185 ), QColor::fromRgb( 162, 200, 224 ) )
	PALETTE_DEF2( LinkVisited, QColor::fromRgb( 127, 140, 141 ), QColor::fromRgb( 199, 203, 203 ) )
	PALETTE_DEF2( AlternateBase, backgroundColour, disabledBackground )
	app.setPalette( palette );

	app.installEventFilter( new WindowsIconFixer( &app ) );
}

#undef PALETTE_DEF1
#undef PALETTE_DEF2
#undef PALETTE_DEF3

#else
static inline void applyWindowsFixes( [[maybe_unused]] const QApplication &app ) {}
#endif

void applyUiFixes( QApplication &app ) {
	app.setWindowIcon( Icon::appIcon() );
	app.installEventFilter( new TooltipHack( &app ) );
	applyWindowsFixes( app );
}
