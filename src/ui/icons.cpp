#include "icons.hpp"

#define LOAD_ICON( varName, iconName ) const QIcon &Icon::varName() { \
	static const QIcon s_icon = QIcon::fromTheme( iconName, QIcon( ":/fallback-icons/" iconName ".svg" ) ); \
	return s_icon; \
}

const QIcon &Icon::appIcon() {
	static const QIcon s_icon = QIcon( ":/appicon.svg" );
	return s_icon;
}

LOAD_ICON( menu, "application-menu" )
LOAD_ICON( delet, "delete" )
LOAD_ICON( cancel, "dialog-cancel" )
LOAD_ICON( close, "dialog-close" )
LOAD_ICON( dialogError, "dialog-error" )
LOAD_ICON( dialogInfo, "dialog-information" )
LOAD_ICON( ok, "dialog-ok" )
LOAD_ICON( dialogWarning, "dialog-warning" )
LOAD_ICON( browse, "document-open" )
LOAD_ICON( save, "document-save" )
LOAD_ICON( saveAs, "document-save-as" )
LOAD_ICON( edit, "document-edit" )
LOAD_ICON( gamepad, "input-gamepad" )
LOAD_ICON( add, "list-add" )
LOAD_ICON( play, "media-playback-start" )
LOAD_ICON( fastForward, "media-seek-forward" )
LOAD_ICON( pkill, "process-stop" )
LOAD_ICON( refresh, "view-refresh" )
LOAD_ICON( skip, "go-next-skip" )
LOAD_ICON( group, "group" )
LOAD_ICON( configure, "configure" )
LOAD_ICON( clear, "edit-clear" )
LOAD_ICON( search, "edit-find" )
LOAD_ICON( keyboard, "input-keyboard" )

#undef LOAD_ICON
