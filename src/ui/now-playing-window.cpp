#include "src/ui/now-playing-window.hpp"
#include "ui_now-playing-window.h"

#include <chrono>
#include <cstdio>
#include "src/core/numeric-string.hpp"
#include "src/ui/icons.hpp"

static inline int64 nowMs() {
	return std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::steady_clock::now().time_since_epoch()
	).count();
}

NowPlayingWindow::NowPlayingWindow(
	AsyncProcess *process,
	const string &romName,
	int64 pastPlayTime
) :
	QMainWindow( nullptr ),
	m_ui( new Ui::NowPlayingWindow ),
	m_process( process ),
	m_timer( this ),
	m_pastPlayTime( pastPlayTime ),
	m_startTime( nowMs() )
{
	m_ui->setupUi( this );

	m_ui->romNameLabel->setText( romName.c_str() );

	connect( &m_timer, SIGNAL(timeout()), this, SLOT(updateTimers()) );
	m_timer.start( 1000 );

	setWindowIcon( Icon::appIcon() );
	m_ui->killButton->setIcon( Icon::pkill() );

	updateTimers();
}

NowPlayingWindow::~NowPlayingWindow() {
	delete m_ui;
}

int64 NowPlayingWindow::getSessionTime() const {
	return nowMs() - m_startTime;
}

static string formatTime( int64 ms ) {
	static constexpr int64 MINUTE = 60;
	static constexpr int64 HOUR = MINUTE * 60;

	const int64 totalSeconds = (ms + 500ll) / 1000ll;

	const int64 seconds = totalSeconds % MINUTE;
	const int64 minutes = (totalSeconds % HOUR) / MINUTE;
	const int64 hours = totalSeconds / HOUR;

	if( hours > 0 ) {
		char mss[9];
		std::sprintf( mss, ":%02lld:%02lld", minutes, seconds );
		return Number::toString( hours ) + mss;
	} else {
		char mss[8];
		std::sprintf( mss, "%lld:%02lld", minutes, seconds );
		return string( mss );
	}
}

void NowPlayingWindow::killEmulator() {
	m_process->kill();
	close();
}

void NowPlayingWindow::updateTimers() {
	const int64 sessionTime = getSessionTime();
	m_ui->sessionTimeLabel->setText( formatTime( sessionTime ).c_str() );
	m_ui->totalTimeLabel->setText( formatTime( sessionTime + m_pastPlayTime ).c_str() );
}
