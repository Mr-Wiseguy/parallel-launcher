#ifndef SRC_UI_ICONS_HPP_
#define SRC_UI_ICONS_HPP_

#include <QIcon>

namespace Icon {
	extern const QIcon &appIcon();
	extern const QIcon &menu();
	extern const QIcon &delet();
	extern const QIcon &cancel();
	extern const QIcon &close();
	extern const QIcon &dialogError();
	extern const QIcon &dialogInfo();
	extern const QIcon &ok();
	extern const QIcon &dialogWarning();
	extern const QIcon &browse();
	extern const QIcon &save();
	extern const QIcon &saveAs();
	extern const QIcon &edit();
	extern const QIcon &gamepad();
	extern const QIcon &add();
	extern const QIcon &play();
	extern const QIcon &fastForward();
	extern const QIcon &pkill();
	extern const QIcon &refresh();
	extern const QIcon &skip();
	extern const QIcon &group();
	extern const QIcon &configure();
	extern const QIcon &clear();
	extern const QIcon &search();
	extern const QIcon &keyboard();
}



#endif /* SRC_UI_ICONS_HPP_ */
