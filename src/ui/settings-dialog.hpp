#ifndef SRC_UI_SETTINGS_DIALOG_HPP_
#define SRC_UI_SETTINGS_DIALOG_HPP_

#include <QDialog>

namespace Ui {
	class SettingsDialog;
}

class SettingsDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::SettingsDialog *m_ui;

	protected:
	virtual void showEvent( QShowEvent *event ) override;
	virtual void closeEvent( QCloseEvent *event ) override;

	private slots:
	void save() const;
	void windowScaleChanged( int index );

	public:
	SettingsDialog( QWidget *parent = nullptr );
	~SettingsDialog();

};

#endif /* SRC_UI_SETTINGS_DIALOG_HPP_ */
