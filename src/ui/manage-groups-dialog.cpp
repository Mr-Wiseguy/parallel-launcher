#include "src/ui/manage-groups-dialog.hpp"
#include "ui_manage-groups-dialog.h"

#include <QInputDialog>
#include <QMessageBox>
#include "src/core/file-controller.hpp"
#include "src/core/numeric-string.hpp"
#include "src/ui/icons.hpp"

ManageGroupsDialog::ManageGroupsDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ManageGroupsDialog )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );

	m_ui->addButton->setIcon( Icon::add() );
	m_ui->renameButton->setIcon( Icon::edit() );
	m_ui->deleteButton->setIcon( Icon::delet() );
}

ManageGroupsDialog::~ManageGroupsDialog() {
	delete m_ui;
}

static QString formatGroup( const string &groupName, int romCount ) {
	return QString(( groupName + " (" + Number::toString( romCount ) + ')').c_str());
}

void ManageGroupsDialog::updateButtons() {
	const bool groupSelected = (m_ui->groupsList->count() > 0 || m_ui->groupsList->currentRow() >= 0);
	m_ui->deleteButton->setEnabled( groupSelected );
	m_ui->renameButton->setEnabled( groupSelected );
}

void ManageGroupsDialog::deleteGroup() {
	QListWidgetItem *item = m_ui->groupsList->currentItem();
	_MGD_GroupInfo itemData = item->data( Qt::UserRole ).value<_MGD_GroupInfo>();

	if( itemData.romCount > 0 ) {
		if( QMessageBox::question( this, "Confirm Delete", "Are you sure you want to delete this group?" ) != QMessageBox::Yes ) {
			return;
		}
	}

	m_groups.erase( itemData.name );
	m_ui->groupsList->removeItemWidget( item );
	delete item;

	for( ROM rom : m_roms ) {
		rom.tags.erase( itemData.name );
	}
}

void ManageGroupsDialog::renameGroup() {
	const QString qName = QInputDialog::getText( this, "Rename Group", "Enter a new name for your group" );
	if( qName.isEmpty() || qName.isNull() ) return;

	QListWidgetItem *item = m_ui->groupsList->currentItem();
	_MGD_GroupInfo itemData = item->data( Qt::UserRole ).value<_MGD_GroupInfo>();

	const string &oldName = itemData.name;
	const string newName = qName.toStdString();

	if( m_groups.count( newName ) > 0 ) {
		QMessageBox::critical( this, "Rename Failed", "A group with this name already exists." );
		return;
	}

	itemData.name = newName;
	item->setData( Qt::UserRole, QVariant::fromValue<_MGD_GroupInfo>( itemData ) );
	item->setText( formatGroup( newName, itemData.romCount ) );

	m_groups.erase( oldName );
	m_groups.insert( newName );
	for( ROM rom : m_roms ) {
		if( rom.tags.erase( oldName ) > 0 ) {
			rom.tags.insert( newName );
		}
	}
}

void ManageGroupsDialog::addGroup() {
	const QString qName = QInputDialog::getText( this, "Create Group", "Enter a name for your new group" );
	if( qName.isEmpty() || qName.isNull() ) return;

	const string group = qName.toStdString();
	if( m_groups.count( group ) > 0 ) {
		QMessageBox::critical( this, "Create Failed", "A group with this name already exists." );
		return;
	}

	m_groups.insert( group );

	QListWidgetItem *item = new QListWidgetItem( formatGroup( group, 0 ) );
	item->setData( Qt::UserRole, QVariant::fromValue<_MGD_GroupInfo>({ group, 0 }) );
	m_ui->groupsList->addItem( item );
	m_ui->groupsList->setCurrentItem( item );
}

void ManageGroupsDialog::showEvent( QShowEvent *event ) {
	m_groups = FileController::loadTags();
	m_roms = FileController::loadRomList();

	HashMap<string,int> groupSizes;
	for( const ROM &rom : m_roms ) {
		for( const string &group : rom.tags ) {
			groupSizes[group]++;
		}
	}

	m_ui->groupsList->clear();
	for( const string &group : m_groups ) {
		QListWidgetItem *item = new QListWidgetItem( formatGroup( group, groupSizes[group] ) );
		item->setData( Qt::UserRole, QVariant::fromValue<_MGD_GroupInfo>({ group, groupSizes[group] }) );
		m_ui->groupsList->addItem( item );
	}

	if( !m_groups.empty() ) {
		m_ui->groupsList->setCurrentRow( 0 );
	}

	updateButtons();
	QDialog::showEvent( event );
}

void ManageGroupsDialog::closeEvent( QCloseEvent *event ) {
	save();
	QDialog::closeEvent( event );
}

void ManageGroupsDialog::save() const {
	FileController::saveTags( m_groups );
	FileController::saveRomList( m_roms );
}
