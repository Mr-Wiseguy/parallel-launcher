#ifndef SRC_UI_ROM_LIST_MODEL_HPP_
#define SRC_UI_ROM_LIST_MODEL_HPP_

#include <QAbstractItemModel>
#include <QSize>
#include <vector>
#include <array>
#include "src/core/rom.hpp"

struct _RomListModelIndex {
	int group;
	int row;
	int col;
	int lookupIndex;
};

struct _RomListModelGroup {
	string name;
	_RomListModelIndex self;
	std::vector<std::array<_RomListModelIndex,6>> children;
};

class RomListModel : public QAbstractItemModel {
	Q_OBJECT

	private:
	std::vector<ROM> m_roms;
	std::vector<_RomListModelGroup> m_groups;
	int m_sortBy;
	Qt::SortOrder m_sortOrder;

	static constexpr int COL_NAME = 0;
	static constexpr int COL_INTERNAL_NAME = 1;
	static constexpr int COL_PATH = 2;
	static constexpr int COL_LAST_PLAYED = 3;
	static constexpr int COL_PLAY_TIME = 4;
	static constexpr int COL_NOTES = 5;

	void sortInternal();

	public:
	RomListModel( QObject *parent ) :
		QAbstractItemModel( parent ),
		m_sortBy( COL_NAME ),
		m_sortOrder( Qt::SortOrder::AscendingOrder )
	{}

	virtual ~RomListModel() {}

	void update( const std::vector<ROM> &roms );
	string tryGetGroup( const QModelIndex &index ) const;
	const ROM *tryGetRomInfo( const QModelIndex &index ) const;
	QModelIndex tryGetIndex( const string &group, const fs::path &rom ) const;
	QModelIndex tryGetIndex( const string &group ) const;

	virtual int columnCount( [[maybe_unused]] const QModelIndex &parent ) const override { return 6; }
	virtual QSize span( const QModelIndex &index ) const override {
		return QSize( hasChildren( index ) ? 6 : 0, 1 );
	}

	virtual QVariant data( const QModelIndex &index, int role ) const override;
	virtual Qt::ItemFlags flags( const QModelIndex &index ) const override;
	virtual QVariant headerData( int section, Qt::Orientation orientation, int role ) const override;
	virtual QModelIndex index( int row, int column, const QModelIndex &parent ) const override;
	virtual QModelIndex parent( const QModelIndex &index ) const override;
	virtual bool hasChildren( const QModelIndex &parent ) const override;
	virtual int rowCount( const QModelIndex &parent ) const override;
	virtual void sort( int column, Qt::SortOrder order ) override;

};



#endif /* SRC_UI_ROM_LIST_MODEL_HPP_ */
