# Parallel Launcher

An open source launcher for the ParallelN64 and Mupen64Plus-Next emulators featuring very easy controller configuration, automatic loading of ROMs from given folders, and automatic setup of optimal settings.

Currently, the most widely used N64 emulator is Project64. While it is the most popular emulator, it suffers from inferior performance and accuracy compared to other emulators, and most graphics plugin developers have dropped support for Project64 in favour of supporting more modern emulators, leaving Project64 with very out of date plugins. Most notably, it does not support the ParaLLEl graphics plugin at all, which is the first graphics plugin that is able to correctly emulate N64 graphics with near perfect accuracy in real time.

The ParallelN64 and Mupen64Plus-Next emulators, on the other hand, are fast, accurate, open source, and run on every operating system that matters. There's just one problem: as libretro cores, they must be run through RetroArch, which is infamous for it's confusing user interface, especially its controller setup interface. As a result, many people give up on trying to configure it and simply use Project 64 instead, which is much easier to setup.

Parallel Launcher aims to give you the best of both worlds by providing a very simple interface that takes care of configuring RetroArch for you, allowing you to have the accuracy and performance of ParallelN64/Mupen64Plus-Next with the ease of use of Project64.

## Building Parallel Launcher from Source

### Building on Linux

- Install the dev dependencies.
  - Debian/Ubuntu/Mint: `sudo apt install build-essential qt5-qmake qtdeclarative5-dev libsdl2-dev`
  - OpenSUSE: `sudo zypper install gcc9 gcc9-c++ libqt5-qtbase-common-devel libqt5-qtdeclarative-devel libSDL2-devel findutils`
  - Fedora/CentOS: `sudo yum install gcc9 gcc9-c++ libqt5-qtbase-common-devel libqt5-qtdeclarative-devel libSDL2-devel findutils`
  - Arch/Manjaro: `sudo pacman -S gcc qt5-declarative make sdl2 findutils`
- Build the makefile with `./qmake-release.sh` (or `./qmake-debug.sh` for the debug build)
- Run `make` to build
  
If you are editing the UI, you will also want to install either Qt Designer or Qt Creator

### Building on Windows

#### Setup
- Install Visual Studio 2019
- Install "Desktop development with C++" from the Visual Studio installer
- Download Qt from http://download.qt.io/official_releases/qt/5.12/5.12.10/qt-opensource-windows-x86-5.12.10.exe
- Run the installer and select the MSVC 2017 component to install as well as the defaults
- In Qt Creator, go to Tools -> Options, then set your cmake location if it was not set automatically
- Create a `win64` folder (or `win32` for building 32-bit) in your checkout directory and put an `include` and `lib` directory inside it.
- Copy the library file for OneCore to your `lib` directory (should be found somewhere like `C:\Program Files (x86)\Windows Kits\10\Lib\10.0.18362.0\um\x64`)
- Download the SDL2 development libraries for Visual C++ from https://www.libsdl.org/download-2.0.php
  - Copy the `SDL2.dll` file into your `release` and `debug` folders in your checkout directory
  - Copy the `SDL2.lib` file into your `win64/lib` (or `win32/lib`) folder
  - Make an `SDL2` directory in your `win64/include` or `win32/include` directory, and copy all of the SDL header files into it
- Open the `app.pro` file in Qt Creator
- Set the build directory to your checkout directory and set your build configuration to release (or debug)
- Build the project for the first time in Qt Creator
- Run `windeployqt release/parallel-launcher.exe`
- Copy the C++ runtimes DLLs from your system to the `release` directory (they will be located in a directory path something like `C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Redist\MSVC\14.28.29325\onecore\x64`)
- Optionally, you can also build the style plugins to make more themes available (see below)

#### Building
- If you made changes to the ui files or changed build configurations, run qmake again
- Build the project from Qt Creator.

#### Building the Styles/Themes (Optional)
- Clone the repository hosted at https://github.com/qt/qtstyleplugins into some temporary directory
- Open the project file in Qt Creator
- Go to the Projects tab and set the build configuration to Release
- Run qmake and build from Qt Creator
- Copy the built plugin DLL files to the `style` directory in your debug/release folder for parallel-launcher

#### Building the Installer
- Download and install Inno Setup (https://jrsoftware.org/isinfo.php)
- Before running the installer, remove the `qrc_resources.cpp` file from your release folder
- Open win64-installer.iss (or win32-installer.iss for a 32-bit build) in the source directory
- Click the green play button to build the installer
