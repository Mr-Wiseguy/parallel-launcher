include(app.pro)

program.path = "/usr/local/bin"
program.files = "parallel-launcher"

icon.path = "/usr/local/share/parallel-launcher"
icon.files = "data/appicon.svg"

launcher.path = "/usr/local/share/applications"
launcher.files = "local/parallel-launcher.desktop"

launcher_post.path = "."
launcher_post.files = ""
launcher_post.extra = "update-desktop-database /usr/local/share/applications"

INSTALLS += program icon launcher launcher_post
