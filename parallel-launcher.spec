Name:		parallel-launcher
Version:	2.1.3
Release:	0%{?dist}
Summary:	A simple easy-to-use launcher for the ParallelN64 emulator
License:	GPLv2
Group:		games
URL:		https://blueprint64.ca/parallel-launcher
Source:		https://gitlab.com/mpharoah/parallel-launcher/-/archive/v2.1-3/parallel-launcher-v2.1-3.tar.bz2
Vendor:		Matt Pharoah

BuildRequires: (gcc >= 9 or gcc9 or gcc-toolset-9-gcc)
BuildRequires: (gcc-c++ >= 9 or gcc9-c++ or gcc-toolset-9-gcc-c++)
BuildRequires: (libqt5-qtbase-common-devel or qt5-qtbase-devel)
BuildRequires: (libqt5-qtdeclarative-devel or qt5-qtdeclarative-devel)
BuildRequires: (libSDL2-devel or SDL2-devel)

Requires: glibc
Requires: (libstdc++6 or libstdc++)
Requires: (libgcc_s1 or libgcc)
Requires: ((libQt5Core5 and libQt5Widgets5) or (qt5-qtbase and qt5-qtbase-common and qt5-qtdeclarative and qt5-qtsvg))
Requires: (libQt5Gui5 or qt5-qtbase-gui)
Requires: (libSDL2-2_0-0 or SDL2)
Requires: findutils

%define debug_package %{nil}

%description
A simple easy-to-use launcher for the ParallelN64 and Mupen64Plus-Next emulators.

%prep
%setup -q

%build
if [ `which g++-9` ]; then
	qmake-qt5 app.pro -spec linux-g++ QMAKE_CXX="g++-9"
else
	qmake-qt5 app.pro -spec linux-g++
fi
make

%install
install -D parallel-launcher %{buildroot}/usr/bin/parallel-launcher
install -D parallel-launcher.desktop %{buildroot}/usr/share/applications/parallel-launcher.desktop
install -D data/appicon.svg %{buildroot}/usr/share/parallel-launcher/appicon.svg

%files
/usr/bin/parallel-launcher
/usr/share/applications/parallel-launcher.desktop
/usr/share/parallel-launcher/appicon.svg
